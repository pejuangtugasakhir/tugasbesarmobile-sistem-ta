package id.ac.usu.sistemta.dosen;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import id.ac.usu.sistemta.R;
import id.ac.usu.sistemta.config.ConfigDosenBimbingan;
import id.ac.usu.sistemta.config.ConfigDosenPenilaian;
import id.ac.usu.sistemta.config.ConfigProfilDosen;
import id.ac.usu.sistemta.handler.RequestHandler;
import id.ac.usu.sistemta.session.UserSessionManager;

public class profile extends Fragment {
    UserSessionManager session;
    String name;
    Activity context;
    Button edit;
    TextView nimmhs,namamhs,alamatmhs,emailmhs,mabing,mapeng;

    public profile() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        context = getActivity();
        return inflater.inflate(R.layout.dosen_profile, container, false);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void onStart() {
        super.onStart();
        session = new UserSessionManager(context.getApplicationContext());

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();

        // get name
        name = user.get(UserSessionManager.KEY_NAME);

        nimmhs = (TextView) context.findViewById(R.id.textView1);
        namamhs = (TextView) context.findViewById(R.id.textView3);
        alamatmhs = (TextView) context.findViewById(R.id.alamat);
        emailmhs = (TextView) context.findViewById(R.id.email);
        mabing = (TextView) context.findViewById(R.id.mabing);
        mapeng = (TextView) context.findViewById(R.id.mapeng);

        edit = (Button) context.findViewById(R.id.button52);
        edit.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent m = new Intent(context, EditProfileDosen.class);
                                        startActivity(m);
                                    }
                                }
        );
        getProfil();
        getDoping();

        ListView listview1 = (ListView) context.findViewById(R.id.listView2);
        listview1.setFastScrollEnabled(true);

        String url = ConfigDosenBimbingan.URL_GET_BIMBINGAN_DSN + name;

        try {
            JSONArray data = new JSONArray(getJSONUrl(url));

            final ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> map;

            for (int i = 0; i < data.length(); i++) {
                JSONObject c = data.getJSONObject(i);
                map = new HashMap<String, String>();
                map.put("id_bimbingan", c.getString("id_bimbingan"));
                map.put("nama_mhs", c.getString("nama_mhs"));
                map.put("tgl_bimbingan", c.getString("tgl_bimbingan"));
                map.put("topik_diskusi", c.getString("topik_diskusi"));
                map.put("hasil_bimbingan", c.getString("hasil_bimbingan"));
                MyArrList.add(map);
            }

            SimpleAdapter sAdap;

            sAdap = new SimpleAdapter(getActivity(), MyArrList, R.layout.activity_column_bimbingan,
                    new String[]{"nama_mhs", "tgl_bimbingan", "topik_diskusi", "hasil_bimbingan"}, new int[]
                    {R.id.ColNama, R.id.ColTgl, R.id.ColTopik, R.id.ColHasil});

            listview1.setAdapter(sAdap);
            listview1.setOnTouchListener(new ListView.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    int action = event.getAction();
                    switch (action) {
                        case MotionEvent.ACTION_DOWN:
                            // Disallow ScrollView to intercept touch events.
                            v.getParent().requestDisallowInterceptTouchEvent(true);
                            break;

                        case MotionEvent.ACTION_UP:
                            // Allow ScrollView to intercept touch events.
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                    // Handle ListView touch events.
                    v.onTouchEvent(event);
                    return true;
                }
            });

            final AlertDialog.Builder viewDetail = new AlertDialog.Builder(context);
            //OnClick item

            listview1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long mylng) {
                    HashMap<String, String> map = (HashMap) parent.getItemAtPosition(position);
                    String idB = map.get("id_bimbingan");
                    String nmdsn = map.get("nama_mhs");
                    String tgl = map.get(ConfigDosenBimbingan.TAG_TGL_DSN).toString();
                    String topibim = map.get(ConfigDosenBimbingan.TAG_TOPIK_DSN).toString();
                    String hsil = map.get(ConfigDosenBimbingan.TAG_HASIL_DSN).toString();

                    //viewDetail.setIcon(android.R.drawable.star_on);
                    viewDetail.setTitle("DETAIL");
                    viewDetail.setMessage("Nama Mahasiswa : " + nmdsn + "\n\n" +
                            "Tanggal Bimbingan :" + tgl + "\n\n" +
                            "Topik Bimbingan :" + topibim + "\n\n" +
                            "Hasil Bimbingan :" + hsil);

                    viewDetail.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    //TODOAutoGenerated

                                    dialog.dismiss();
                                }
                            });
                    viewDetail.show();
                }
            });

            ListView listview2 = (ListView) context.findViewById(R.id.listView5);
            listview2.setFastScrollEnabled(true);

            String url2 = ConfigDosenPenilaian.URL_GET_MHS_BIM + name;

            try {
                JSONArray data2 = new JSONArray(getJSONUrl(url2));

                final ArrayList<HashMap<String, String>> MyArrList2 = new ArrayList<HashMap<String, String>>();
                HashMap<String, String> map2;

                for (int i = 0; i < data2.length(); i++) {
                    JSONObject c = data2.getJSONObject(i);
                    map2 = new HashMap<String, String>();
                    map2.put("id_nilai", c.getString("id_nilai"));
                    map2.put("n_semhas", c.getString("n_semhas"));
                    map2.put("nama_mhs", c.getString("nama_mhs"));
                    map2.put("n_sidang", c.getString("n_sidang"));
                    map2.put("nama_dosen", c.getString("nama_dosen"));
                    map2.put("judul_ta", c.getString("judul_ta"));
                    map2.put("tgl_nilai_semhas", c.getString("tgl_nilai_semhas"));
                    map2.put("tgl_nilai_sidang", c.getString("tgl_nilai_sidang"));
                    map2.put("nama_bidang", c.getString("nama_bidang"));
                    MyArrList2.add(map2);
                }

                SimpleAdapter sAdap2;

                sAdap2 = new SimpleAdapter(getActivity(), MyArrList2, R.layout.activity_column_penilaian,
                        new String[]{"nama_mhs", "n_semhas", "n_sidang", "judul_ta", "nama_dosen"}, new int[]
                        {R.id.ColNama, R.id.ColTgl, R.id.ColTopik, R.id.ColHasil, R.id.ColStatus});

                listview2.setAdapter(sAdap2);
                listview2.setOnTouchListener(new ListView.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        int action = event.getAction();
                        switch (action) {
                            case MotionEvent.ACTION_DOWN:
                                // Disallow ScrollView to intercept touch events.
                                v.getParent().requestDisallowInterceptTouchEvent(true);
                                break;

                            case MotionEvent.ACTION_UP:
                                // Allow ScrollView to intercept touch events.
                                v.getParent().requestDisallowInterceptTouchEvent(false);
                                break;
                        }
                        // Handle ListView touch events.
                        v.onTouchEvent(event);
                        return true;
                    }
                });

                final AlertDialog.Builder viewDetail2 = new AlertDialog.Builder(context);
                //OnClick item

                listview2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        HashMap<String, String> map = (HashMap) parent.getItemAtPosition(position);
                        String idB = map.get("id_jadwal");
                        String nilai_semhas = map.get("n_semhas");
                        String namaMhs = map.get("nama_mhs");
                        String judul_ta = map.get("judul_ta");
                        String namadosenn = map.get("nama_dosen");
                        String nilaisidang = map.get("n_sidang");
                        String tanggalsemhas = map.get("tgl_nilai_semhas");
                        String tanggalsidang = map.get("tgl_nilai_sidang");
                        String namabidang = map.get("nama_bidang");


                        //viewDetail.setIcon(android.R.drawable.star_on);
                        viewDetail2.setTitle("DETAIL");
                        viewDetail2.setMessage("Nama Mahasiswa : " + namaMhs + "\n\n" +
                                "Nilai Seminar Hasil :" + nilai_semhas + "\n\n" +
                                "Tanggal Penilaian Semhas :" + tanggalsemhas + "\n\n" +
                                "Nilai Sidang :" + nilaisidang + "\n\n" +
                                "Tanggal Penilaian Sidang :" + tanggalsidang + "\n\n" +
                                "Judul TA :" + judul_ta + "\n\n" +
                                "Bidang TA :" + namabidang + "\n\n" +
                                "Dosen Penilai :" + namadosenn);

                        viewDetail2.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        //TODOAutoGenerated

                                        dialog.dismiss();
                                    }
                                });
                        viewDetail2.show();
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getDoping(){
        class GetDop extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute(){
                super.onPreExecute();
                //loading = ProgressDialog.show(getActivity(),"Fetching....","Wait...",false,false);
            }
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                //loading.dismiss();
                showDop(s);
            }
            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequestParam(ConfigProfilDosen.URL_SELECT_MHS_UJI,name);
                return s;
            }
        }
        GetDop gp = new GetDop();
        gp.execute();
    }

    private void showDop(String json){
        try{
            JSONObject jsonObject = new JSONObject(json);
            JSONArray result = jsonObject.getJSONArray(ConfigProfilDosen.TAG_JSON_ARRAY2);
            JSONObject c = result.getJSONObject(0);
            String namapenguji=c.getString(ConfigProfilDosen.TAG_MAPENG);

            mapeng.setText(namapenguji);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getProfil(){
        class GetProf extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute(){
                super.onPreExecute();
                loading = ProgressDialog.show(getActivity(),"Fetching....","Wait...",false,false);
            }
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                showProf(s);
            }
            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequestParam(ConfigProfilDosen.URL_SELECT_PROFIL_DOSEN,name);
                return s;
            }
        }
        GetProf gp = new GetProf();
        gp.execute();
    }

    public String getJSONUrl(String url){
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) { // Download OK
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download result..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }

    private void showProf(String json){
        try{
            JSONObject jsonObject = new JSONObject(json);
            JSONArray result = jsonObject.getJSONArray(ConfigProfilDosen.TAG_JSON_ARRAY);
            JSONObject c = result.getJSONObject(0);
            String nims = c.getString(ConfigProfilDosen.TAG_NIP);
            String namas = c.getString(ConfigProfilDosen.TAG_NAMA_DOSEN);
            String alamats = c.getString(ConfigProfilDosen.TAG_ALAMAT_DOSEN);
            String emails = c.getString(ConfigProfilDosen.TAG_EMAIL_DOSEN);
            String mabinganda = c.getString(ConfigProfilDosen.TAG_MABING);

            namamhs.setText(namas);
            nimmhs.setText(nims);
            alamatmhs.setText(alamats);
            emailmhs.setText(emails);
            mabing.setText(mabinganda);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
