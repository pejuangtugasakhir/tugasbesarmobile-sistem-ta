package id.ac.usu.sistemta.dosen;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import id.ac.usu.sistemta.R;
import id.ac.usu.sistemta.config.ConfigDosenPenilaian;
import id.ac.usu.sistemta.session.UserSessionManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class Dosen_Penilaian_Semhas extends Fragment {
    Button syarat;
    Activity context;
    UserSessionManager session;
    String name;
    public Dosen_Penilaian_Semhas() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        context=getActivity();
        return inflater.inflate(R.layout.dosen__penilaian__semhas, null);
    }

    public void onStart(){
        super.onStart();
        session = new UserSessionManager(context.getApplicationContext());

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();

        // get name
        name = user.get(UserSessionManager.KEY_NAME);


        if(android.os.Build.VERSION.SDK_INT > 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        syarat = (Button) context.findViewById(R.id.button52);
        syarat.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent m = new Intent(context, assesment.class);
                                        startActivity(m);
                                    }
                                }
        );

        ListView listview3 = (ListView) context.findViewById(R.id.listView4);
        listview3.setFastScrollEnabled(true);

        String url3 = ConfigDosenPenilaian.URL_GET_MHS_SEMINAR + name;

        try {
            JSONArray data3 = new JSONArray(getJSONUrl(url3));

            final ArrayList<HashMap<String, String>> MyArrList3 = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> map3;

            for (int i = 0; i < data3.length(); i++) {
                JSONObject c = data3.getJSONObject(i);
                map3 = new HashMap<String, String>();
                map3.put("id_jadwal", c.getString("id_jadwal"));
                map3.put("nama_mhs", c.getString("nama_mhs"));
                map3.put("tanggal_seminar", c.getString("tanggal_seminar"));
                map3.put("waktu_seminar", c.getString("waktu_seminar"));
                map3.put("judul_ta", c.getString("judul_ta"));
                map3.put("status_kegiatan", c.getString("status_kegiatan"));
                MyArrList3.add(map3);
            }

            SimpleAdapter sAdap3;

            sAdap3 = new SimpleAdapter(getActivity(), MyArrList3, R.layout.activity_column_penilaian,
                    new String[]{"nama_mhs", "tanggal_seminar", "waktu_seminar", "judul_ta", "status_kegiatan"}, new int[]
                    {R.id.ColNama, R.id.ColTgl, R.id.ColTopik, R.id.ColHasil, R.id.ColStatus});

            listview3.setAdapter(sAdap3);
            listview3.setOnTouchListener(new ListView.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    int action = event.getAction();
                    switch (action) {
                        case MotionEvent.ACTION_DOWN:
                            // Disallow ScrollView to intercept touch events.
                            v.getParent().requestDisallowInterceptTouchEvent(true);
                            break;

                        case MotionEvent.ACTION_UP:
                            // Allow ScrollView to intercept touch events.
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                    // Handle ListView touch events.
                    v.onTouchEvent(event);
                    return true;
                }
            });

            final AlertDialog.Builder viewDetail3 = new AlertDialog.Builder(context);
            //OnClick item

            listview3.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent3 = new Intent(context, borang_masukan.class);

                    HashMap<String, String> map3 = (HashMap) parent.getItemAtPosition(position);
                    String idB = map3.get("id_jadwal");
                    String namaMhs = map3.get("nama_mhs");

                    intent3.putExtra(ConfigDosenPenilaian.JADWAL_ID, idB);
                    intent3.putExtra(ConfigDosenPenilaian.MHS_NAMA, namaMhs);

                    startActivity(intent3);
                }
            });


            ListView listview1 = (ListView)context.findViewById(R.id.listView2);
        listview1.setFastScrollEnabled(true);

        String url = ConfigDosenPenilaian.URL_GET_MHS_SEMHAS+name;

        try {
            JSONArray data = new JSONArray(getJSONUrl(url));

            final ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> map;

            for (int i = 0; i < data.length(); i++) {
                JSONObject c = data.getJSONObject(i);
                map = new HashMap<String, String>();
                map.put("id_jadwal", c.getString("id_jadwal"));
                map.put("nama_mhs", c.getString("nama_mhs"));
                map.put("tanggal_seminar", c.getString("tanggal_seminar"));
                map.put("waktu_seminar", c.getString("waktu_seminar"));
                map.put("judul_ta", c.getString("judul_ta"));
                map.put("status_kegiatan", c.getString("status_kegiatan"));
                MyArrList.add(map);
            }

            SimpleAdapter sAdap;

            sAdap = new SimpleAdapter(getActivity(), MyArrList, R.layout.activity_column_penilaian,
                    new String[]{"nama_mhs", "tanggal_seminar", "waktu_seminar", "judul_ta", "status_kegiatan"}, new int[]
                    {R.id.ColNama, R.id.ColTgl, R.id.ColTopik, R.id.ColHasil, R.id.ColStatus});

            listview1.setAdapter(sAdap);
            listview1.setOnTouchListener(new ListView.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    int action = event.getAction();
                    switch (action) {
                        case MotionEvent.ACTION_DOWN:
                            // Disallow ScrollView to intercept touch events.
                            v.getParent().requestDisallowInterceptTouchEvent(true);
                            break;

                        case MotionEvent.ACTION_UP:
                            // Allow ScrollView to intercept touch events.
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                    // Handle ListView touch events.
                    v.onTouchEvent(event);
                    return true;
                }
            });

            final AlertDialog.Builder viewDetail = new AlertDialog.Builder(context);
            //OnClick item

            listview1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(context, borang_penilaian_semhas.class);

                    HashMap<String, String> map = (HashMap) parent.getItemAtPosition(position);
                    String idB = map.get("id_jadwal");
                    String namaMhs = map.get("nama_mhs");
                    String judul_ta = map.get("judul_ta");

                    intent.putExtra(ConfigDosenPenilaian.JADWAL_ID, idB);
                    intent.putExtra(ConfigDosenPenilaian.MHS_NAMA, namaMhs);
                    intent.putExtra(ConfigDosenPenilaian.MHS_JUDUL, judul_ta);

                    startActivity(intent);
                }
            });

            ListView listview2 = (ListView) context.findViewById(R.id.listView5);
            listview2.setFastScrollEnabled(true);

            String url2 = ConfigDosenPenilaian.URL_GET_MHS_SIDANG + name;

            try {
                JSONArray data2 = new JSONArray(getJSONUrl(url2));

                final ArrayList<HashMap<String, String>> MyArrList2 = new ArrayList<HashMap<String, String>>();
                HashMap<String, String> map2;

                for (int i = 0; i < data2.length(); i++) {
                    JSONObject c = data2.getJSONObject(i);
                    map2 = new HashMap<String, String>();
                    map2.put("id_jadwal", c.getString("id_jadwal"));
                    map2.put("n_semhas", c.getString("n_semhas"));
                    map2.put("nama_mhs", c.getString("nama_mhs"));
                    map2.put("tanggal_seminar", c.getString("tanggal_seminar"));
                    map2.put("waktu_seminar", c.getString("waktu_seminar"));
                    map2.put("judul_ta", c.getString("judul_ta"));
                    map2.put("status_kegiatan", c.getString("status_kegiatan"));
                    map2.put("id_nilai", c.getString("id_nilai"));
                    map2.put("tgl_nilai_semhas", c.getString("tgl_nilai_semhas"));
                    MyArrList2.add(map2);
                }

                SimpleAdapter sAdap2;

                sAdap2 = new SimpleAdapter(getActivity(), MyArrList2, R.layout.activity_column_penilaian,
                        new String[]{"nama_mhs", "tanggal_seminar", "waktu_seminar", "judul_ta", "status_kegiatan"}, new int[]
                        {R.id.ColNama, R.id.ColTgl, R.id.ColTopik, R.id.ColHasil, R.id.ColStatus});

                listview2.setAdapter(sAdap2);
                listview2.setOnTouchListener(new ListView.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        int action = event.getAction();
                        switch (action) {
                            case MotionEvent.ACTION_DOWN:
                                // Disallow ScrollView to intercept touch events.
                                v.getParent().requestDisallowInterceptTouchEvent(true);
                                break;

                            case MotionEvent.ACTION_UP:
                                // Allow ScrollView to intercept touch events.
                                v.getParent().requestDisallowInterceptTouchEvent(false);
                                break;
                        }
                        // Handle ListView touch events.
                        v.onTouchEvent(event);
                        return true;
                    }
                });

                final AlertDialog.Builder viewDetail2 = new AlertDialog.Builder(context);
                //OnClick item

                listview2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent intent2 = new Intent(context, borang_penilaian_sidang.class);

                        HashMap<String, String> map2 = (HashMap) parent.getItemAtPosition(position);
                        String idB = map2.get("id_jadwal");
                        String nilai_semhas = map2.get("n_semhas");
                        String namaMhs = map2.get("nama_mhs");
                        String judul_ta = map2.get("judul_ta");
                        String nilaiID = map2.get("id_nilai");
                        String tgl_semhas = map2.get("tgl_nilai_semhas");


                        intent2.putExtra(ConfigDosenPenilaian.JADWAL_ID, idB);
                        intent2.putExtra(ConfigDosenPenilaian.NILAI_SEMHAS, nilai_semhas);
                        intent2.putExtra(ConfigDosenPenilaian.MHS_NAMA, namaMhs);
                        intent2.putExtra(ConfigDosenPenilaian.MHS_JUDUL, judul_ta);
                        intent2.putExtra(ConfigDosenPenilaian.NILAI_ID, nilaiID);
                        intent2.putExtra(ConfigDosenPenilaian.TGL_SEMHAS, tgl_semhas);

                        startActivity(intent2);
                    }
                });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public String getJSONUrl(String url){
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) { // Download OK
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download result..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }
}
