package id.ac.usu.sistemta.koorTA;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.android.volley.toolbox.NetworkImageView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import id.ac.usu.sistemta.R;
import id.ac.usu.sistemta.config.ConfigInputPenguji;
import id.ac.usu.sistemta.config.ConfigKoorTAPenilaianLihat;
import id.ac.usu.sistemta.session.UserSessionManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class pengujiKoorta extends Fragment {

    UserSessionManager session;
    String name;
    Activity context;

    public pengujiKoorta() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context=getActivity();
        return inflater.inflate(R.layout.koorta_penguji, null);
    }

    public void onStart() {
        super.onStart();
        session = new UserSessionManager(context.getApplicationContext());

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();

        // get name
        name = user.get(UserSessionManager.KEY_NAME);
        if(android.os.Build.VERSION.SDK_INT > 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        ListView listview1 = (ListView)context.findViewById(R.id.listView5);
        listview1.setFastScrollEnabled(true);

        String url="http://sistemta.esy.es/sistem_ta/mod/koorta/select_penguji.php";
        try {
            JSONArray data = new JSONArray(getJSONUrl(url));
            final ArrayList<HashMap<String, String>> MyarrList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> map;

            for (int i = 0; i < data.length(); i++) {
                JSONObject c = data.getJSONObject(i);
                map = new HashMap<String, String>();
                map.put("nim", c.getString("nim"));
                map.put("nama_mhs", c.getString("nama_mhs"));
                map.put("judul", c.getString("judul"));
                map.put("bidang", c.getString("bidang"));
                map.put("doping1", c.getString("doping1"));
                MyarrList.add(map);
            }

            SimpleAdapter sAdap;
            sAdap = new SimpleAdapter(getActivity(), MyarrList, R.layout.activity_column_penguji,
                    new String[]{"nim", "nama_mhs", "judul", "bidang", "doping1"}, new int[]
                    {R.id.ColNim, R.id.ColNama, R.id.ColJudul, R.id.ColBidang,R.id.ColDoping1});

            listview1.setAdapter(sAdap);

            listview1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(context, input_pengujiKoorta.class);

                    HashMap<String, String> map = (HashMap) parent.getItemAtPosition(position);

                    String nimMhs = map.get(ConfigInputPenguji.TAG_NIMMHS).toString();
                    String namaMhs = map.get(ConfigInputPenguji.TAG_NAMAMHS).toString();
                    String namadopingsatu = map.get(ConfigInputPenguji.TAG_NAMADOPINGSATU).toString();

                    intent.putExtra(ConfigInputPenguji.KEY_EMP_NIM, nimMhs);
                    intent.putExtra(ConfigInputPenguji.KEY_EMP_NAMA, namaMhs);
                    intent.putExtra(ConfigInputPenguji.KEY_EMP_DOSENPEMBIMBINGSATU, namadopingsatu);

                    startActivity(intent);
                }
            });
        }
        catch (JSONException e){
            e.printStackTrace();
        }
    }

    public String getJSONUrl(String url){
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) { // Download OK
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download result..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
