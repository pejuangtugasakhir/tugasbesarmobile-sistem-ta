package id.ac.usu.sistemta.tataUsaha;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.PrivateKey;
import java.util.HashMap;

import id.ac.usu.sistemta.R;
import id.ac.usu.sistemta.config.ConfigJudul;
import id.ac.usu.sistemta.config.ConfigJudulTA;
import id.ac.usu.sistemta.config.ConfigMhs;
import id.ac.usu.sistemta.config.ConfigSum;
import id.ac.usu.sistemta.handler.RequestHandler;

public class tu_update_judul extends AppCompatActivity implements View.OnClickListener {
    private String id_judulta,nim,jdl,bd;
    private EditText NIM;
    private EditText judul;
    RadioGroup bidanggroup;
    private RadioButton bidangmul,bidangcom,bidangdat,bidangstatus;
    Button buttonupd,buttoncenl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tu_update_judul);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        id_judulta = intent.getStringExtra(ConfigJudulTA.ASS_ID);
        nim = intent.getStringExtra(ConfigJudulTA.ASS_NIM);
        jdl = intent.getStringExtra(ConfigJudulTA.ASS_JUDUL);
        bd =intent.getStringExtra(ConfigJudulTA.ASS_BIDANG);

        NIM = (EditText) findViewById(R.id.eNim);
        judul = (EditText) findViewById(R.id.editText5);

        bidanggroup = (RadioGroup) findViewById(R.id.bidang);
        bidangmul=(RadioButton)findViewById(R.id.radioBidang1);
        bidangdat=(RadioButton)findViewById(R.id.radioBidang2);
        bidangcom=(RadioButton)findViewById(R.id.radioBidang3);

        buttonupd = (Button) findViewById(R.id.button5);
        buttoncenl = (Button) findViewById(R.id.button6);

        buttonupd.setOnClickListener(this);
        buttoncenl.setOnClickListener(this);

        NIM.setText(nim);
        judul.setText(jdl);

        if(bd.equals("Multimedia")){
            bidangmul.setChecked(true);
            bidangdat.setChecked(false);
            bidangcom.setChecked(false);
        }
        else if(bd.equals("Knowledge Management dan Data Science")){
            bidangmul.setChecked(false);
            bidangdat.setChecked(true);
            bidangcom.setChecked(false);
        }
        else{
            bidangmul.setChecked(false);
            bidangdat.setChecked(false);
            bidangcom.setChecked(true);
        }

        judul.requestFocus();
        getJdl();
    }

    @Override
    public void onClick(View v) {
        if(v == buttonupd){
            updateJdl();
        }

        if(v == buttoncenl){
            reset();
        }
    }

    private void getJdl(){
        class GetJdl extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;

            @Override
            protected void onPreExecute(){
                super.onPreExecute();
                loading = ProgressDialog.show(tu_update_judul.this,"Fetching....","Wait...",false,false);
            }
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                showJdl(s);
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequestParam(ConfigJudulTA.URL_GET_JDL_ONE,id_judulta);
                return s;
            }
        }
        GetJdl ge = new GetJdl();
        ge.execute();
    }

    private void showJdl(String json){
        try{
            JSONObject jsonObject = new JSONObject(json);
            JSONArray result = jsonObject.getJSONArray(ConfigJudulTA.TAG_JSON_ARRAY);
            JSONObject c = result.getJSONObject(0);
            String nm = c.getString(ConfigJudulTA.TAG_NIM);
            String jdl = c.getString(ConfigJudulTA.TAG_JUDUL);
            String bd = c.getString(ConfigJudulTA.TAG_BIDANG);

            NIM.setText(nm);
            judul.setText(jdl);
            if(bd.equals("Multimedia")){
                bidangmul.setChecked(true);
                bidangdat.setChecked(false);
                bidangcom.setChecked(false);
            }
            if(bd.equals("Knowledge Management dan Data Science")){
                bidangmul.setChecked(false);
                bidangdat.setChecked(true);
                bidangcom.setChecked(false);
            }
            if(bd.equals("Computer System")){
                bidangmul.setChecked(false);
                bidangdat.setChecked(false);
                bidangcom.setChecked(true);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void updateJdl(){
        final String nim = NIM.getText().toString().trim();
        final String jdl = judul.getText().toString().trim();

        int selectId = bidanggroup.getCheckedRadioButtonId();

        bidangstatus=(RadioButton)findViewById(selectId);
        final String bidangsum = bidangstatus.getText().toString().trim();

        class UpdateJdl extends AsyncTask<Void,Void,String>{
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(tu_update_judul.this,"Updating...","Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(tu_update_judul.this, s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(Void... params) {
                HashMap<String,String> hashMap = new HashMap<>();
                hashMap.put(ConfigJudulTA.KEY_EMP_ID,id_judulta);
                hashMap.put(ConfigJudulTA.KEY_EMP_NIM,nim);
                hashMap.put(ConfigJudulTA.KEY_EMP_JUDUL,jdl);
                hashMap.put(ConfigJudulTA.KEY_EMP_BIDANG,bidangsum);

                RequestHandler rh = new RequestHandler();

                String s = rh.sendPostRequest(ConfigJudulTA.URL_UPDATE_JDL, hashMap);
                return s;
            }
        }

        UpdateJdl ue = new UpdateJdl();
        ue.execute();
    }
    private void reset() {
        NIM.setText(" ");
        judul.setText(" ");
        bidangstatus.equals(" ");
        judul.requestFocus();
    }
}