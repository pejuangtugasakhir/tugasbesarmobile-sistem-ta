package id.ac.usu.sistemta.menuUtama;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import id.ac.usu.sistemta.dosen.MainActivity;
import id.ac.usu.sistemta.koorTA.MainActivityKoorta;
import id.ac.usu.sistemta.mahasiswa.MainActivityMhs;
import id.ac.usu.sistemta.tataUsaha.MainActivityTU;
import id.ac.usu.sistemta.R;
import id.ac.usu.sistemta.session.UserSessionManager;

public class Login extends AppCompatActivity {
    private EditText username, password;
    private Button sign_in_login;
    private RequestQueue requestQueue;
    private static final String URL="http://sistemta.esy.es/sistem_ta/mod/login/cek_login.php";
    private StringRequest request;
    UserSessionManager session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        session = new UserSessionManager(getApplicationContext());

        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        sign_in_login = (Button) findViewById(R.id.sign_in_login);

        requestQueue = Volley.newRequestQueue(this);
        username.requestFocus();

        sign_in_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String username_s = username.getText().toString();
                String password_p = password.getText().toString();

                if(username_s.isEmpty()){
                    username.setError("Enter username");
                    Toast.makeText(getBaseContext(),"Login Failed", Toast.LENGTH_LONG).show();

                } else if(password_p.isEmpty()){
                    password.setError("Enter password");
                    Toast.makeText(getBaseContext(),"Login Failed", Toast.LENGTH_LONG).show();

                } else if(username_s.isEmpty() || password_p.isEmpty()){
                    Toast.makeText(getBaseContext(),"Both field not allowed empty", Toast.LENGTH_LONG).show();
                }else {
                    final ProgressDialog progresRing = ProgressDialog.show(Login.this, "", "Authenticating...", true);

                    progresRing.setCancelable(true);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(3000);
                            } catch (Exception e) {

                            }
                            progresRing.dismiss();
                        }
                    }).start();

                    request = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                //String s = jsonObject.getString("status");
                                if (jsonObject.getString("status").equals("admin")) {
                                    Toast.makeText(getApplication(), "Welcome " + username.getText(), Toast.LENGTH_SHORT).show();

                                    String u = username.getText().toString();
                                    String p = password.getText().toString();

                                    session.createUserLoginSession(u,p);

                                    Intent intent = new Intent(Login.this, MainActivityTU.class);

                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                                    startActivity(intent);
                                } else if (jsonObject.getString("status").equals("dosen")) {
                                    Toast.makeText(getApplication(), "Welcome " + username.getText(), Toast.LENGTH_SHORT).show();

                                    String u = username.getText().toString();
                                    String p = password.getText().toString();

                                    session.createUserLoginSession(u, p);

                                    Intent intent = new Intent(Login.this, MainActivity.class);

                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                                    startActivity(intent);

                                } else if (jsonObject.getString("status").equals("mahasiswa")) {
                                    Toast.makeText(getApplication(), "Welcome " + username.getText(), Toast.LENGTH_SHORT).show();

                                    String u = username.getText().toString();
                                    String p = password.getText().toString();

                                    session.createUserLoginSession(u, p);

                                    Intent intent = new Intent(Login.this, MainActivityMhs.class);

                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                                    startActivity(intent);
                                }
                                else if (jsonObject.getString("status").equals("koordinator TA")) {
                                    Toast.makeText(getApplication(), "Welcome " + username.getText(), Toast.LENGTH_SHORT).show();

                                    String u = username.getText().toString();
                                    String p = password.getText().toString();

                                    session.createUserLoginSession(u, p);

                                    Intent intent = new Intent(Login.this, MainActivityKoorta.class);

                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                                    startActivity(intent);
                                }
                                else if (jsonObject.getString("status").equals("Not Found")) {
                                    Toast.makeText(getApplication(), "Error " + jsonObject.getString("status"), Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getApplication(), "Error " + jsonObject.getString("error"), Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            HashMap<String, String> hashMap = new HashMap<String, String>();
                            hashMap.put("username", username.getText().toString());
                            hashMap.put("password", password.getText().toString());

                            return hashMap;
                        }
                    };
                    requestQueue.add(request);
                }
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();  // Always call the superclass method first
        username.requestFocus();
        username.setText("");
        password.setText("");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }
    public void onBackPressed() {
        super.onBackPressed();
        Intent m=new Intent(Login.this,SistemTA.class);
        m.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(m);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
