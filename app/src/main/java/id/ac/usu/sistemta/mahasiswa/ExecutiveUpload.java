package id.ac.usu.sistemta.mahasiswa;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Toast;

import java.util.HashMap;

import id.ac.usu.sistemta.R;
import id.ac.usu.sistemta.config.ConfigSum;
import id.ac.usu.sistemta.handler.RequestHandler;
import id.ac.usu.sistemta.session.UserSessionManager;

public class ExecutiveUpload extends AppCompatActivity implements View.OnClickListener,OnCheckedChangeListener{
    private EditText judul,nmadsn;
    private RadioGroup rekomgroup,bidanggroup;
    private RadioButton rekomta,bidangta,rekomdos,rekommhs;
    UserSessionManager session;
    private Button submit,cancel;
    private String name;

    public ExecutiveUpload() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mhs_executive_upload);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        judul = (EditText) findViewById(R.id.editText5);
        nmadsn=(EditText) findViewById(R.id.namadsn);
        rekomgroup = (RadioGroup) findViewById(R.id.rekom);
        bidanggroup = (RadioGroup) findViewById(R.id.bidang);

        rekomdos =(RadioButton)findViewById(R.id.radioButton2);
        rekommhs=(RadioButton)findViewById(R.id.radioButton);

        submit = (Button) findViewById(R.id.button5);
        cancel = (Button) findViewById(R.id.button6);

        submit.setOnClickListener(this);
        cancel.setOnClickListener(this);

        rekomgroup.setOnCheckedChangeListener(this);
    }

    public void actv (final boolean active){
        nmadsn.setEnabled(active);
        if(active)
        {
            nmadsn.requestFocus();
        }
    }

    public void onStart(){
        super.onStart();
        session = new UserSessionManager(getApplicationContext());

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();

        // get name
        name = user.get(UserSessionManager.KEY_NAME);
    }

    @Override
    public void onClick(View v) {
        if(v==submit){
            addsum();
        }
        else if(v==cancel){
            startActivity(new Intent(this, ExecutiveUpload.class));
            finish();
        }
    }

    private void addsum(){
        final String judulsum=judul.getText().toString().trim();

        int selectId = bidanggroup.getCheckedRadioButtonId();
        bidangta=(RadioButton)findViewById(selectId);
        final String bidangsum1 = bidangta.getText().toString().trim();

        int selectIdrekom = rekomgroup.getCheckedRadioButtonId();
        rekomta=(RadioButton)findViewById(selectIdrekom);

        final String rekom1 = rekomta.getText().toString().trim();

        final String ndsn=nmadsn.getText().toString().trim();

        class addsum extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(ExecutiveUpload.this,"Adding...","Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(ExecutiveUpload.this, s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(Void... v) {
                HashMap<String,String> params = new HashMap<>();
                params.put(ConfigSum.KEY_EMP_NAMAMHS,name);
                params.put(ConfigSum.KEY_EMP_JUDUL,judulsum);
                params.put(ConfigSum.KEY_EMP_REKOM1, rekom1);
                params.put(ConfigSum.KEY_EMP_BIDANG1, bidangsum1);
                params.put(ConfigSum.KEY_EMP_NAMADSN,ndsn);

                RequestHandler rh = new RequestHandler();
                String res = rh.sendPostRequest(ConfigSum.URL_ADD, params);
                return res;
            }
        }

        if(judulsum.isEmpty()){
            Toast.makeText(getBaseContext(),"Masukkan judul TA yang diajukan: ", Toast.LENGTH_LONG).show();
        }
        else if(rekom1.equals("Dosen") && ndsn.isEmpty()){
            Toast.makeText(getBaseContext(),"Masukkan Nama Dosen Yang Merekomendasi: ", Toast.LENGTH_LONG).show();
        }
        else{
            addsum ae = new addsum();
            ae.execute();
            judul.setText(" ");
            nmadsn.setText(" ");
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId){
            case R.id.radioButton:
                actv(false);
                break;
            case R.id.radioButton2:
                actv(true);
                break;
        }
    }
}
