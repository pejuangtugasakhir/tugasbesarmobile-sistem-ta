package id.ac.usu.sistemta.mahasiswa;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import id.ac.usu.sistemta.R;
import id.ac.usu.sistemta.config.ConfigSum;
import id.ac.usu.sistemta.handler.RequestHandler;
import id.ac.usu.sistemta.session.UserSessionManager;

public class Mhssummary extends Fragment {
    Activity context;
    ImageButton add,upload;
    UserSessionManager session;
    String name;
    ArrayList<HashMap<String, String>> MyArrList;
    String id_exsum;

    public Mhssummary() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        context=getActivity();
        return inflater.inflate(R.layout.mhs_excutivesummary, container, false);
    }

    public void onStart(){
        super.onStart();
        session = new UserSessionManager(context.getApplicationContext());

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();

        // get name
        name = user.get(UserSessionManager.KEY_NAME);

        add=(ImageButton)context.findViewById(R.id.add_sum);
        add.setOnClickListener(new View.OnClickListener() {
                                   @Override
                                   public void onClick(View view) {
                                       Intent m = new Intent(context, ExecutiveUpload.class);
                                       startActivity(m);
                                   }
                               }
        );

        upload=(ImageButton)context.findViewById(R.id.imageButton);
        upload.setOnClickListener(new View.OnClickListener() {
                                      @Override
                                      public void onClick(View view) {
                                          Intent m = new Intent(context, UploadFileAsst.class);
                                          startActivity(m);
                                      }
                                  }
        );

        if(android.os.Build.VERSION.SDK_INT > 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        ListView listview1 = (ListView)context.findViewById(R.id.listView5);
        listview1.setFastScrollEnabled(true);

        String url = ConfigSum.URL_GET_SUM+name;

        try{
            JSONArray data = new JSONArray(getJSONUrl(url));

            final ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
            HashMap<String,String> map;

            for (int i = 0; i<data.length(); i++){
                JSONObject c= data.getJSONObject(i);
                map = new HashMap<String,String>();
                map.put("id_exsum", c.getString("id_exsum"));
                map.put("nim", c.getString("nim"));
                map.put("judul_exsum", c.getString("judul_exsum"));
                map.put("rekomendasi_exsum", c.getString("rekomendasi_exsum"));
                map.put("nama_bidang", c.getString("nama_bidang"));
                map.put("tanggal_masuk_exsum", c.getString("tanggal_masuk_exsum"));
                MyArrList.add(map);
            }

            SimpleAdapter sAdap;

            sAdap = new SimpleAdapter(getActivity(), MyArrList, R.layout.activity_column_summary,
                    new String[] {"judul_exsum", "rekomendasi_exsum", "nama_bidang","tanggal_masuk_exsum"}, new int[]
                    {R.id.ColJudul , R.id.ColRekom, R.id.ColNamabid,R.id.ColTgl});
            listview1.setAdapter(sAdap);

            final android.app.AlertDialog.Builder viewDetail = new android.app.AlertDialog.Builder(context);
            //OnClick item

            /*listview1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View myView, int position, long mylng) {
                    HashMap<String,String> map = (HashMap)parent.getItemAtPosition(position);

                    id_exsum = map.get("id_exsum");

                    //viewDetail.setIcon(android.R.drawable.star_on);
                    viewDetail.setTitle("COMMAND").setIcon(R.mipmap.delete);
                    viewDetail.setMessage("HAPUS LIST");

                    viewDetail.setPositiveButton("HAPUS",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    //TODOAutoGenerated
                                    deleteBim();
                                    startActivity(new Intent(context, MainActivityMhs.class));
                                }
                            });
                    viewDetail.setNegativeButton("BATAL",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {

                                }
                            });

                    viewDetail.show();
                }
            });
            */

            listview1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(context, Mhs_update_exsum.class);
                    HashMap<String,String> map = (HashMap)parent.getItemAtPosition(position);
                    String idB = map.get("id_exsum");
                    String nm = map.get("nim");
                    String jdul=map.get("judul_exsum");
                    String bdg=map.get("nama_bidang");

                    intent.putExtra(ConfigSum.MHS_IDEX, idB);
                    intent.putExtra(ConfigSum.MHS_NIM, nm);
                    intent.putExtra(ConfigSum.MHS_JUDUL, jdul);
                    intent.putExtra(ConfigSum.MHS_BIDANG1, bdg);
                    startActivity(intent);
                }
            });
        }
        catch (JSONException e){
            e.printStackTrace();
        }
    }

    public String getJSONUrl(String url){
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) { // Download OK
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download result..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }
}
