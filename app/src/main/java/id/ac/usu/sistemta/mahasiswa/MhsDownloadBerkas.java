package id.ac.usu.sistemta.mahasiswa;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import id.ac.usu.sistemta.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MhsDownloadBerkas extends Fragment {
    ImageView sempro,semhas,sidang;
    Activity context;

    public MhsDownloadBerkas() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        context=getActivity();
        return inflater.inflate(R.layout.mhs_download_berkas, container, false);
    }

    public void onStart() {
        super.onStart();

        sempro=(ImageView) context.findViewById(R.id.prop);
        semhas=(ImageView) context.findViewById(R.id.has);
        sidang=(ImageView) context.findViewById(R.id.sid);

        sempro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Boolean result = isDownloadManagerAvailable(context.getApplicationContext());
                if (result)
                    downloadFileSempro();
            }
        });

        semhas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Boolean result = isDownloadManagerAvailable(context.getApplicationContext());
                if (result)
                    downloadFileSemhas();
            }
        });

        sidang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Boolean result = isDownloadManagerAvailable(context.getApplicationContext());
                if (result)
                    downloadFileSidang();
            }
        });

    }

    @SuppressLint("NewApi")
    public void downloadFileSempro(){
        String file_sempro="sempro.zip" ;
        String DownloadUrl = "http://sistemta.esy.es/sistem_ta/mod/mhs/Data/"+file_sempro;
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(DownloadUrl));

        request.setDescription("Downloading");   //appears the same in Notification bar while downloading
        request.setTitle("Download");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        }
        request.setDestinationInExternalFilesDir(context.getApplicationContext(), null, file_sempro);

        // get download service and enqueue file
        DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);
    }

    @SuppressLint("NewApi")
    public void downloadFileSemhas(){
        String file_semhas="semhas.zip" ;
        String DownloadUrl = "http://sistemta.esy.es/sistem_ta/mod/mhs/Data/"+file_semhas;
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(DownloadUrl));

        request.setDescription("Downloading");
        request.setTitle("Download");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        }
        request.setDestinationInExternalFilesDir(context.getApplicationContext(),null,file_semhas);

        DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);
    }

    @SuppressLint("NewApi")
    public void downloadFileSidang(){
        String file_semhas="sidang.zip" ;
        String DownloadUrl = "http://sistemta.esy.es/sistem_ta/mod/mhs/Data/"+file_semhas;
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(DownloadUrl));

        request.setDescription("Downloading");
        request.setTitle("Download");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        }
        request.setDestinationInExternalFilesDir(context.getApplicationContext(),null,file_semhas);

        DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);
    }

    public static boolean isDownloadManagerAvailable(Context context) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            return true;
        }
        return false;
    }
}
