package id.ac.usu.sistemta.tataUsaha;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import id.ac.usu.sistemta.R;
import id.ac.usu.sistemta.config.ConfigAssesment;
import id.ac.usu.sistemta.config.ConfigJudul;
import id.ac.usu.sistemta.config.ConfigJudulTA;
import id.ac.usu.sistemta.config.ConfigMhs;
import id.ac.usu.sistemta.koorTA.koorta_assesment_confirm;
import id.ac.usu.sistemta.session.UserSessionManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class tu_judulMhs extends Fragment {

    Activity context;
    //SimpleAdapter sAdap;

    public tu_judulMhs() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        context=getActivity();
        return inflater.inflate(R.layout.tu_judul_mhs, container, false);
    }

    public void onStart() {

        super.onStart();

        if(android.os.Build.VERSION.SDK_INT > 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        ListView listview1 = (ListView)context.findViewById(R.id.listView2);
        listview1.setFastScrollEnabled(true);

        String url = "http://sistemta.esy.es/sistem_ta/mod/judul/select_judul.php";

        try{
            JSONArray data = new JSONArray(getJSONUrl(url));

            final ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
            HashMap<String,String> map;

            for (int i = 0; i<data.length(); i++){
                JSONObject c= data.getJSONObject(i);
                map = new HashMap<String,String>();
                map.put("id_judulta", c.getString("id_judulta"));
                map.put("nim", c.getString("nim"));
                map.put("judul_ta", c.getString("judul_ta"));
                map.put("nama_bidang", c.getString("nama_bidang"));
                MyArrList.add(map);
            }
            SimpleAdapter sAdap;

            sAdap = new SimpleAdapter(getActivity(), MyArrList, R.layout.activity_colomn_judul,
                    new String[] {"nim", "judul_ta", "nama_bidang" }, new int[]
                    {R.id.Colnim, R.id.Coljudul , R.id.Colbidang});

            listview1.setAdapter(sAdap);

            final AlertDialog.Builder viewDetail = new AlertDialog.Builder(context);
            //OnClick item

            listview1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(context, tu_update_judul.class);

                    HashMap<String, String> map = (HashMap) parent.getItemAtPosition(position);
//                  HashMap<String, String> info = (HashMap<String, String>) parent.getItemAtPosition(position);
                    String idJ = map.get("id_judulta");
                    String nm = map.get("nim");
                    String jd = map.get("judul_ta");
                    String bd = map.get("nama_bidang");

                    intent.putExtra(ConfigJudulTA.ASS_ID, idJ);
                    intent.putExtra(ConfigJudulTA.ASS_NIM, nm);
                    intent.putExtra(ConfigJudulTA.ASS_JUDUL, jd);
                    intent.putExtra(ConfigJudulTA.ASS_BIDANG, bd);

                    startActivity(intent);

                }
            });
        }
        catch (JSONException e){
            e.printStackTrace();
        }
    }

    public String getJSONUrl(String url){
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) { // Download OK
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download result..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }

}
