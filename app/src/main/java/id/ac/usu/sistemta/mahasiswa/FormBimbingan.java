package id.ac.usu.sistemta.mahasiswa;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.widget.DatePicker;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import id.ac.usu.sistemta.R;
import id.ac.usu.sistemta.config.ConfigMhsBimbingan;
import id.ac.usu.sistemta.handler.RequestHandler;
import id.ac.usu.sistemta.session.UserSessionManager;

public class FormBimbingan extends AppCompatActivity implements AdapterView.OnItemSelectedListener,View.OnClickListener {
    private EditText topik, hsil, DateEtxt;
    private Spinner nama;
    private Button cancel, done;
    TextView selection;
    private DatePickerDialog DatePickerDialog;
    private SimpleDateFormat dateFormatter;
    UserSessionManager session;
    private String name;

    ArrayList<String> listItems=new ArrayList<>();
    ArrayAdapter<String> aa;

    public FormBimbingan() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mhs_form_bimbingan);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        topik = (EditText) findViewById(R.id.editText4);
        hsil = (EditText) findViewById(R.id.editText5);
        cancel = (Button) findViewById(R.id.button4);
        done = (Button) findViewById(R.id.button3);

        selection = (TextView) findViewById(R.id.textView11);

        nama = (Spinner)findViewById(R.id.spinnerID);
        nama.setOnItemSelectedListener(this);
        aa = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,listItems);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        nama.setAdapter(aa);

        cancel.setOnClickListener(this);
        done.setOnClickListener(this);

        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        DateEtxt = (EditText) findViewById(R.id.editTgl);
        DateEtxt.setInputType(InputType.TYPE_NULL);
        DateEtxt.requestFocus();

        setDateTimeField();
    }

    public void onStart(){
        super.onStart();
        session = new UserSessionManager(getApplicationContext());

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();

        // get name
        name = user.get(UserSessionManager.KEY_NAME);

        BackTask bt=new BackTask();
        bt.execute();
    }

    private class BackTask extends AsyncTask<Void,Void,Void> {
        ArrayList<String> list;
        protected void onPreExecute(){
            super.onPreExecute();
            list = new ArrayList<>();
        }

        protected Void doInBackground(Void... params) {
            InputStream is = null;
            String result = "";
            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(ConfigMhsBimbingan.URL_GET_SPINNER+name);
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                // Get our response as a String.
                is = entity.getContent();
            }catch(IOException e){
                e.printStackTrace();
            }

            //convert response to string
            try{
                BufferedReader reader = new BufferedReader(new InputStreamReader(is,"utf-8"));
                String line = null;
                while ((line = reader.readLine()) != null) {
                    result+=line;
                }
                is.close();
                //result=sb.toString();
            }catch(Exception e){
                e.printStackTrace();
            }
            // parse json data
            try{
                JSONArray jArray =new JSONArray(result);
                for(int i=0;i<jArray.length();i++){
                    JSONObject jsonObject=jArray.getJSONObject(i);
                    // add interviewee name to arraylist
                    list.add(jsonObject.getString("nama_dosen"));
                }
            }
            catch(JSONException e){
                e.printStackTrace();
            }
            return null;
        }
        protected void onPostExecute(Void result){
            listItems.addAll(list);
            aa.notifyDataSetChanged();
        }
    }

    private void setDateTimeField() {
        DateEtxt.setOnClickListener(this);
        Calendar newCalendar = Calendar.getInstance();
        DatePickerDialog = new DatePickerDialog(this, new OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                DateEtxt.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
        Toast.makeText(getBaseContext(), nama.getSelectedItem().toString(),
                Toast.LENGTH_SHORT).show();
    }

    public void onNothingSelected(AdapterView<?> parent) {
        Toast.makeText(this, "Silahkan Pilih Dosen Pembimbing", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View v) {
        if(v==done){
            addMhs();
        }
        else if(v==cancel){
            startActivity(new Intent(this, FormBimbingan.class));
            finish();
        }
        else if(v==DateEtxt){
            DatePickerDialog.show();
        }
    }

    private void addMhs(){
        final String tgl= DateEtxt.getText().toString().trim();
        final String namadosen= nama.getSelectedItem().toString().trim();
        final String topikstring= topik.getText().toString().trim();
        final String hasilbimbingan=hsil.getText().toString().trim();

        class addMhs extends AsyncTask<Void,Void,String>{
            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(FormBimbingan.this,"Adding...","Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(FormBimbingan.this, s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(Void... v) {
                HashMap<String,String> params = new HashMap<>();
                params.put(ConfigMhsBimbingan.KEY_EMP_NAMAMHS,name);
                params.put(ConfigMhsBimbingan.KEY_EMP_NAMADOSEN,namadosen);
                params.put(ConfigMhsBimbingan.KEY_EMP_TGL, tgl);
                params.put(ConfigMhsBimbingan.KEY_EMP_TOPIK, topikstring);
                params.put(ConfigMhsBimbingan.KEY_EMP_HASIL, hasilbimbingan);

                RequestHandler rh = new RequestHandler();
                String res = rh.sendPostRequest(ConfigMhsBimbingan.URL_ADD, params);
                return res;
            }
        }
        if(namadosen.isEmpty()) {
            Toast.makeText(this, "Maaf Anda Belum Memiliki Dosen Pembimbing", Toast.LENGTH_LONG).show();
        }else if(topikstring.isEmpty()){
            Toast.makeText(getBaseContext(),"Masukkan topik diskusi", Toast.LENGTH_LONG).show();
        }else if(hasilbimbingan.isEmpty()){
            Toast.makeText(getBaseContext(),"Masukkan hasil bimbingan", Toast.LENGTH_LONG).show();
        }else if(topikstring.isEmpty() || hasilbimbingan.isEmpty()|| namadosen.isEmpty()){
            Toast.makeText(getBaseContext(),"Lengkapi Data", Toast.LENGTH_LONG).show();
        }
        else{
            addMhs ae = new addMhs();
            ae.execute();
            topik.setText(" ");
            hsil.setText(" ");
        }
    }
}
