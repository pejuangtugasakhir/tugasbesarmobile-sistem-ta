package id.ac.usu.sistemta.mahasiswa;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import android.widget.AdapterView;

import id.ac.usu.sistemta.R;
import id.ac.usu.sistemta.config.ConfigProfil;
import id.ac.usu.sistemta.config.ConfigSum;
import id.ac.usu.sistemta.handler.RequestHandler;
import id.ac.usu.sistemta.session.UserSessionManager;

public class ProfilMhs extends Fragment {
    UserSessionManager session;
    String name;
    Activity context;
    Button edit;
    TextView nimmhs,namamhs,alamatmhs,emailmhs,namadsn,nmadoping,nilaism,nilaisd,nilaitt;
    ImageView sempro,semhas,sidang;

    public ProfilMhs() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        context = getActivity();
        return inflater.inflate(R.layout.mhs_profil, container, false);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void onStart() {
        super.onStart();
        session = new UserSessionManager(context.getApplicationContext());

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();

        // get name
        name = user.get(UserSessionManager.KEY_NAME);

        nimmhs = (TextView) context.findViewById(R.id.textView1);
        namamhs = (TextView) context.findViewById(R.id.textView3);
        alamatmhs = (TextView) context.findViewById(R.id.alamat);
        emailmhs = (TextView) context.findViewById(R.id.email);
        namadsn = (TextView) context.findViewById(R.id.doping);
        nmadoping = (TextView) context.findViewById(R.id.dopenguji);
        nilaisd = (TextView) context.findViewById(R.id.sidang);
        nilaism = (TextView) context.findViewById(R.id.semhas);
        nilaitt = (TextView) context.findViewById(R.id.Nakhir);

        edit = (Button) context.findViewById(R.id.button52);
        edit.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent m = new Intent(context, EditProfil.class);
                                        startActivity(m);
                                    }
                                }
        );

        sempro=(ImageView) context.findViewById(R.id.prop);
        semhas=(ImageView) context.findViewById(R.id.has);
        sidang=(ImageView) context.findViewById(R.id.sid);

        sempro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Boolean result = isDownloadManagerAvailable(context.getApplicationContext());
                if (result)
                    downloadFileSempro();
            }
        });

        semhas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Boolean result = isDownloadManagerAvailable(context.getApplicationContext());
                if (result)
                    downloadFileSemhas();
            }
        });

        sidang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Boolean result = isDownloadManagerAvailable(context.getApplicationContext());
                if (result)
                    downloadFileSidang();
            }
        });

        getProfil();
        getDoping();
        getNilai();

        ListView listview1 = (ListView) context.findViewById(R.id.listView5);
        listview1.setFastScrollEnabled(true);

        String url = ConfigSum.URL_GET_HISTORY + name;

        try {
            JSONArray data = new JSONArray(getJSONUrl(url));

            final ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> map;

            for (int i = 0; i < data.length(); i++) {
                JSONObject c = data.getJSONObject(i);
                map = new HashMap<String, String>();
                map.put("judul_exsum", c.getString("judul_exsum"));
                map.put("status", c.getString("status"));
                map.put("tanggal_masuk_exsum", c.getString("tanggal_masuk_exsum"));
                map.put("keterangan_exsum", c.getString("keterangan_exsum"));
                MyArrList.add(map);
            }

            SimpleAdapter sAdap;

            sAdap = new SimpleAdapter(getActivity(), MyArrList, R.layout.activity_column_history,
                    new String[]{"judul_exsum", "status", "tanggal_masuk_exsum", "keterangan_exsum"}, new int[]
                    {R.id.ColJudul, R.id.ColStatus, R.id.ColTgl, R.id.ColKet});

            listview1.setAdapter(sAdap);
            listview1.setOnTouchListener(new ListView.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    int action = event.getAction();
                    switch (action) {
                        case MotionEvent.ACTION_DOWN:
                            // Disallow ScrollView to intercept touch events.
                            v.getParent().requestDisallowInterceptTouchEvent(true);
                            break;

                        case MotionEvent.ACTION_UP:
                            // Allow ScrollView to intercept touch events.
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                    // Handle ListView touch events.
                    v.onTouchEvent(event);
                    return true;
                }
            });

            final AlertDialog.Builder viewDetail = new AlertDialog.Builder(context);

            //OnClick item
            listview1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> myAdapter, View myView, int position, long mylng) {
                    String sJudul = MyArrList.get(position).get("judul_exsum").toString();
                    String sStatus = MyArrList.get(position).get("status").toString();
                    String sTgl = MyArrList.get(position).get("tanggal_masuk_exsum").toString();
                    String sKet = MyArrList.get(position).get("keterangan_exsum").toString();

                    //viewDetail.setIcon(android.R.drawable.star_on);
                    viewDetail.setTitle("DETAIL");
                    viewDetail.setMessage("Judul : " + sJudul + "\n\n" + "Status Judul :" + sStatus + "\n\n" +
                            "Tanggal Pengajuan:" + sTgl + "\n\n" +
                            "Keterangan Judul :" + sKet);

                    viewDetail.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    //TODOAutoGenerated

                                    dialog.dismiss();
                                }
                            });
                    viewDetail.show();
                }
            });


            ListView listview2 = (ListView) context.findViewById(R.id.listView2);
            listview2.setFastScrollEnabled(true);

            String url2 = ConfigSum.URL_GET_BORANG + name;

            try {
                JSONArray data2 = new JSONArray(getJSONUrl(url2));

                final ArrayList<HashMap<String, String>> MyArrList2 = new ArrayList<HashMap<String, String>>();
                HashMap<String, String> map2;

                for (int i = 0; i < data2.length(); i++) {
                    JSONObject c = data2.getJSONObject(i);
                    map2 = new HashMap<String, String>();
                    map2.put("id_jadwal", c.getString("id_jadwal"));
                    map2.put("nip", c.getString("nip"));
                    map2.put("nama_dosen", c.getString("nama_dosen"));
                    map2.put("tanggal_seminar", c.getString("tanggal_seminar"));
                    map2.put("waktu_seminar", c.getString("waktu_seminar"));
                    map2.put("judul_ta", c.getString("judul_ta"));
                    map2.put("kategori_kegiatan", c.getString("kategori_kegiatan"));
                    map2.put("ltr_belakang", c.getString("ltr_belakang"));
                    map2.put("rms_masalah", c.getString("rms_masalah"));
                    map2.put("tujuan_penelitian", c.getString("tujuan_penelitian"));
                    map2.put("landasan_teori", c.getString("landasan_teori"));
                    map2.put("metodologi", c.getString("metodologi"));
                    map2.put("dftr_pustaka", c.getString("dftr_pustaka"));
                    MyArrList2.add(map2);
                }

                SimpleAdapter sAdap2;

                sAdap2 = new SimpleAdapter(getActivity(), MyArrList2, R.layout.activity_column_borang,
                        new String[]{"nama_dosen", "tanggal_seminar", "waktu_seminar", "judul_ta", "kategori_kegiatan"}, new int[]
                        {R.id.ColNados, R.id.ColTgl, R.id.ColWaktu, R.id.ColJudul, R.id.ColStatus});

                listview2.setAdapter(sAdap2);
                listview2.setOnTouchListener(new ListView.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        int action = event.getAction();
                        switch (action) {
                            case MotionEvent.ACTION_DOWN:
                                // Disallow ScrollView to intercept touch events.
                                v.getParent().requestDisallowInterceptTouchEvent(true);
                                break;

                            case MotionEvent.ACTION_UP:
                                // Allow ScrollView to intercept touch events.
                                v.getParent().requestDisallowInterceptTouchEvent(false);
                                break;
                        }
                        // Handle ListView touch events.
                        v.onTouchEvent(event);
                        return true;
                    }
                });

                final AlertDialog.Builder viewDetail2 = new AlertDialog.Builder(context);

                //OnClick item
                listview2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> myAdapter, View myView, int position, long mylng) {
                        String sJudul = MyArrList2.get(position).get("judul_ta").toString();
                        String sStatus = MyArrList2.get(position).get("kategori_kegiatan").toString();
                        String sTgl = MyArrList2.get(position).get("tanggal_seminar").toString();
                        String sltrbl = MyArrList2.get(position).get("ltr_belakang").toString();
                        String srms = MyArrList2.get(position).get("rms_masalah").toString();
                        String stjn = MyArrList2.get(position).get("tujuan_penelitian").toString();
                        String slnd = MyArrList2.get(position).get("landasan_teori").toString();
                        String smtd = MyArrList2.get(position).get("metodologi").toString();
                        String sdft = MyArrList2.get(position).get("dftr_pustaka").toString();
                        String sdosen = MyArrList2.get(position).get("nama_dosen").toString();

                        viewDetail2.setTitle("BORANG MASUKAN SEMINAR").setIcon(R.mipmap.proposal);
                        viewDetail2.setMessage("Judul : " + sJudul + "\n\n" + "Kategori Seminar :" + sStatus + "\n\n" + "Masukan Oleh Dosen:" + sdosen + "\n\n" +
                                        "Tanggal Seminar:" + sTgl + "\n\n\n" +
                                        "PERBAIKAN LAPORAN" + "\n\n\n" + "Latar Belakang:" + sltrbl + "\n\n" +
                                        "Rumusan Masalah:" + srms + "\n\n" + "Tujuan Penelitian:" + stjn + "\n\n" +
                                        "Landasan Teori:" + slnd + "\n\n" + "Metodologi:" + smtd + "\n\n" +
                                        "Daftar Pustaka:" + sdft
                        );

                        viewDetail2.setPositiveButton("BACK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        //TODOAutoGenerated
                                        dialog.dismiss();
                                    }
                                });
                        viewDetail2.show();
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getJSONUrl(String url){
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) { // Download OK
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download result..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }

    private void getDoping(){
        class GetDop extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute(){
                super.onPreExecute();
                //loading = ProgressDialog.show(getActivity(),"Fetching....","Wait...",false,false);
            }
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                //loading.dismiss();
                showDop(s);
            }
            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequestParam(ConfigProfil.URL_SELECT_DOPING,name);
                return s;
            }
        }
        GetDop gp = new GetDop();
        gp.execute();
    }

    private void showDop(String json){
        try{
            JSONObject jsonObject = new JSONObject(json);
            JSONArray result = jsonObject.getJSONArray(ConfigProfil.TAG_JSON_ARRAY2);
            JSONObject c = result.getJSONObject(0);
            String namapenguji=c.getString(ConfigProfil.TAG_NAMADOSENPENG);

            nmadoping.setText(namapenguji);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getNilai(){
        class GetNil extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute(){
                super.onPreExecute();
                loading = ProgressDialog.show(getActivity(),"Fetching....","Wait...",false,false);
            }
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                showNil(s);
            }
            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequestParam(ConfigProfil.URL_SELECT_NILAI,name);
                return s;
            }
        }
        GetNil gp = new GetNil();
        gp.execute();
    }

    private void showNil(String json){
        try{
            JSONObject jsonObject = new JSONObject(json);
            JSONArray result = jsonObject.getJSONArray(ConfigProfil.TAG_JSON_ARRAY3);
            JSONObject c = result.getJSONObject(0);
            String nsemhas=c.getString(ConfigProfil.TAG_NILAISEMHAS);
            String nsidang=c.getString(ConfigProfil.TAG_NILAISIDANG);
            String ntotal=c.getString(ConfigProfil.TAG_NILAITOTAL);

            nilaism.setText(nsemhas);
            nilaisd.setText(nsidang);
            nilaitt.setText(ntotal);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getProfil(){
        class GetProf extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute(){
                super.onPreExecute();
                loading = ProgressDialog.show(getActivity(),"Fetching....","Wait...",false,false);
            }
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                showProf(s);
            }
            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequestParam(ConfigProfil.URL_SELECT_PROFIL,name);
                return s;
            }
        }
        GetProf gp = new GetProf();
        gp.execute();
    }

    private void showProf(String json){
        try{
            JSONObject jsonObject = new JSONObject(json);
            JSONArray result = jsonObject.getJSONArray(ConfigProfil.TAG_JSON_ARRAY);
            JSONObject c = result.getJSONObject(0);
            String nims = c.getString(ConfigProfil.TAG_NIM);
            String namas = c.getString(ConfigProfil.TAG_NAMA);
            String alamats = c.getString(ConfigProfil.TAG_ALAMAT);
            String emails = c.getString(ConfigProfil.TAG_EMAIL);
            String namadosen=c.getString(ConfigProfil.TAG_NAMADOSEN);

            namamhs.setText(namas);
            nimmhs.setText(nims);
            alamatmhs.setText(alamats);
            emailmhs.setText(emails);
            namadsn.setText(namadosen);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("NewApi")
    public void downloadFileSempro(){
        String file_sempro="sempro.zip" ;
        String DownloadUrl = "http://sistemta.esy.es/sistem_ta/mod/mhs/Data/"+file_sempro;
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(DownloadUrl));

        request.setDescription("Downloading");   //appears the same in Notification bar while downloading
        request.setTitle("Download");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        }
        request.setDestinationInExternalFilesDir(context.getApplicationContext(), null, file_sempro);

        // get download service and enqueue file
        DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);
    }

    @SuppressLint("NewApi")
    public void downloadFileSemhas(){
        String file_semhas="semhas.zip" ;
        String DownloadUrl = "http://sistemta.esy.es/sistem_ta/mod/mhs/Data/"+file_semhas;
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(DownloadUrl));

        request.setDescription("Downloading");
        request.setTitle("Download");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        }
        request.setDestinationInExternalFilesDir(context.getApplicationContext(),null,file_semhas);

        DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);
    }

    @SuppressLint("NewApi")
    public void downloadFileSidang(){
        String file_semhas="sidang.zip" ;
        String DownloadUrl = "http://sistemta.esy.es/sistem_ta/mod/mhs/Data/"+file_semhas;
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(DownloadUrl));

        request.setDescription("Downloading");
        request.setTitle("Download");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        }
        request.setDestinationInExternalFilesDir(context.getApplicationContext(),null,file_semhas);

        DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);
    }

    public static boolean isDownloadManagerAvailable(Context context) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            return true;
        }
        return false;
    }
}
