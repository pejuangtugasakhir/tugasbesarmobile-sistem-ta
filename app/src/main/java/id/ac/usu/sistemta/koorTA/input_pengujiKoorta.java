package id.ac.usu.sistemta.koorTA;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import id.ac.usu.sistemta.R;
import id.ac.usu.sistemta.config.ConfigInputPenguji;
import id.ac.usu.sistemta.config.ConfigMhsBimbingan;
import id.ac.usu.sistemta.handler.RequestHandler;
import id.ac.usu.sistemta.session.UserSessionManager;

public class input_pengujiKoorta extends AppCompatActivity implements View.OnClickListener{
    private EditText nimMhs;
    private EditText namaMhs;
    private EditText pembimbingsatu;
    private Button btnSubmid,btnReset;
    private String strNimMhs,strNamaMhs,strPembimbingSatu,strPembimbingDua;
    private Spinner spinner1,spinner2;
    UserSessionManager session;
    ArrayList<String> listItems=new ArrayList<>();
    ArrayAdapter<String> aa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.koorta_input_penguji);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        strNimMhs = intent.getStringExtra(ConfigInputPenguji.TAG_NIMMHS);
        strNamaMhs = intent.getStringExtra(ConfigInputPenguji.TAG_NAMAMHS);
        strPembimbingSatu = intent.getStringExtra(ConfigInputPenguji.TAG_NAMADOPINGSATU);

        nimMhs = (EditText)findViewById(R.id.editTextNimMhs);
        namaMhs = (EditText)findViewById(R.id.editTextNamaMhs);
        pembimbingsatu = (EditText)findViewById(R.id.editTextPembimbing1);

        btnSubmid = (Button) findViewById(R.id.submitt);
        btnReset = (Button) findViewById(R.id.resett);

        btnSubmid.setOnClickListener(this);
        btnReset.setOnClickListener(this);

        nimMhs.setText(strNimMhs);
        namaMhs.setText(strNamaMhs);
        pembimbingsatu.setText(strPembimbingSatu);

        spinner1 = (Spinner)findViewById(R.id.spinner3);
        spinner2 = (Spinner)findViewById(R.id.spinner4);

        aa = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,listItems);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(aa);
        spinner2.setAdapter(aa);

        getInputPenguji();
    }

    private class BackTask extends AsyncTask<Void,Void,Void> {
        ArrayList<String> list;
        protected void onPreExecute(){
            super.onPreExecute();
            list = new ArrayList<>();
        }

        protected Void doInBackground(Void... params) {
            InputStream is = null;
            String result = "";
            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost("http://sistemta.esy.es/sistem_ta/mod/dosen/select_dosen.php");
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                // Get our response as a String.
                is = entity.getContent();
            }catch(IOException e){
                e.printStackTrace();
            }
            //convert response to string
            try{
                BufferedReader reader = new BufferedReader(new InputStreamReader(is,"utf-8"));
                String line = null;
                while ((line = reader.readLine()) != null) {
                    result+=line;
                }
                is.close();
                //result=sb.toString();
            }catch(Exception e){
                e.printStackTrace();
            }
            // parse json data
            try{
                JSONArray jArray =new JSONArray(result);
                for(int i=0;i<jArray.length();i++){
                    JSONObject jsonObject=jArray.getJSONObject(i);
                    // add interviewee name to arraylist
                    list.add(jsonObject.getString("nama_dosen"));
                }
            }
            catch(JSONException e){
                e.printStackTrace();
            }
            return null;
        }
        protected void onPostExecute(Void result){
            listItems.addAll(list);
            aa.notifyDataSetChanged();
        }
    }

    private void getInputPenguji(){
        class GetBim extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute(){
                super.onPreExecute();
                loading = ProgressDialog.show(input_pengujiKoorta.this,"Fetching....","Wait...",false,false);
            }
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                showInpPenguji(s);
            }
            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequestParam(ConfigInputPenguji.URL_SELECT_PENGUJI_LIST,strNimMhs);
                return s;
            }
        }
        GetBim ge = new GetBim();
        ge.execute();
    }

    private void showInpPenguji(String json){
        try{
            JSONObject jsonObject = new JSONObject(json);
            JSONArray result = jsonObject.getJSONArray(ConfigMhsBimbingan.TAG_JSON_ARRAY);
            JSONObject c = result.getJSONObject(0);
            String Nim = c.getString(ConfigInputPenguji.TAG_NIMMHS);
            String Nama = c.getString(ConfigInputPenguji.TAG_NAMAMHS);
            String PembimbingSatu = c.getString(ConfigInputPenguji.TAG_NAMADOPINGSATU);

            nimMhs.setText(Nim);
            namaMhs.setText(Nama);
            pembimbingsatu.setText(PembimbingSatu);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void onClick(View v) {
        if(v == btnSubmid){
            updatePenguji();
        }

        if(v == btnReset){
            reset();
        }
    }

    public void reset(){
        spinner1.getEmptyView();
        spinner2.getEmptyView();
    }

    public void onStart(){
        super.onStart();
        session = new UserSessionManager(getApplicationContext());

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();

        // get name
        strNamaMhs = user.get(UserSessionManager.KEY_NAME);

        BackTask bt=new BackTask();
        bt.execute();
    }


    private void updatePenguji() {
        final String PENGUJIsatu = spinner1.getSelectedItem().toString().trim();
        final String PENGUJIDua = spinner2.getSelectedItem().toString().trim();

        class UpdateMhs extends AsyncTask<Void,Void,String>{
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(input_pengujiKoorta.this,"Updating...","Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(input_pengujiKoorta.this, s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(Void... params) {
                HashMap<String,String> hashMap = new HashMap<>();
                hashMap.put(ConfigInputPenguji.KEY_EMP_NIM,strNimMhs);
                hashMap.put(ConfigInputPenguji.KEY_EMP_DOSENPENGUJISATU,PENGUJIsatu);
                hashMap.put(ConfigInputPenguji.KEY_EMP_DOSENPENGUJIDUA,PENGUJIDua);

                RequestHandler rh = new RequestHandler();
                String s = rh.sendPostRequest(ConfigInputPenguji.URL_INPUT_PENGUJI, hashMap);
                return s;
            }
        }

        UpdateMhs ue = new UpdateMhs();
        ue.execute();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_input_penguji_koorta, menu);
        return true;
    }

}
