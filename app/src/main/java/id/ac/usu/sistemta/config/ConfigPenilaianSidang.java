package id.ac.usu.sistemta.config;

/**
 * Created by asus on 1/1/2016.
 */
public class ConfigPenilaianSidang {
    //address
    public static final String URL_UPDATE_SIDANG ="http://sistemta.esy.es/sistem_ta/mod/dosen/update_borang_penilaian_sidang.php";

    //key
    public static final String KEY_UPD_NAMADOSEN = "username";
    public static final String KEY_UPD_NAMAMHS = "nama_mhs";
    public static final String KEY_UPD_IDBIM = "id_jadwal";
    public static final String KEY_UPD_TGL_SIDANG = "tgl_nilai_sidang";
    public static final String KEY_UPD_TOPIK = "judul_ta";
    public static final String KEY_UPD_SEMHAS = "n_semhas";
    public static final String KEY_UPD_SIDANG = "n_sidang";
    public static final String KEY_UPD_NILAI_ID = "id_nilai";
    public static final String KEY_UPD_TGL_SEMHAS = "tgl_nilai_semhas";
}
