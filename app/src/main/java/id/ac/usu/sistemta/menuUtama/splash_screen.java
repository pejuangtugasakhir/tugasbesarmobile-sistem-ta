package id.ac.usu.sistemta.menuUtama;

/**
 * Created by yeni on 10/22/2015.
 */

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import id.ac.usu.sistemta.R;
//import id.ac.usu.sistemta.tataUsaha.MainActivityTU;
import id.ac.usu.sistemta.tataUsaha.tu_tambah_jadwal;

public class splash_screen extends Activity {
    //Set durasi splashscreen

    private static  int splashInterval = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        setContentView(R.layout.splash_screen);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method sub
//                Intent i = new Intent(splash_screen.this,tu_tambah_jadwal.class);

                Intent i = new Intent(splash_screen.this, SistemTA.class);
//                Intent i = new Intent(splash_screen.this, koorta_assesment_view_new_entry_exum.class);

                startActivity(i);
                //Jeda
                this.finish();
            }
            private void finish() {
                // TODO Auto-generated method stub

            }
        }, splashInterval);

    };

}