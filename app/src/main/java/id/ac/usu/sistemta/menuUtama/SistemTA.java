package id.ac.usu.sistemta.menuUtama;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import id.ac.usu.sistemta.R;

public class SistemTA extends AppCompatActivity implements View.OnClickListener {

    ImageView user,repository,jadwal;
    TextView l,r,j;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_sistemta);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        user=(ImageView)findViewById(R.id.imageView2);
        user.setOnClickListener(this);
        repository=(ImageView)findViewById(R.id.imageView3);
        repository.setOnClickListener(this);
        jadwal=(ImageView)findViewById(R.id.imageView4);
        jadwal.setOnClickListener(this);

        l=(TextView)findViewById(R.id.l);
        l.setOnClickListener(this);
        r=(TextView)findViewById(R.id.r);
        r.setOnClickListener(this);
        j=(TextView)findViewById(R.id.j);
        j.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if(v==user || v==l){
            Intent intent = new Intent(this,Login.class);
            startActivity(intent);
        }
        if (v==repository || v==r){
            Intent intent = new Intent(this,Repository.class);
            startActivity(intent);
        }
        if (v==jadwal || v==j){
            Intent intent = new Intent(this,JadwalTA.class);
            startActivity(intent);
        }
    }

}
