package id.ac.usu.sistemta.tataUsaha;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.HashMap;

import id.ac.usu.sistemta.R;
import id.ac.usu.sistemta.config.ConfigDosen;
import id.ac.usu.sistemta.handler.RequestHandler;

public class tu_tambah_dosen extends AppCompatActivity implements View.OnClickListener {

    private EditText editTextNip;
    private EditText editTextNama;
    private EditText editTextAlamat;
    private EditText editTextEmail;
    private EditText editTextUsername;
    private EditText editTextPassword;

    private Button addDosen;
    private Button reset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tu_tambah_dosen);

        editTextNip = (EditText)findViewById(R.id.eNip);
        editTextNama = (EditText)findViewById(R.id.eNama);
        editTextAlamat = (EditText)findViewById(R.id.eAlamat);
        editTextEmail = (EditText)findViewById(R.id.eEmail);
        editTextUsername = (EditText)findViewById(R.id.eUsername);
        editTextPassword = (EditText)findViewById(R.id.ePassword);

        addDosen = (Button) findViewById(R.id.add_dosen);
        reset = (Button) findViewById(R.id.btnReset);

        addDosen.setOnClickListener(this);
        reset.setOnClickListener(this);
        editTextNip.requestFocus();
    }

    public void onClick(View v) {
        if(v == addDosen){
            addDosen();
        }
        if(v ==reset){
            reset();
        }
    }

    private void addDosen(){

        final String nip = editTextNip.getText().toString().trim();
        final String nama = editTextNama.getText().toString().trim();
        final String alamat = editTextAlamat.getText().toString().trim();
        final String email = editTextEmail.getText().toString().trim();
        final String username = editTextUsername.getText().toString().trim();
        final String password = editTextPassword.getText().toString().trim();

        class AddDosen extends AsyncTask<Void,Void,String>{
            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(tu_tambah_dosen.this,"Adding...","Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(tu_tambah_dosen.this, s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(Void... v) {
                HashMap<String,String> params = new HashMap<>();
                params.put(ConfigDosen.KEY_EMP_NIP,nip);
                params.put(ConfigDosen.KEY_EMP_NAMA,nama);
                params.put(ConfigDosen.KEY_EMP_ALAMAT, alamat);
                params.put(ConfigDosen.KEY_EMP_EMAIL, email);
                params.put(ConfigDosen.KEY_EMP_USERNAME, username);
                params.put(ConfigDosen.KEY_EMP_PASSWORD, password);

                RequestHandler rh = new RequestHandler();
                String res = rh.sendPostRequest(ConfigDosen.URL_ADD, params);
                return res;
            }
        }
        if(nip.isEmpty()){
            Toast.makeText(getBaseContext(),"Masukkan NIP", Toast.LENGTH_LONG).show();
        }else if(nama.isEmpty()){
            Toast.makeText(getBaseContext(),"Masukkan Nama", Toast.LENGTH_LONG).show();
        }else if(alamat.isEmpty()){
            Toast.makeText(getBaseContext(),"Masukkan Alamat", Toast.LENGTH_LONG).show();
        }else if(email.isEmpty()){
            Toast.makeText(getBaseContext(),"Masukkan Email", Toast.LENGTH_LONG).show();
        }else if(username.isEmpty()){
            Toast.makeText(getBaseContext(),"Masukkan Username", Toast.LENGTH_LONG).show();
        }else if(password.isEmpty()){
            Toast.makeText(getBaseContext(),"Masukkan Password", Toast.LENGTH_LONG).show();
        }else if(nip.isEmpty() || nama.isEmpty() || alamat.isEmpty() || email.isEmpty() || username.isEmpty() || email.isEmpty()){
            Toast.makeText(getBaseContext(),"Lengkapi Data", Toast.LENGTH_LONG).show();
        }
        else{
            AddDosen ae = new AddDosen();
            ae.execute();

            editTextNip.setText(" ");
            editTextNama.setText(" ");
            editTextAlamat.setText(" ");
            editTextEmail.setText(" ");
            editTextUsername.setText(" ");
            editTextPassword.setText(" ");
            editTextNip.requestFocus();
        }

    }
    private void reset() {
        editTextNip.setText(" ");
        editTextNama.setText(" ");
        editTextAlamat.setText(" ");
        editTextEmail.setText(" ");
        editTextUsername.setText(" ");
        editTextPassword.setText(" ");
        editTextNip.requestFocus();
    }
}
