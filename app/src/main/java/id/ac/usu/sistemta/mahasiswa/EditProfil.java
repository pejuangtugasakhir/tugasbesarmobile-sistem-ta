package id.ac.usu.sistemta.mahasiswa;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import id.ac.usu.sistemta.R;
import id.ac.usu.sistemta.config.ConfigProfil;
import id.ac.usu.sistemta.handler.RequestHandler;
import id.ac.usu.sistemta.session.UserSessionManager;

public class EditProfil extends AppCompatActivity implements View.OnClickListener{
    private EditText alamat,email,passbaru,passlama;
    private TextView namamhs,nimmhs;
    private Button edit,cancel;
    UserSessionManager session;
    String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mhs_edit_profil);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        namamhs = (TextView)findViewById(R.id.namamhs);
        nimmhs=(TextView)findViewById(R.id.nim);
        alamat=(EditText) findViewById(R.id.alamat);
        email=(EditText)findViewById(R.id.email);
        passbaru=(EditText)findViewById(R.id.passbaru);
        passlama=(EditText)findViewById(R.id.pass);

        edit=(Button) findViewById(R.id.buttonedit);
        cancel=(Button)findViewById(R.id.buttoncan);
        edit.setOnClickListener(this);
        cancel.setOnClickListener(this);

        getProfil();
    }

    private void getProfil(){
        class GetProf extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute(){
                super.onPreExecute();
                loading = ProgressDialog.show(EditProfil.this,"Fetching....","Wait...",false,false);
            }
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                showProf(s);
            }
            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequestParam(ConfigProfil.URL_SELECT_PROFIL,name);
                return s;
            }
        }
        GetProf gp = new GetProf();
        gp.execute();
    }


    private void showProf(String json){
        try{
            JSONObject jsonObject = new JSONObject(json);
            JSONArray result = jsonObject.getJSONArray(ConfigProfil.TAG_JSON_ARRAY);
            JSONObject c = result.getJSONObject(0);
            String nims = c.getString(ConfigProfil.TAG_NIM);
            String namas = c.getString(ConfigProfil.TAG_NAMA);
            String alamats = c.getString(ConfigProfil.TAG_ALAMAT);
            String emails = c.getString(ConfigProfil.TAG_EMAIL);

            namamhs.setText(namas);
            nimmhs.setText(nims);
            alamat.setText(alamats);
            email.setText(emails);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_profil, menu);
        return true;
    }

    public void onStart(){

        super.onStart();
        session = new UserSessionManager(getApplicationContext());

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();

        // get name
        name = user.get(UserSessionManager.KEY_NAME);
    }

    @Override
    public void onClick(View v) {
        if(v==edit){
            editProfil();
        }
        else if(v==cancel){
            startActivity(new Intent(this, EditProfil.class));
            finish();
        }
    }

    private void editProfil(){
        final String alamatstring= alamat.getText().toString().trim();
        final String emailstring= email.getText().toString().trim();
        final String passlma= passlama.getText().toString().trim();
        final String passbru= passbaru.getText().toString().trim();

        class addProfil extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(EditProfil.this,"Edit...","Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(EditProfil.this, s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(Void... v) {
                HashMap<String,String> params = new HashMap<>();
                params.put(ConfigProfil.KEY_EMP_UNAMA,name);
                params.put(ConfigProfil.KEY_EMP_ALAMAT,alamatstring);
                params.put(ConfigProfil.KEY_EMP_EMAIL,emailstring);
                params.put(ConfigProfil.KEY_EMP_PASSBARU, passbru);
                params.put(ConfigProfil.KEY_EMP_PASSLAMA, passlma);

                RequestHandler rh = new RequestHandler();
                String res = rh.sendPostRequest(ConfigProfil.URL_UPDATE, params);
                return res;
            }
        }

        addProfil ap = new addProfil();
        ap.execute();
    }
}
