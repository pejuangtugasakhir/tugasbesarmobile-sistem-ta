package id.ac.usu.sistemta.config;

/**
 * Created by yeni on 11/27/2015.
 */
public class ConfigMhs {
    //address

    public static final String URL_ADD ="http://sistemta.esy.es/sistem_ta/mod/mhs/insert_mhs.php";
//    public static final String URL_GET_ALL = "http://sistemta.esy.es/sistem_ta/mod/dosen/select_dosen.php";
    public static final String URL_GET_MHS = "http://sistemta.esy.es/sistem_ta/mod/mhs/select_mhs_one.php?nim=";
    public static final String URL_UPDATE_MHS = "http://sistemta.esy.es/sistem_ta/mod/mhs/update_mhs.php";
    public static final String URL_DELETE_MHS = "http://sistemta.esy.es/sistem_ta/mod/mhs/delete_mhs.php?nim=";

    //Keys that will be used to send the request to php scripts
    public static final String KEY_EMP_NIM = "nim";
    public static final String KEY_EMP_NAMA = "nama_mhs";
    public static final String KEY_EMP_JENIS = "jenis_kelamin";
    public static final String KEY_EMP_ALAMAT = "alamat_mhs";
    public static final String KEY_EMP_EMAIL = "email";
    public static final String KEY_EMP_USERNAME = "username";
    public static final String KEY_EMP_PASSWORD = "password";

    //JSON Tags
    public static final String TAG_JSON_ARRAY="result";
    public static final String TAG_NIM = "nim";
    public static final String TAG_NAMA = "nama_mhs";
    public static final String TAG_JENIS = "jenis_kelamin";
    public static final String TAG_ALAMAT = "alamat_mhs";
    public static final String TAG_EMAIL = "email";

    public static final String MHS_NIM = "nim";
    public static final String MHS_NAMA= "nama_mhs";
    public static final String MHS_JENIS = "jenis_kelamin";
    public static final String MHS_ALAMAT = "alamat_mhs";
    public static final String MHS_EMAIL = "email";
}
