package id.ac.usu.sistemta.config;

/**
 * Created by Citi Fatimah on 1/12/2016.
 */
public class ConfigKoorTAPenilaianLihat
{
    public static final String URL_ADD ="http://sistemta.esy.es/sistem_ta/mod/mhs/insert_summary.php";
    public static final String URL_GET_PENILAIAN="http://sistemta.esy.es/sistem_ta/mod/koorta/select_penilaian.php";

    //Keys that will be used to send the request to php scripts

    public static final String KEY_EMP_TGL = "tanggal_masuk_exsum";
    public static final String KEY_EMP_NIM= "nim";

    //JSON Tags
    public static final String TAG_JSON_ARRAY="result";
    public static final String TAG_NIM = "nim";

    public static final String MHS_NAMAMHS = "username";
    public static final String MHS_JUDUL = "judul";
    public static final String MHS_REKOM1 = "rekomendasi_exsum";
    public static final String MHS_BIDANG1 = "nama_bidang";
    public static final String MHS_TGL = "tanggal_masuk_exsum";
    public static final String MHS_NIM = "nim";
    public static final String MHS_NAMADSN= "nama_dosen";
    public static final String MHS_IDEX = "id_exsum";
}
