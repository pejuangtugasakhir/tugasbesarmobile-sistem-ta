package id.ac.usu.sistemta.tataUsaha;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TimePicker;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import id.ac.usu.sistemta.R;
import id.ac.usu.sistemta.config.ConfigJadwal;
import id.ac.usu.sistemta.config.ConfigMhs;
import id.ac.usu.sistemta.handler.RequestHandler;

public class tu_update_jadwal extends AppCompatActivity implements View.OnClickListener{
    private EditText DateEtxt;
    private android.app.DatePickerDialog DatePickerDialog;
    private SimpleDateFormat dateFormatter;

    private String nim;
    private String tgl;
    private String wkt;
    private String status;
    private String id_jadwal;

    private EditText enim;
    private EditText etgl;
    private EditText ewkt;

    private RadioGroup radioStatus;
    private RadioButton radioButtonStatus;
    private RadioButton radioButtonProposal;
    private RadioButton radioButtonHasil;
    private RadioButton radioButtonSidang;

    private Button btnUpdate;
    private Button btnDelete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tu_update_jadwal);

        Intent intent = getIntent();

        nim = intent.getStringExtra(ConfigJadwal.JADWAL_NIM);
        tgl = intent.getStringExtra(ConfigJadwal.JADWAL_TGL);
        wkt = intent.getStringExtra(ConfigJadwal.JADWAL_WAKTU);
        status = intent.getStringExtra(ConfigJadwal.JADWAL_STATUS);
        id_jadwal = intent.getStringExtra(ConfigJadwal.JADWAL_ID);

        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        findViewsById();

        setDateTimeField();

        etgl = (EditText)findViewById(R.id.editTgl);
        ewkt = (EditText)findViewById(R.id.editWaktu);
        enim = (EditText)findViewById(R.id.nim);

        radioStatus = (RadioGroup)findViewById(R.id.radioStatus);
        radioButtonProposal = (RadioButton)findViewById(R.id.radioProposal);
        radioButtonHasil = (RadioButton)findViewById(R.id.radioHasil);
        radioButtonSidang = (RadioButton)findViewById(R.id.radioSidang);

        btnUpdate = (Button) findViewById(R.id.btn_update);
        btnDelete = (Button) findViewById(R.id.btn_delete);

        enim.setText(nim);
        etgl.setText(tgl);
        ewkt.setText(wkt);

        if(status.equals("sempro")){
            radioButtonProposal.setChecked(true);
            radioButtonHasil.setChecked(false);
            radioButtonSidang.setChecked(false);
        }else if(status.equals("semhas")){
            radioButtonProposal.setChecked(false);
            radioButtonHasil.setChecked(true);
            radioButtonSidang.setChecked(false);
        }else{
            radioButtonProposal.setChecked(false);
            radioButtonHasil.setChecked(false);
            radioButtonSidang.setChecked(true);
        }

        etgl.requestFocus();
        getMhs(nim, status);
        btnDelete.setOnClickListener(this);
        btnUpdate.setOnClickListener(this);

        ewkt.setInputType(InputType.TYPE_NULL);
        ewkt.requestFocus();
        ewkt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(tu_update_jadwal.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        ewkt.setText(new StringBuilder().append(pad(selectedHour)).append(":").append(pad(selectedMinute)));
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });
    }

    private static String pad(int c){
        if(c>=10)
            return String.valueOf(c);
        else
            return "0" + String.valueOf(c);
    }

    private void getMhs(String hnim, String hstatus) {
        class HttpGetAsyncTask extends AsyncTask<String, Void, String> {
            protected String doInBackground(String... params) {
                String paramNim = params[0];
                String paramStatus = params[1];

                HttpClient httpClient = new DefaultHttpClient();

                HttpGet httpGet = new HttpGet("http://sistemta.esy.es/sistem_ta/mod/jadwal/select_jadwal_one.php?nim=" + paramNim + "&status_kegiatan=" + paramStatus);

                try{
                    HttpResponse httpResponse = httpClient.execute(httpGet);
                    InputStream inputStream = httpResponse.getEntity().getContent();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                    StringBuilder stringBuilder = new StringBuilder();

                    String bufferedStrChunk = null;

                    while((bufferedStrChunk = bufferedReader.readLine()) != null){
                        stringBuilder.append(bufferedStrChunk);
                    }
                    return stringBuilder.toString();
                } catch (ClientProtocolException cpe) {
                    System.out.println("Exceptionrates caz of httpResponse :" + cpe);
                    cpe.printStackTrace();
                } catch (IOException ioe) {
                    System.out.println("Secondption generates caz of httpResponse :" + ioe);
                    ioe.printStackTrace();
                }

                return null;
            }
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                showMhs(s);
            }
        }

        HttpGetAsyncTask httpGetAsyncTask = new HttpGetAsyncTask();

        httpGetAsyncTask.execute(hnim, hstatus);
    }

    private void showMhs(String json){
        try{
            JSONObject jsonObject = new JSONObject(json);
            JSONArray result = jsonObject.getJSONArray(ConfigJadwal.TAG_JSON_ARRAY);
            JSONObject c = result.getJSONObject(0);
            String nim = c.getString(ConfigJadwal.TAG_NIM);
            String tanggal = c.getString(ConfigJadwal.TAG_TGL);
            String waktu = c.getString(ConfigJadwal.TAG_WAKTU);
            String status = c.getString(ConfigJadwal.TAG_STATUS);

            etgl.setText(tanggal);
            enim.setText(nim);
            ewkt.setText(waktu);

            if(status.equals("sempro")){
                radioButtonProposal.setChecked(true);
                radioButtonHasil.setChecked(false);
                radioButtonSidang.setChecked(false);
            }if(status.equals("semhas")){
                radioButtonProposal.setChecked(false);
                radioButtonHasil.setChecked(true);
                radioButtonSidang.setChecked(false);
            }if(status.equals("sidang")){
                radioButtonProposal.setChecked(false);
                radioButtonHasil.setChecked(false);
                radioButtonSidang.setChecked(true);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void findViewsById() {
        DateEtxt = (EditText) findViewById(R.id.editTgl);
        DateEtxt.setInputType(InputType.TYPE_NULL);
        DateEtxt.requestFocus();
    }

    private void setDateTimeField() {
        DateEtxt.setOnClickListener(this);
        Calendar newCalendar = Calendar.getInstance();
        DatePickerDialog = new DatePickerDialog(this, new android.app.DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                DateEtxt.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    public void onClick(View v) {

        if(v==DateEtxt){
            DatePickerDialog.show();
        }
        if(v == btnUpdate){
            updateJadwal();
        }
        if(v == btnDelete){
            confirmDeleteJadwal();
        }
    }

    private void updateJadwal(){
        final String tanggal = etgl.getText().toString();
        final String waktu = ewkt.getText().toString();

        int selectId = radioStatus.getCheckedRadioButtonId();
        radioButtonStatus = (RadioButton)findViewById(selectId);
        final String status = radioButtonStatus.getText().toString();

        class UpdateJadwal extends AsyncTask<Void,Void,String>{
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(tu_update_jadwal.this,"Updating...","Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(tu_update_jadwal.this, s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(Void... params) {
                HashMap<String,String> hashMap = new HashMap<>();
                hashMap.put(ConfigJadwal.KEY_EMP_ID,id_jadwal);
                hashMap.put(ConfigJadwal.KEY_EMP_NIM,nim);
                hashMap.put(ConfigJadwal.KEY_EMP_TGL,tanggal);
                hashMap.put(ConfigJadwal.KEY_EMP_WAKTU,waktu);
                hashMap.put(ConfigJadwal.KEY_EMP_STATUS,status);

                RequestHandler rh = new RequestHandler();

                String s = rh.sendPostRequest(ConfigJadwal.URL_UPDATE_JDWL, hashMap);

                return s;
            }
        }

        UpdateJadwal ue = new UpdateJadwal();
        ue.execute();
    }

    private void confirmDeleteJadwal(){
        int selectId = radioStatus.getCheckedRadioButtonId();
        radioButtonStatus = (RadioButton)findViewById(selectId);
        final String sStatus = radioButtonStatus.getText().toString();

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Are you sure you want to delete this jadwal?");

        alertDialogBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        deleteJadwal(nim, sStatus);
                        startActivity(new Intent(tu_update_jadwal.this,MainActivityTU.class));
                    }
                });

        alertDialogBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void deleteJadwal(String unim, String ustatus){
        class HttpGetAsyncTask extends AsyncTask<String, Void, String> {
            protected String doInBackground(String... params) {
                String paramNim = params[0];
                String paramStatus = params[1];

                HttpClient httpClient = new DefaultHttpClient();

                HttpGet httpGet = new HttpGet("http://sistemta.esy.es/sistem_ta/mod/jadwal/delete_jadwal.php?nim=" + paramNim + "&status_kegiatan=" + paramStatus);

                try{
                    HttpResponse httpResponse = httpClient.execute(httpGet);
                    InputStream inputStream = httpResponse.getEntity().getContent();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                    StringBuilder stringBuilder = new StringBuilder();

                    String bufferedStrChunk = null;

                    while((bufferedStrChunk = bufferedReader.readLine()) != null){
                        stringBuilder.append(bufferedStrChunk);
                    }
                    return stringBuilder.toString();
                } catch (ClientProtocolException cpe) {
                    System.out.println("Exceptionrates caz of httpResponse :" + cpe);
                    cpe.printStackTrace();
                } catch (IOException ioe) {
                    System.out.println("Secondption generates caz of httpResponse :" + ioe);
                    ioe.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                if(s.equals("Jadwal Deleted Successfully")){
                    Toast.makeText(getApplicationContext(), "Jadwal Deleted Successfully", Toast.LENGTH_LONG).show();
                    Intent  intent = new Intent(tu_update_jadwal.this, MainActivityTU.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "Could Not Delete Jadwal, Try Again . . .", Toast.LENGTH_LONG).show();
                }
            }
        }

        HttpGetAsyncTask httpGetAsyncTask = new HttpGetAsyncTask();

        httpGetAsyncTask.execute(unim,ustatus);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tu_update_jadwal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
