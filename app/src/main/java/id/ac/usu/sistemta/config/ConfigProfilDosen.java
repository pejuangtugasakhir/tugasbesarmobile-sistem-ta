package id.ac.usu.sistemta.config;

/**
 * Created by acer on 06/12/2015.
 */
public class ConfigProfilDosen {
    //address
    public static final String URL_SELECT_PROFIL_DOSEN = "http://sistemta.esy.es/sistem_ta/mod/dosen/select_profile.php?username=";
    public static final String URL_UPDATE_DOSEN = "http://sistemta.esy.es/sistem_ta/mod/dosen/update_profildosen.php";
    public static final String URL_SELECT_MHS_UJI = "http://sistemta.esy.es/sistem_ta/mod/dosen/select_mahasiswa_uji.php?username=";

    //Keys that will be used to send the request to php scripts
    public static final String KEY_EMP_ALAMAT_DOSEN = "alamat_dosen";
    public static final String KEY_EMP_EMAIL_DOSEN = "email_dosen";
    public static final String KEY_EMP_PASSBARU_DOSEN = "passwordbaru";
    public static final String KEY_EMP_PASSLAMA_DOSEN = "password";
    public static final String KEY_EMP_NAMA_DOSEN = "nama_dosen";
    public static final String KEY_EMP_UNAMA = "username";
    public static final String KEY_EMP_NIP = "nip";

    //JSON Tags
    public static final String TAG_JSON_ARRAY="result";
    public static final String TAG_JSON_ARRAY2="result2";
    public static final String TAG_JSON_ARRAY3="result3";
    public static final String TAG_ALAMAT_DOSEN = "alamat_dosen";
    public static final String TAG_EMAIL_DOSEN = "email_dosen";
    public static final String TAG_PASSBARU_DOSEN = "passwordbaru";
    public static final String TAG_PASSLAMA_DOSEN = "password";
    public static final String TAG_NAMA_DOSEN = "nama_dosen";
    public static final String TAG_NIP = "nip";
    public static final String TAG_MABING = "mabing";
    public static final String TAG_MAPENG = "mapeng";

}
