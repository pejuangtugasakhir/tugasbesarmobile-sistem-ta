package id.ac.usu.sistemta.dosen;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import id.ac.usu.sistemta.R;
import id.ac.usu.sistemta.config.ConfigDosenPenilaian;
import id.ac.usu.sistemta.session.UserSessionManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class jadwal extends Fragment {
    ImageButton bim;
    Activity context;
    UserSessionManager session;
    String name;
    public jadwal() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        context=getActivity();
        return inflater.inflate(R.layout.dosen_jadwal, null);
    }

    public void onStart(){
        super.onStart();
        session = new UserSessionManager(context.getApplicationContext());

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();

        // get name
        name = user.get(UserSessionManager.KEY_NAME);


        if(android.os.Build.VERSION.SDK_INT > 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        ListView listview1 = (ListView)context.findViewById(R.id.listView2);
        listview1.setFastScrollEnabled(true);

        String url = ConfigDosenPenilaian.URL_GET_MHS_BIM+name;

        try{
            JSONArray data = new JSONArray(getJSONUrl(url));

            final ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
            HashMap<String,String> map;

            for (int i = 0; i<data.length(); i++){
                JSONObject c= data.getJSONObject(i);
                map = new HashMap<String,String>();
                map.put("id_nilai", c.getString("id_nilai"));
                map.put("n_semhas", c.getString("n_semhas"));
                map.put("nama_mhs", c.getString("nama_mhs"));
                map.put("n_sidang", c.getString("n_sidang"));
                map.put("nama_dosen", c.getString("nama_dosen"));
                map.put("judul_ta", c.getString("judul_ta"));
                map.put("tgl_nilai_semhas", c.getString("tgl_nilai_semhas"));
                map.put("tgl_nilai_sidang", c.getString("tgl_nilai_sidang"));
                map.put("nama_bidang", c.getString("nama_bidang"));

                MyArrList.add(map);
            }

            SimpleAdapter sAdap;

            sAdap = new SimpleAdapter(getActivity(), MyArrList, R.layout.activity_column_penilaian,
                    new String[] {"nama_mhs", "n_semhas", "n_sidang", "judul_ta", "nama_dosen"}, new int[]
                    {R.id.ColNama, R.id.ColTgl , R.id.ColTopik, R.id.ColHasil, R.id.ColStatus});

            listview1.setAdapter(sAdap);

            final AlertDialog.Builder viewDetail = new AlertDialog.Builder(context);
            //OnClick item

            listview1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    HashMap<String,String> map = (HashMap)parent.getItemAtPosition(position);
                    String idB = map.get("id_jadwal");
                    String nilai_semhas = map.get("n_semhas");
                    String namaMhs = map.get("nama_mhs");
                    String judul_ta = map.get("judul_ta");
                    String namadosenn = map.get("nama_dosen");
                    String nilaisidang = map.get("n_sidang");
                    String tanggalsemhas = map.get("tgl_nilai_semhas");
                    String tanggalsidang = map.get("tgl_nilai_sidang");
                    String namabidang = map.get("nama_bidang");


                    //viewDetail.setIcon(android.R.drawable.star_on);
                    viewDetail.setTitle("DETAIL");
                    viewDetail.setMessage("Nama Mahasiswa : " + namaMhs + "\n\n" +
                            "Nilai Seminar Hasil :" + nilai_semhas + "\n\n" +
                            "Tanggal Penilaian Semhas :" + tanggalsemhas + "\n\n" +
                            "Nilai Sidang :" + nilaisidang + "\n\n"+
                            "Tanggal Penilaian Sidang :" + tanggalsidang + "\n\n" +
                            "Judul TA :" + judul_ta + "\n\n" +
                            "Bidang TA :" + namabidang + "\n\n" +
                            "Dosen Penilai :" + namadosenn);

                    viewDetail.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    //TODOAutoGenerated

                                    dialog.dismiss();
                                }
                            });
                    viewDetail.show();
                }
            });

        }
        catch (JSONException e){
            e.printStackTrace();
        }
    }


    public String getJSONUrl(String url){
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) { // Download OK
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download result..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }
}
