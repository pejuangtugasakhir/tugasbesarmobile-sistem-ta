package id.ac.usu.sistemta.dosen;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import id.ac.usu.sistemta.R;
import id.ac.usu.sistemta.config.ConfigDosenPenilaian;
import id.ac.usu.sistemta.handler.RequestHandler;
import id.ac.usu.sistemta.session.UserSessionManager;

public class borang_masukan extends AppCompatActivity implements View.OnClickListener {
    private EditText editNamaMahasiswa, Tanggal, topik, hsil, DateEtxt, tupel, lanter, metod, daput;
    private Button cancel, done;
    UserSessionManager session;
    private String name, id_jadwal, namaMHS;

    ArrayList<String> listItems=new ArrayList<>();
    ArrayAdapter<String> aa;

    public borang_masukan() {
        // Required empty public constructor

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dosen_borang_masukan);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        id_jadwal = intent.getStringExtra(ConfigDosenPenilaian.JADWAL_ID);
        namaMHS = intent.getStringExtra(ConfigDosenPenilaian.MHS_NAMA);

        editNamaMahasiswa = (EditText)findViewById(R.id.editNamaMHS);
        Tanggal = (EditText)findViewById(R.id.editTanggal);
        topik = (EditText) findViewById(R.id.editText4);
        hsil = (EditText) findViewById(R.id.editText5);
        tupel = (EditText) findViewById(R.id.editTextTUPEL);
        lanter = (EditText) findViewById(R.id.editTextLANTER);
        metod = (EditText) findViewById(R.id.editTextMETOD);
        daput = (EditText) findViewById(R.id.editTextDAPUT);
        cancel = (Button) findViewById(R.id.button4);
        done = (Button) findViewById(R.id.button3);

        cancel.setOnClickListener(this);
        done.setOnClickListener(this);

        editNamaMahasiswa.setText(namaMHS);
        Tanggal.setText(getCurrentDate());
    }

    public String getCurrentDate(){
        final Calendar c = Calendar.getInstance();
        int year, month, day;
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DATE);
        return year + "-" + (month+1) + "-" + day;
    }

    public void onStart(){
        super.onStart();
        session = new UserSessionManager(getApplicationContext());

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();

        // get name
        name = user.get(UserSessionManager.KEY_NAME);
    }

    @Override
    public void onClick(View v) {
        if(v==done){
            addMhs();
        }
        else if(v==cancel){
            startActivity(new Intent(this, borang_masukan.class));
            finish();
        }
    }

    private void addMhs(){
        final String tgl= Tanggal.getText().toString().trim();
        final String namadosen= editNamaMahasiswa.getText().toString().trim();
        final String topikstring= topik.getText().toString().trim();
        final String hasilbimbingan=hsil.getText().toString().trim();
        final String tujuanpenelitian=tupel.getText().toString().trim();
        final String landasanteori=lanter.getText().toString().trim();
        final String metodologi=metod.getText().toString().trim();
        final String daftarpustaka=daput.getText().toString().trim();

        class addMhs extends AsyncTask<Void,Void,String>{
            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(borang_masukan.this,"Adding...","Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(borang_masukan.this, s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(Void... v) {
                HashMap<String,String> params = new HashMap<>();
                params.put(ConfigDosenPenilaian.KEY_EMP_NAMADOSEN,name);
                params.put(ConfigDosenPenilaian.KEY_EMP_NAMAMHS,namadosen);
                params.put(ConfigDosenPenilaian.KEY_EMP_TGL, tgl);
                params.put(ConfigDosenPenilaian.KEY_EMP_TOPIK, topikstring);
                params.put(ConfigDosenPenilaian.KEY_EMP_HASIL, hasilbimbingan);
                params.put(ConfigDosenPenilaian.KEY_EMP_IDBIM, id_jadwal);
                params.put(ConfigDosenPenilaian.KEY_EMP_TUPEL, tujuanpenelitian);
                params.put(ConfigDosenPenilaian.KEY_EMP_LANTER, landasanteori);
                params.put(ConfigDosenPenilaian.KEY_EMP_METOD, metodologi);
                params.put(ConfigDosenPenilaian.KEY_EMP_DAPUT, daftarpustaka);

                RequestHandler rh = new RequestHandler();
                String res = rh.sendPostRequest(ConfigDosenPenilaian.URL_ADD, params);
                return res;
            }
        }
        if(namadosen.isEmpty()) {
            Toast.makeText(this, "Maaf Anda Tidak Memiliki Mahasiswa Untuk Dikomentari", Toast.LENGTH_LONG).show();
        }else if(topikstring.isEmpty()){
            Toast.makeText(getBaseContext(),"Komentari Latar Belakang", Toast.LENGTH_LONG).show();
        }else if(hasilbimbingan.isEmpty()){
            Toast.makeText(getBaseContext(),"Komentari Rumusan Masalah", Toast.LENGTH_LONG).show();
        }else if(tujuanpenelitian.isEmpty()){
            Toast.makeText(getBaseContext(),"Komentari Tujuan Penelitian", Toast.LENGTH_LONG).show();
        }else if(landasanteori.isEmpty()){
            Toast.makeText(getBaseContext(),"Komentari Landasan Teori", Toast.LENGTH_LONG).show();
        }else if(metodologi.isEmpty()){
            Toast.makeText(getBaseContext(),"Komentari Metodologi", Toast.LENGTH_LONG).show();
        }else if(daftarpustaka.isEmpty()){
            Toast.makeText(getBaseContext(),"Komentari Daftar Pustaka", Toast.LENGTH_LONG).show();
        }else if(topikstring.isEmpty() || hasilbimbingan.isEmpty()|| namadosen.isEmpty()){
            Toast.makeText(getBaseContext(),"Lengkapi Komentar", Toast.LENGTH_LONG).show();
        }
        else{
            addMhs ae = new addMhs();
            ae.execute();
            topik.setText(" ");
            hsil.setText(" ");
            tupel.setText(" ");
            lanter.setText(" ");
            metod.setText(" ");
            daput.setText(" ");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_borang_masukan, menu);
        return true;
    }
}
