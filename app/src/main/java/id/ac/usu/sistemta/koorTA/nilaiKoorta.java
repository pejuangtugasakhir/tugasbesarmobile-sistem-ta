package id.ac.usu.sistemta.koorTA;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import id.ac.usu.sistemta.R;
import id.ac.usu.sistemta.config.ConfigKoorTAPenilaianLihat;
import id.ac.usu.sistemta.session.UserSessionManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class nilaiKoorta extends Fragment {

    UserSessionManager session;
    String name;
    Activity context;
    public nilaiKoorta() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        context=getActivity();
        return inflater.inflate(R.layout.koorta_nilai, container, false);
    }

    public void onStart() {

        super.onStart();
        session = new UserSessionManager(context.getApplicationContext());

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();

        // get name
        name = user.get(UserSessionManager.KEY_NAME);
        if(android.os.Build.VERSION.SDK_INT > 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        ListView listview1 = (ListView)context.findViewById(R.id.listView);
        listview1.setFastScrollEnabled(true);

        String url = ConfigKoorTAPenilaianLihat.URL_GET_PENILAIAN;

        try{
            JSONArray data = new JSONArray(getJSONUrl(url));

            final ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
            HashMap<String,String> map;

            for (int i = 0; i<data.length(); i++){
                JSONObject c= data.getJSONObject(i);
                map = new HashMap<String,String>();
                map.put("nimMhs", c.getString("nimMhs"));
                map.put("namaMhs", c.getString("namaMhs"));
                map.put("nilaiSemhas", c.getString("nilaiSemhas"));
                map.put("nilaiSidang", c.getString("nilaiSidang"));
                map.put("totalNilai", c.getString("totalNilai"));
                map.put("nilaiHuruf", c.getString("nilaiHuruf"));
                MyArrList.add(map);
            }

            SimpleAdapter sAdap;

            sAdap = new SimpleAdapter(getActivity(), MyArrList, R.layout.activity_nilai_koor_ta,
                    new String[] {"nimMhs", "namaMhs", "nilaiSemhas","nilaiSidang","totalNilai","nilaiHuruf"}, new int[]
                    {R.id.ColnimMhs,R.id.ColNamaMhs ,R.id.ColNilaiHasil,R.id.ColNilaiSidang,R.id.ColTotalNilai,R.id.ColNilaiHuruf});

            listview1.setAdapter(sAdap);
            final AlertDialog.Builder viewDetail = new AlertDialog.Builder(context);

            //OnClick item

            listview1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> myAdapter, View myView, int position, long mylng) {
                    String strNim = MyArrList.get(position).get("nimMhs").toString();
                    String strNama = MyArrList.get(position).get("namaMhs").toString();
                    String strNilaiSemhas = MyArrList.get(position).get("nilaiSemhas");
                    String strNilaiSidang = MyArrList.get(position).get("nilaiSidang").toString();
                    String strTotalNilai = MyArrList.get(position).get("totalNilai").toString();
                    String strNilaiHuruf = MyArrList.get(position).get("nilaiHuruf").toString();


                    //viewDetail.setIcon(android.R.drawable.star_on);
                    viewDetail.setTitle("DETAIL");
                    viewDetail.setMessage("NIM : " + strNim + "\n\n" + "Nama Mahasiswa :" + strNama + "\n\n" +
                            "Nilai Seminar Hasil:" + strNilaiSemhas + "\n\n" +
                            "Nilai Sidang :" + strNilaiSidang + "\n\n" + "Total Nilai :" + strTotalNilai
                            + "\n\n" + "Nilai Dalam Huruf :" + strNilaiHuruf);

                    viewDetail.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    //TODOAutoGenerated

                                    dialog.dismiss();
                                }
                            });
                    viewDetail.show();
                }
            });
        }
        catch (JSONException e){
            e.printStackTrace();
        }
    }

    public String getJSONUrl(String url){
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) { // Download OK
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download result..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }
}
