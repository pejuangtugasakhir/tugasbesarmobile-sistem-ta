package id.ac.usu.sistemta.tataUsaha;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.TimePicker;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import android.widget.AdapterView.OnItemSelectedListener;
import java.util.Locale;

import id.ac.usu.sistemta.mahasiswa.Bimbingan_Mhs;
import id.ac.usu.sistemta.R;
import id.ac.usu.sistemta.config.ConfigJadwal;
import id.ac.usu.sistemta.config.ConfigMhs;
import id.ac.usu.sistemta.handler.RequestHandler;

public class tu_tambah_jadwal extends AppCompatActivity implements View.OnClickListener{
    private EditText DateEtxt,Time;

    ArrayList<String> listItems=new ArrayList<>();
    ArrayAdapter<String> adapter;
    private android.app.DatePickerDialog DatePickerDialog;
    private SimpleDateFormat dateFormatter;

    private RadioGroup radioStatus;
    private RadioButton radioButtonStatus;
//    private RadioButton radioButtonProposal;
//    private RadioButton radioButtonHasil;
//    private RadioButton radioButtonSidang;

    private Button addJadwal;
    private Button reset;
    private EditText tgl;
    private EditText wkt;
    private Spinner nama;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tu_tambah_jadwal);

        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        findViewsById();

        setDateTimeField();

        nama = (Spinner)findViewById(R.id.spinnerMhs);
        radioStatus = (RadioGroup)findViewById(R.id.radioStatus);

        tgl = (EditText)findViewById(R.id.editTgl);
        wkt = (EditText)findViewById(R.id.editWaktu);
        wkt.setInputType(InputType.TYPE_NULL);
        wkt.requestFocus();
        wkt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(tu_tambah_jadwal.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        wkt.setText(new StringBuilder().append(pad(selectedHour)).append(":").append(pad(selectedMinute)));
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });

        adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,listItems);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        nama.setAdapter(adapter);

        nama.setOnItemSelectedListener(new OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int pos, long arg3) {
                // TODO Auto-generated method stub

                Toast.makeText(getBaseContext(), nama.getSelectedItem().toString(),
                        Toast.LENGTH_SHORT).show();
            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

        addJadwal = (Button)findViewById(R.id.add_jadwal);
        reset = (Button)findViewById(R.id.reset);

        addJadwal.setOnClickListener(this);
        reset.setOnClickListener(this);
    }

    private static String pad(int c){
        if(c>=10)
            return String.valueOf(c);
        else
            return "0" + String.valueOf(c);
    }

    @Override
    public void onClick(View v) {
        if(v==DateEtxt){
            DatePickerDialog.show();
        }
        if(v == addJadwal){
            addJadwal();
        }
        if(v == reset){
            reset();
        }
    }

    private void addJadwal(){

        final String snim = nama.getSelectedItem().toString().trim();
        final String stgl = tgl.getText().toString().trim();
        final String swkt = wkt.getText().toString().trim();

        int selectId = radioStatus.getCheckedRadioButtonId();
        radioButtonStatus = (RadioButton)findViewById(selectId);
        final String status = radioButtonStatus.getText().toString();

        class AddJadwal extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(tu_tambah_jadwal.this,"Adding...","Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(tu_tambah_jadwal.this, s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(Void... v) {
                HashMap<String,String> params = new HashMap<>();
                params.put(ConfigJadwal.KEY_EMP_NIM,snim);
                params.put(ConfigJadwal.KEY_EMP_TGL,stgl);
                params.put(ConfigJadwal.KEY_EMP_WAKTU,swkt);
                params.put(ConfigJadwal.KEY_EMP_STATUS,status);

                RequestHandler rh = new RequestHandler();
                String res = rh.sendPostRequest(ConfigJadwal.URL_ADD, params);
                return res;
            }
        }

        if(stgl.isEmpty()){
            Toast.makeText(getBaseContext(),"Masukkan Tanggal", Toast.LENGTH_LONG).show();
        }else if(swkt.isEmpty()){
            Toast.makeText(getBaseContext(),"Masukkan Waktu", Toast.LENGTH_LONG).show();
        }else if(snim.isEmpty()){
            Toast.makeText(getBaseContext(),"Masukkan NIM", Toast.LENGTH_LONG).show();
        }
        else{
            AddJadwal ae = new AddJadwal();
            ae.execute();

            tgl.setText(" ");
            wkt.setText(" ");
            nama.getEmptyView();
            tgl.requestFocus();
        }
    }

    private void reset() {
        tgl.setText(" ");
        wkt.setText(" ");
        nama.getEmptyView();
        tgl.requestFocus();
    }

    public void onStart(){
        super.onStart();
        BackTask bt=new BackTask();
        bt.execute();
    }

    private class BackTask extends AsyncTask<Void,Void,Void> {
        ArrayList<String> list;
        protected void onPreExecute(){
            super.onPreExecute();
            list = new ArrayList<>();
        }

        protected Void doInBackground(Void... params) {
            InputStream is = null;
            String result = "";
            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost("http://sistemta.esy.es/sistem_ta/mod/mhs/select_mhs.php");
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                // Get our response as a String.
                is = entity.getContent();
            }catch(IOException e){
                e.printStackTrace();
            }

            //convert response to string
            try{
                BufferedReader reader = new BufferedReader(new InputStreamReader(is,"utf-8"));
                String line = null;
                while ((line = reader.readLine()) != null) {
                    result+=line;
                }
                is.close();
                //result=sb.toString();
            }catch(Exception e){
                e.printStackTrace();
            }
            // parse json data
            try{
                JSONArray jArray =new JSONArray(result);
                for(int i=0;i<jArray.length();i++){
                    JSONObject jsonObject=jArray.getJSONObject(i);
                    // add interviewee name to arraylist
                    list.add(jsonObject.getString("nim"));
                }
            }
            catch(JSONException e){
                e.printStackTrace();
            }
            return null;
        }
        protected void onPostExecute(Void result){
            listItems.addAll(list);
            adapter.notifyDataSetChanged();
        }
    }

    private void findViewsById() {
        DateEtxt = (EditText) findViewById(R.id.editTgl);
        DateEtxt.setInputType(InputType.TYPE_NULL);
        DateEtxt.requestFocus();

    }

    private void setDateTimeField() {
        DateEtxt.setOnClickListener(this);
        Calendar newCalendar = Calendar.getInstance();
        DatePickerDialog = new DatePickerDialog(this, new android.app.DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                DateEtxt.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }


}