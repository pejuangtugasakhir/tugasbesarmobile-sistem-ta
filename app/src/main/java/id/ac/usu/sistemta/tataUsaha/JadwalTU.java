package id.ac.usu.sistemta.tataUsaha;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import id.ac.usu.sistemta.R;
import id.ac.usu.sistemta.config.ConfigJadwal;

/**
 * A simple {@link Fragment} subclass.
 */
public class JadwalTU extends Fragment {
    ImageButton add,update,view;
    Activity context;

    public JadwalTU() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        context=getActivity();
        return inflater.inflate(R.layout.tu_jadwal,container, false);
    }
    @Override
    public void onStart(){
        super.onStart();

        add = (ImageButton)context.findViewById(R.id.add_jadwal);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent m = new Intent(context, tu_tambah_jadwal.class);
                startActivity(m);
            }
        });

        if(android.os.Build.VERSION.SDK_INT > 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        final ListView listview1 = (ListView)context.findViewById(R.id.listView4);
        listview1.setFastScrollEnabled(true);

        String url = "http://sistemta.esy.es/sistem_ta/mod/jadwal/select_jadwal.php";

        try{
            JSONArray data = new JSONArray(getJSONUrl(url));

            final ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
            HashMap<String,String> map;

            for (int i = 0; i<data.length(); i++){
                JSONObject c= data.getJSONObject(i);
                map = new HashMap<String,String>();
                map.put("id_jadwal",c.getString("id_jadwal"));
                map.put("nim",c.getString("nim"));
                map.put("tanggal_seminar", c.getString("tanggal_seminar"));
                map.put("waktu_seminar", c.getString("waktu_seminar"));
                map.put("nama_mhs", c.getString("nama_mhs"));
                map.put("judul_ta", c.getString("judul_ta"));
                map.put("status_kegiatan", c.getString("status_kegiatan"));
                MyArrList.add(map);
            }

            SimpleAdapter sAdap;

            sAdap = new SimpleAdapter(getActivity(), MyArrList, R.layout.activity_column_jadwaltu,
                    new String[] {"nim","tanggal_seminar", "waktu_seminar", "nama_mhs", "judul_ta", "status_kegiatan"}, new int[]
                    {R.id.ColNim, R.id.ColTanggal, R.id.ColWaktu, R.id.ColNama , R.id.ColJudul, R.id.ColStatus});

            listview1.setAdapter(sAdap);

            final AlertDialog.Builder viewDetail = new AlertDialog.Builder(context);
            //OnClick item

            listview1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(context, tu_update_jadwal.class);

//                    HashMap<String, String> map = (HashMap) parent.getItemAtPosition(position);
                    HashMap<String, String> info = (HashMap<String, String>) parent.getItemAtPosition(position);

                    //Then access individual data items within the hash map using the key
//                    String personName = info.get("first_name");

                    String idJ = info.get("id_jadwal");
                    String nim = info.get("nim");
                    String tanggal = info.get(ConfigJadwal.TAG_TGL);
                    String waktu = info.get(ConfigJadwal.TAG_WAKTU);
                    String status = info.get(ConfigJadwal.TAG_STATUS);

                    intent.putExtra(ConfigJadwal.JADWAL_NIM, nim);
                    intent.putExtra(ConfigJadwal.JADWAL_ID, idJ);
                    intent.putExtra(ConfigJadwal.JADWAL_TGL, tanggal);
                    intent.putExtra(ConfigJadwal.JADWAL_WAKTU,waktu);
                    intent.putExtra(ConfigJadwal.JADWAL_STATUS, status);
                    startActivity(intent);
                }
            });
        }
        catch (JSONException e){
            e.printStackTrace();
        }
    }

    public String getJSONUrl(String url){
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) { // Download OK
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download result..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


}
