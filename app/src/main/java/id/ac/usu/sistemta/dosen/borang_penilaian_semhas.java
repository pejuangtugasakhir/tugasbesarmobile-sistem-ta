package id.ac.usu.sistemta.dosen;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;

import id.ac.usu.sistemta.R;
import id.ac.usu.sistemta.config.ConfigDosenPenilaian;
import id.ac.usu.sistemta.config.ConfigMhsBimbingan;
import id.ac.usu.sistemta.handler.RequestHandler;
import id.ac.usu.sistemta.session.UserSessionManager;

public class borang_penilaian_semhas extends AppCompatActivity implements View.OnClickListener{
    private EditText editNamaMahasiswa,Tanggal, editJudulTA,editNilaiSemhas,edittgl;
    private Button buttondel,buttonupd;
    private String namaMHS,judulTA,id_jadwal,nilaiSemhas,tgl,name;
    UserSessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dosen_borang_penilaian_semhas);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        id_jadwal = intent.getStringExtra(ConfigDosenPenilaian.JADWAL_ID);
        namaMHS = intent.getStringExtra(ConfigDosenPenilaian.MHS_NAMA);
        judulTA = intent.getStringExtra(ConfigDosenPenilaian.MHS_JUDUL);

        Tanggal = (EditText)findViewById(R.id.editTanggal);
        editNamaMahasiswa = (EditText)findViewById(R.id.editNamaMHS);
        editJudulTA = (EditText)findViewById(R.id.editJudul);
        editNilaiSemhas = (EditText)findViewById(R.id.editNilai);

        buttonupd = (Button) findViewById(R.id.buttonupd);
        buttondel = (Button) findViewById(R.id.buttondel);

        buttonupd.setOnClickListener(this);
        buttondel.setOnClickListener(this);

        editNamaMahasiswa.setText(namaMHS);
        editJudulTA.setText(judulTA);
        Tanggal.setText(getCurrentDate());

        getBimReport();
    }

    public String getCurrentDate(){
        final Calendar c = Calendar.getInstance();
        int year, month, day;
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DATE);
        return year + "-" + (month+1) + "-" + day;
    }

    private void getBimReport(){
        class GetBim extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute(){
                super.onPreExecute();
                loading = ProgressDialog.show(borang_penilaian_semhas.this,"Fetching....","Wait...",false,false);
            }
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                showBim(s);
            }
            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequestParam(ConfigDosenPenilaian.URL_GET_MHS_SEMHAS,id_jadwal);
                return s;
            }
        }
        GetBim ge = new GetBim();
        ge.execute();
    }


    private void showBim(String json){
        try{
            JSONObject jsonObject = new JSONObject(json);
            JSONArray result = jsonObject.getJSONArray(ConfigMhsBimbingan.TAG_JSON_ARRAY);
            JSONObject c = result.getJSONObject(0);
            String nama_mhs_semhas = c.getString(ConfigDosenPenilaian.TAG_NAMA_MHS);
            String judul_mhs = c.getString(ConfigDosenPenilaian.TAG_JUDUL);

            editNamaMahasiswa.setText(nama_mhs_semhas);
            editJudulTA.setText(judul_mhs);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void onClick(View v) {
        if(v == buttonupd){
            addMhs();
        }

        if(v == buttondel){
            startActivity(new Intent(this, borang_penilaian_semhas.class));
            finish();
        }
    }

    public void onStart(){
        super.onStart();
        session = new UserSessionManager(getApplicationContext());

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();

        // get name
        name = user.get(UserSessionManager.KEY_NAME);

    }

    private void addMhs(){
        final String tgl= Tanggal.getText().toString().trim();
        final String nama_mahasiswa_semhas= editNamaMahasiswa.getText().toString().trim();
        final String nilai_Semhas=editNilaiSemhas.getText().toString().trim();

        class addMhs extends AsyncTask<Void,Void,String>{
            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(borang_penilaian_semhas.this,"Adding...","Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(borang_penilaian_semhas.this, s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(Void... v) {
                HashMap<String,String> params = new HashMap<>();
                params.put(ConfigDosenPenilaian.KEY_EMP_NAMADOSEN_SEMHAS,name);
                params.put(ConfigDosenPenilaian.KEY_EMP_NAMAMHS_SEMHAS,nama_mahasiswa_semhas);
                params.put(ConfigDosenPenilaian.KEY_EMP_TGL_SEMHAS, tgl);
                params.put(ConfigDosenPenilaian.KEY_EMP_NILAI_SEMHAS, nilai_Semhas);

                RequestHandler rh = new RequestHandler();
                String res = rh.sendPostRequest(ConfigDosenPenilaian.URL_ADD_SEMHAS, params);
                return res;
            }
        }
        if(nama_mahasiswa_semhas.isEmpty()) {
            Toast.makeText(this, "Maaf Anda Tidak Memiliki Mahasiswa Untuk Dinilai", Toast.LENGTH_LONG).show();
        }else if(nilai_Semhas.isEmpty()){
            Toast.makeText(getBaseContext(),"Masukkan Nilai", Toast.LENGTH_LONG).show();
        }else if(nama_mahasiswa_semhas.isEmpty() || nilai_Semhas.isEmpty()){
            Toast.makeText(getBaseContext(),"Lengkapi Data", Toast.LENGTH_LONG).show();
        }
        else{
            addMhs ae = new addMhs();
            ae.execute();
            editNilaiSemhas.setText(" ");
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_borang_penilaian_semhas, menu);
        return true;
    }

}
