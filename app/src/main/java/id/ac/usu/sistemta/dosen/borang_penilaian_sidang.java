package id.ac.usu.sistemta.dosen;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;

import id.ac.usu.sistemta.R;
import id.ac.usu.sistemta.config.ConfigDosenPenilaian;
import id.ac.usu.sistemta.config.ConfigMhsBimbingan;
import id.ac.usu.sistemta.config.ConfigPenilaianSidang;
import id.ac.usu.sistemta.handler.RequestHandler;
import id.ac.usu.sistemta.session.UserSessionManager;

public class borang_penilaian_sidang extends AppCompatActivity implements View.OnClickListener{
    private EditText edittopik,edithasil,Tanggal,editnamadsn,edittgl,editNilaiSemhas;
    private Button buttondel,buttonupd;
    private String hasil,topik,tgl,name,id_bimbingan,id_jadwal,id_nilai,namadsn,nilaiSemhas,tglSemhas;
    UserSessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dosen_borang_penilaian_sidang);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        id_jadwal = intent.getStringExtra(ConfigDosenPenilaian.JADWAL_ID);
        id_nilai = intent.getStringExtra(ConfigDosenPenilaian.NILAI_ID);
        namadsn = intent.getStringExtra(ConfigDosenPenilaian.MHS_NAMA);
        topik = intent.getStringExtra(ConfigDosenPenilaian.MHS_JUDUL);
        nilaiSemhas = intent.getStringExtra(ConfigDosenPenilaian.NILAI_SEMHAS);
        tglSemhas = intent.getStringExtra(ConfigDosenPenilaian.TGL_SEMHAS);

        Tanggal = (EditText)findViewById(R.id.editTanggal);
        edittopik = (EditText)findViewById(R.id.edittopik);
        edithasil = (EditText)findViewById(R.id.edithasil);
        editnamadsn = (EditText)findViewById(R.id.editnamadsn);
        editNilaiSemhas= (EditText)findViewById(R.id.editTextNilaiSemhas);

        buttonupd = (Button) findViewById(R.id.buttonupd);
        buttondel = (Button) findViewById(R.id.buttondel);

        buttonupd.setOnClickListener(this);
        buttondel.setOnClickListener(this);

        editnamadsn.setText(namadsn);
        editNilaiSemhas.setText(nilaiSemhas);
        edittopik.setText(topik);
        Tanggal.setText(getCurrentDate());

        getBimReport();
    }

    public String getCurrentDate(){
        final Calendar c = Calendar.getInstance();
        int year, month, day;
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DATE);
        return year + "-" + (month+1) + "-" + day;
    }

    private void getBimReport(){
        class GetBim extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute(){
                super.onPreExecute();
                loading = ProgressDialog.show(borang_penilaian_sidang.this,"Fetching....","Wait...",false,false);
            }
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                showBim(s);
            }
            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequestParam(ConfigDosenPenilaian.URL_GET_MHS_SIDANG,id_jadwal);
                return s;
            }
        }
        GetBim ge = new GetBim();
        ge.execute();
    }


    private void showBim(String json){
        try{
            JSONObject jsonObject = new JSONObject(json);
            JSONArray result = jsonObject.getJSONArray(ConfigMhsBimbingan.TAG_JSON_ARRAY);
            JSONObject c = result.getJSONObject(0);
            String namadsnbm = c.getString(ConfigDosenPenilaian.TAG_NAMA_MHS);
            String nilaisemhasMHS = c.getString(ConfigDosenPenilaian.NILAI_SEMHAS);
            String judul_mhs = c.getString(ConfigDosenPenilaian.TAG_JUDUL);

            editnamadsn.setText(namadsnbm);
            editNilaiSemhas.setText(nilaisemhasMHS);
            edittopik.setText(judul_mhs);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void onClick(View v) {
        if(v == buttonupd){
            updateBim();
        }

        if(v == buttondel){
            startActivity(new Intent(this, borang_penilaian_semhas.class));
            finish();
        }
    }

    public void onStart(){
        super.onStart();
        session = new UserSessionManager(getApplicationContext());

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();

        // get name
        name = user.get(UserSessionManager.KEY_NAME);

    }

    private void updateBim(){
        //final String namadsnbm = editnamadsn.getText().toString().trim();
        final String tglbim = Tanggal.getText().toString().trim();
        final String tpikbim = edittopik.getText().toString().trim();
        final String hsl = edithasil.getText().toString().trim();

        class UpdateMhs extends AsyncTask<Void,Void,String>{
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(borang_penilaian_sidang.this,"Updating...","Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(borang_penilaian_sidang.this, s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(Void... params) {
                HashMap<String,String> hashMap = new HashMap<>();
                hashMap.put(ConfigPenilaianSidang.KEY_UPD_NAMADOSEN,name);
                hashMap.put(ConfigPenilaianSidang.KEY_UPD_NAMAMHS,namadsn);
                hashMap.put(ConfigPenilaianSidang.KEY_UPD_NILAI_ID,id_nilai);
                hashMap.put(ConfigPenilaianSidang.KEY_UPD_TGL_SIDANG,tglbim);
                hashMap.put(ConfigPenilaianSidang.KEY_UPD_TOPIK,tpikbim);
                hashMap.put(ConfigPenilaianSidang.KEY_UPD_SIDANG,hsl);
                hashMap.put(ConfigPenilaianSidang.KEY_UPD_SEMHAS,nilaiSemhas);
                hashMap.put(ConfigPenilaianSidang.KEY_UPD_TGL_SEMHAS,tglSemhas);


                RequestHandler rh = new RequestHandler();
                String s = rh.sendPostRequest(ConfigPenilaianSidang.URL_UPDATE_SIDANG, hashMap);
                return s;
            }
        }

        UpdateMhs ue = new UpdateMhs();
        ue.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_borang_penilaian_sidang, menu);
        return true;
    }

}
