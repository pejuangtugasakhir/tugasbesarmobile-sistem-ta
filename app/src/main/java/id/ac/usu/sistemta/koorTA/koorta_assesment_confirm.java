package id.ac.usu.sistemta.koorTA;

import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import id.ac.usu.sistemta.R;
import id.ac.usu.sistemta.config.ConfigAssesment;
import id.ac.usu.sistemta.config.ConfigJadwal;
import id.ac.usu.sistemta.handler.RequestHandler;

public class koorta_assesment_confirm extends AppCompatActivity implements View.OnClickListener,RadioGroup.OnCheckedChangeListener {

    private String nim;
    private String id_exsum;
    private String nama_file;

    private EditText keterangan;
    private EditText enim;

    private Spinner ssdp1;
    private Spinner ssdp2;

    private TextView tnama_file;

    private RadioGroup radioStatusAs;
    private RadioButton radioButtonStatusAs;
    private RadioButton radioButtonPb;
    private RadioButton radioButtonPk;
    private RadioButton radioButtonDt;
    private RadioButton radioButtonDl;

    private Button btnConfirm;
    private Button btnReset;

    private ImageButton img_dwnload;

    ArrayList<String> listItems=new ArrayList<>();
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.koorta_assesment_confirm);

        Intent intent = getIntent();

        id_exsum = intent.getStringExtra(ConfigAssesment.ASS_ID);
        nim = intent.getStringExtra(ConfigAssesment.ASS_NIM);
        nama_file = intent.getStringExtra(ConfigAssesment.ASS_NAMA_FILE);

        enim = (EditText) findViewById(R.id.enim);
        tnama_file = (TextView) findViewById(R.id.inifile);
        keterangan = (EditText) findViewById(R.id.eKeterangan);

        ssdp1 = (Spinner)findViewById(R.id.sdpg1);
        ssdp2 = (Spinner)findViewById(R.id.sdpg2);

        radioStatusAs = (RadioGroup)findViewById(R.id.radioStatusAs);
        radioButtonPb = (RadioButton)findViewById(R.id.radioPb);
        radioButtonPk = (RadioButton)findViewById(R.id.radioPk);
        radioButtonDt = (RadioButton)findViewById(R.id.radioDt);
        radioButtonDl = (RadioButton)findViewById(R.id.radioDl);

        btnConfirm = (Button) findViewById(R.id.btn_confirm);
        btnReset = (Button) findViewById(R.id.btn_reset);
        img_dwnload = (ImageButton) findViewById(R.id.img_dwnload);

        enim.setText(nim);
        tnama_file.setText(nama_file);

        getAs(id_exsum);
        btnConfirm.setOnClickListener(this);
        btnReset.setOnClickListener(this);

        img_dwnload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Boolean result = isDownloadManagerAvailable(getApplicationContext());
                if (result)
                    downloadFile();
            }
        });

        radioStatusAs.setOnCheckedChangeListener(this);

        adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,listItems);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ssdp1.setAdapter(adapter);
        ssdp2.setAdapter(adapter);

        ssdp1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int pos, long arg3) {
                // TODO Auto-generated method stub

                Toast.makeText(getBaseContext(), ssdp1.getSelectedItem().toString(),
                        Toast.LENGTH_SHORT).show();
            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

        ssdp2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int pos, long arg3) {
                // TODO Auto-generated method stub

                Toast.makeText(getBaseContext(), ssdp2.getSelectedItem().toString(),
                        Toast.LENGTH_SHORT).show();
            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });
    }

//    public void onStart(){
//        super.onStart();
//        BackTask bt=new BackTask();
//        bt.execute();
//    }

    private class BackTask extends AsyncTask<Void,Void,Void> {
        ArrayList<String> list;
        protected void onPreExecute(){
            super.onPreExecute();
            list = new ArrayList<>();
        }

        protected Void doInBackground(Void... params) {
            InputStream is = null;
            String result = "";
            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost("http://sistemta.esy.es/sistem_ta/mod/dosen/select_dosen.php");
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                // Get our response as a String.
                is = entity.getContent();
            }catch(IOException e){
                e.printStackTrace();
            }

            //convert response to string
            try{
                BufferedReader reader = new BufferedReader(new InputStreamReader(is,"utf-8"));
                String line = null;
                while ((line = reader.readLine()) != null) {
                    result+=line;
                }
                is.close();
                //result=sb.toString();
            }catch(Exception e){
                e.printStackTrace();
            }
            // parse json data
            try{
                JSONArray jArray =new JSONArray(result);
                for(int i=0;i<jArray.length();i++){
                    JSONObject jsonObject=jArray.getJSONObject(i);
                    // add interviewee name to arraylist
                    list.add(jsonObject.getString("nama_dosen"));
                }
            }
            catch(JSONException e){
                e.printStackTrace();
            }
            return null;
        }
        protected void onPostExecute(Void result){
            listItems.addAll(list);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId){
            case R.id.radioDt:
                ssdp1.setVisibility(View.VISIBLE);
                ssdp2.setVisibility(View.VISIBLE);

                BackTask bt=new BackTask();
                bt.execute();

                break;
            case R.id.radioPb:
                ssdp1.setVisibility(View.GONE);
                ssdp2.setVisibility(View.GONE);
                break;
            case R.id.radioPk:
                ssdp1.setVisibility(View.GONE);
                ssdp2.setVisibility(View.GONE);
                break;
            case R.id.radioDl:
                ssdp1.setVisibility(View.GONE);
                ssdp2.setVisibility(View.GONE);
                break;

        }
    }

    @SuppressLint("NewApi")
    public void downloadFile(){
        String DownloadUrl = "http://sistemta.esy.es/sistem_ta/mod/mhs/Data/"+nama_file;
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(DownloadUrl));

        request.setDescription("Downloading");   //appears the same in Notification bar while downloading
        request.setTitle("Download");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        }
        request.setDestinationInExternalFilesDir(getApplicationContext(),null, nama_file);

        // get download service and enqueue file
        DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);
    }

    public static boolean isDownloadManagerAvailable(Context context) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            return true;
        }
        return false;
    }


    private void getAs(String hid_exsum) {
        class HttpGetAsyncTask extends AsyncTask<String, Void, String> {
            protected String doInBackground(String... params) {
                String paramIdexsum = params[0];

                HttpClient httpClient = new DefaultHttpClient();

                HttpGet httpGet = new HttpGet("http://sistemta.esy.es/sistem_ta/mod/koorta/select_assement_one.php?id_exsum=" + paramIdexsum);

                try{
                    HttpResponse httpResponse = httpClient.execute(httpGet);
                    InputStream inputStream = httpResponse.getEntity().getContent();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                    StringBuilder stringBuilder = new StringBuilder();

                    String bufferedStrChunk = null;

                    while((bufferedStrChunk = bufferedReader.readLine()) != null){
                        stringBuilder.append(bufferedStrChunk);
                    }
                    return stringBuilder.toString();
                } catch (ClientProtocolException cpe) {
                    System.out.println("Exceptionrates caz of httpResponse :" + cpe);
                    cpe.printStackTrace();
                } catch (IOException ioe) {
                    System.out.println("Secondption generates caz of httpResponse :" + ioe);
                    ioe.printStackTrace();
                }

                return null;
            }
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                showAs(s);
            }
        }

        HttpGetAsyncTask httpGetAsyncTask = new HttpGetAsyncTask();

        httpGetAsyncTask.execute(hid_exsum);
    }

    private void showAs(String json){
        try{
            JSONObject jsonObject = new JSONObject(json);
            JSONArray result = jsonObject.getJSONArray(ConfigAssesment.TAG_JSON_ARRAY);
            JSONObject c = result.getJSONObject(0);

            String nim = c.getString(ConfigAssesment.TAG_NIM);
            String id = c.getString(ConfigAssesment.TAG_ID);
            String nama_file = c.getString(ConfigAssesment.TAG_NAMA_FILE);

            enim.setText(nim);
            tnama_file.setText(nama_file);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {

        if(v == btnConfirm){
            int selectId = radioStatusAs.getCheckedRadioButtonId();
            radioButtonStatusAs = (RadioButton)findViewById(selectId);
            final String status = radioButtonStatusAs.getText().toString();

            if(status.equals("diterima")){
                ConfirmAssD();
            } else{
                ConfirmAss();
            }
        }

        if(v == btnReset){
            ResetConfirm();
        }
    }

    private void ConfirmAssD(){
        final String snim = enim.getText().toString();
        final String sdoping1 = ssdp1.getSelectedItem().toString().trim();
        final String sdoping2 = ssdp2.getSelectedItem().toString().trim();
        final String sketerangan = keterangan.getText().toString();

        int selectId = radioStatusAs.getCheckedRadioButtonId();
        radioButtonStatusAs = (RadioButton)findViewById(selectId);
        final String status = radioButtonStatusAs.getText().toString();

        class UpdateJadwal extends AsyncTask<Void,Void,String>{
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(koorta_assesment_confirm.this,"Updating...","Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(koorta_assesment_confirm.this, s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(Void... params) {
                HashMap<String,String> hashMap = new HashMap<>();

                hashMap.put(ConfigAssesment.KEY_EMP_ID,id_exsum);
                hashMap.put(ConfigAssesment.KEY_EMP_NIM,snim);
                hashMap.put(ConfigAssesment.KEY_EMP_DPG1,sdoping1);
                hashMap.put(ConfigAssesment.KEY_EMP_DPG2,sdoping2);
                hashMap.put(ConfigAssesment.KEY_EMP_KETERANGAN,sketerangan);
                hashMap.put(ConfigAssesment.KEY_EMP_STATUS,status);

                RequestHandler rh = new RequestHandler();

                String s = rh.sendPostRequest(ConfigAssesment.URL_CONFIRM2, hashMap);

                return s;
            }
        }

        UpdateJadwal ue = new UpdateJadwal();
        ue.execute();
    }

    private void ConfirmAss(){
        final String sketerangan = keterangan.getText().toString();

        int selectId = radioStatusAs.getCheckedRadioButtonId();
        radioButtonStatusAs = (RadioButton)findViewById(selectId);
        final String status = radioButtonStatusAs.getText().toString();

        class UpdateJadwal extends AsyncTask<Void,Void,String>{
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(koorta_assesment_confirm.this,"Updating...","Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(koorta_assesment_confirm.this, s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(Void... params) {
                HashMap<String,String> hashMap = new HashMap<>();

                hashMap.put(ConfigAssesment.KEY_EMP_ID,id_exsum);
                hashMap.put(ConfigAssesment.KEY_EMP_KETERANGAN,sketerangan);
                hashMap.put(ConfigAssesment.KEY_EMP_STATUS,status);

                RequestHandler rh = new RequestHandler();

                String s = rh.sendPostRequest(ConfigAssesment.URL_CONFIRM, hashMap);

                return s;
            }
        }

        UpdateJadwal ue = new UpdateJadwal();
        ue.execute();
    }

    private void ResetConfirm(){
        keterangan.setText("");
        radioStatusAs.clearCheck();
    }

}
