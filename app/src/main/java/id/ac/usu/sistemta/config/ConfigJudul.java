package id.ac.usu.sistemta.config;

public class ConfigJudul {
    //address
    public static final String URL_ADD ="http://sistemta.esy.es/sistem_ta/mod/jadwal/insert_jadwal.php";
    public static final String URL_UPDATE_JDWL = "http://sistemta.esy.es/sistem_ta/mod/jadwal/update_jadwal.php";

    //public static final String URL_GET_JDL_ONE = "http://sistemta.esy.es/sistem_ta/mod/jadwal/getDoping.php?nim=";
    //public static final String URL_UPDATE_JUDUL = "http://sistemta.esy.es/sistem_ta/mod/dosen/delete_dosen.php?nip=";

    //Keys that will be used to send the request to php scripts
    public static final String KEY_EMP_NIM = "nim";
    public static final String KEY_EMP_TGL = "tanggal_seminar";
    public static final String KEY_EMP_WAKTU = "waktu_seminar";
    public static final String KEY_EMP_STATUS = "status_kegiatan";
    public static final String KEY_EMP_ID = "id_jadwal";

    //JSON Tags
    public static final String TAG_JSON_ARRAY="result";
    public static final String TAG_TGL = "tanggal_seminar";
    public static final String TAG_NIM = "nim";
    public static final String TAG_WAKTU = "waktu_seminar";
    public static final String TAG_STATUS = "status_kegiatan";

    public static final String JADWAL_ID = "id_jadwal";
    public static final String JADWAL_NIM = "nim";
    public static final String JADWAL_TGL= "tanggal_seminar";
    public static final String JADWAL_WAKTU = "waktu_seminar";
    public static final String JADWAL_STATUS = "status_kegiatan";

}
