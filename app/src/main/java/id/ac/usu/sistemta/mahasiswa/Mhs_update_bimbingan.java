package id.ac.usu.sistemta.mahasiswa;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import id.ac.usu.sistemta.R;
import id.ac.usu.sistemta.config.ConfigMhsBimbingan;
import id.ac.usu.sistemta.handler.RequestHandler;
import id.ac.usu.sistemta.session.UserSessionManager;

public class Mhs_update_bimbingan extends AppCompatActivity implements View.OnClickListener{
    private EditText edittopik,edithasil,editnamadsn,edittgl;
    private Button buttondel,buttonupd;
    private String namadsn,hasil,topik,tgl,name,id_bimbingan;
    private EditText DateEtxt;
    private android.app.DatePickerDialog DatePickerDialog;
    private SimpleDateFormat dateFormatter;
    UserSessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mhs_update_bimbingan2);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        id_bimbingan = intent.getStringExtra(ConfigMhsBimbingan.MHS_ID);
        namadsn = intent.getStringExtra(ConfigMhsBimbingan.MHS_NAMADOSEN);
        tgl = intent.getStringExtra(ConfigMhsBimbingan.MHS_TGL);
        topik = intent.getStringExtra(ConfigMhsBimbingan.MHS_TOPIK);
        hasil = intent.getStringExtra(ConfigMhsBimbingan.MHS_HASIL);

        edittgl = (EditText)findViewById(R.id.editTgl);
        edittopik = (EditText)findViewById(R.id.edittopik);
        edithasil = (EditText)findViewById(R.id.edithasil);
        editnamadsn = (EditText)findViewById(R.id.editnamadsn);

        buttonupd = (Button) findViewById(R.id.buttonupd);
        buttondel = (Button) findViewById(R.id.buttondel);

        buttonupd.setOnClickListener(this);
        buttondel.setOnClickListener(this);

        editnamadsn.setText(namadsn);
        edittgl.setText(tgl);
        edithasil.setText(hasil);
        edittopik.setText(topik);

        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        DateEtxt = (EditText) findViewById(R.id.editTgl);
        DateEtxt.setInputType(InputType.TYPE_NULL);
        edittgl.requestFocus();
        setDateTimeField();
        getBimReport();
    }

    private void setDateTimeField() {
        DateEtxt.setOnClickListener(this);
        Calendar newCalendar = Calendar.getInstance();
        DatePickerDialog = new DatePickerDialog(this, new android.app.DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                DateEtxt.setText(dateFormatter.format(newDate.getTime()));
            }
        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    private void getBimReport(){
        class GetBim extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute(){
                super.onPreExecute();
                loading = ProgressDialog.show(Mhs_update_bimbingan.this,"Fetching....","Wait...",false,false);
            }
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                showBim(s);
            }
            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequestParam(ConfigMhsBimbingan.URL_SELECT_BIMBINGAN,id_bimbingan);
                return s;
            }
        }
        GetBim ge = new GetBim();
        ge.execute();
    }


    private void showBim(String json){
        try{
            JSONObject jsonObject = new JSONObject(json);
            JSONArray result = jsonObject.getJSONArray(ConfigMhsBimbingan.TAG_JSON_ARRAY);
            JSONObject c = result.getJSONObject(0);
            String namadsnbm = c.getString(ConfigMhsBimbingan.TAG_NAMADOSEN);
            String tglbim = c.getString(ConfigMhsBimbingan.TAG_TGL);
            String tpikbim = c.getString(ConfigMhsBimbingan.TAG_TOPIK);
            String hsl = c.getString(ConfigMhsBimbingan.TAG_HASIL);

            editnamadsn.setText(namadsnbm);
            edittgl.setText(tglbim);
            edittopik.setText(tpikbim);
            edithasil.setText(hsl);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void onClick(View v) {
        if(v==DateEtxt){
            DatePickerDialog.show();
        }

        if(v == buttonupd){
            updateBim();
        }

        if(v == buttondel){
            confirmDeleteBim();
        }
    }

    public void onStart(){
        super.onStart();
        session = new UserSessionManager(getApplicationContext());

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();

        // get name
        name = user.get(UserSessionManager.KEY_NAME);

    }

    private void updateBim(){
        //final String namadsnbm = editnamadsn.getText().toString().trim();
        final String tglbim = edittgl.getText().toString().trim();
        final String tpikbim = edittopik.getText().toString().trim();
        final String hsl = edithasil.getText().toString().trim();

        class UpdateMhs extends AsyncTask<Void,Void,String>{
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(Mhs_update_bimbingan.this,"Updating...","Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(Mhs_update_bimbingan.this, s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(Void... params) {
                HashMap<String,String> hashMap = new HashMap<>();
                hashMap.put(ConfigMhsBimbingan.KEY_EMP_NAMAMHS,name);
                hashMap.put(ConfigMhsBimbingan.KEY_EMP_NAMADOSEN,namadsn);
                hashMap.put(ConfigMhsBimbingan.KEY_EMP_IDBIM,id_bimbingan);
                hashMap.put(ConfigMhsBimbingan.KEY_EMP_TGL,tglbim);
                hashMap.put(ConfigMhsBimbingan.KEY_EMP_TOPIK,tpikbim);
                hashMap.put(ConfigMhsBimbingan.KEY_EMP_HASIL,hsl);

                RequestHandler rh = new RequestHandler();
                String s = rh.sendPostRequest(ConfigMhsBimbingan.URL_UPDATE_MHS, hashMap);
                return s;
            }
        }

        UpdateMhs ue = new UpdateMhs();
        ue.execute();
    }

    private void deleteBim(){
        class DeleteBim extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(Mhs_update_bimbingan.this, "Deleting...", "Wait...", false, false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(Mhs_update_bimbingan.this, s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequestParam(ConfigMhsBimbingan.URL_DELETE_MHS,id_bimbingan);
                return s;
            }
        }
        DeleteBim de = new DeleteBim();
        de.execute();
    }

    private void confirmDeleteBim(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Apakah kamu ingin menghapus laporan bimbingan ini?");

        alertDialogBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        deleteBim();
                        startActivity(new Intent(Mhs_update_bimbingan.this,MainActivityMhs.class));
                    }
                });

        alertDialogBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mhs_update_bimbingan, menu);
        return true;
    }

}
