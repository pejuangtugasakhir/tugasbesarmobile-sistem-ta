package id.ac.usu.sistemta.dosen;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import id.ac.usu.sistemta.R;
import id.ac.usu.sistemta.config.ConfigDosenBimbingan;
import id.ac.usu.sistemta.session.UserSessionManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class bimbingan extends Fragment {
    ImageButton bim;
    Activity context;
    UserSessionManager session;
    String name;
    public bimbingan() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        context=getActivity();
        return inflater.inflate(R.layout.dosen_bimbingan, null);
    }

    public void onStart(){
        super.onStart();
        session = new UserSessionManager(context.getApplicationContext());

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();

        // get name
        name = user.get(UserSessionManager.KEY_NAME);


        if(android.os.Build.VERSION.SDK_INT > 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        ListView listview1 = (ListView)context.findViewById(R.id.listView2);
        listview1.setFastScrollEnabled(true);

        String url = ConfigDosenBimbingan.URL_GET_BIMBINGAN_DSN+name;

        try{
            JSONArray data = new JSONArray(getJSONUrl(url));

            final ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
            HashMap<String,String> map;

            for (int i = 0; i<data.length(); i++){
                JSONObject c= data.getJSONObject(i);
                map = new HashMap<String,String>();
                map.put("id_bimbingan", c.getString("id_bimbingan"));
                map.put("nama_mhs", c.getString("nama_mhs"));
                map.put("tgl_bimbingan", c.getString("tgl_bimbingan"));
                map.put("topik_diskusi", c.getString("topik_diskusi"));
                map.put("hasil_bimbingan", c.getString("hasil_bimbingan"));
                MyArrList.add(map);
            }

            SimpleAdapter sAdap;

            sAdap = new SimpleAdapter(getActivity(), MyArrList, R.layout.activity_column_bimbingan,
                    new String[] {"nama_mhs", "tgl_bimbingan", "topik_diskusi", "hasil_bimbingan"}, new int[]
                    {R.id.ColNama, R.id.ColTgl , R.id.ColTopik, R.id.ColHasil});

            listview1.setAdapter(sAdap);

            final AlertDialog.Builder viewDetail = new AlertDialog.Builder(context);
            //OnClick item

            listview1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long mylng) {
                    HashMap<String,String> map = (HashMap)parent.getItemAtPosition(position);
                    String idB = map.get("id_bimbingan");
                    String nmdsn = map.get("nama_mhs");
                    String tgl = map.get(ConfigDosenBimbingan.TAG_TGL_DSN).toString();
                    String topibim = map.get(ConfigDosenBimbingan.TAG_TOPIK_DSN).toString();
                    String hsil = map.get(ConfigDosenBimbingan.TAG_HASIL_DSN).toString();

                    //viewDetail.setIcon(android.R.drawable.star_on);
                    viewDetail.setTitle("DETAIL");
                    viewDetail.setMessage("Nama Mahasiswa : " + nmdsn + "\n\n" +
                            "Tanggal Bimbingan :" + tgl + "\n\n" +
                            "Topik Bimbingan :" + topibim + "\n\n" +
                            "Hasil Bimbingan :" + hsil);

                    viewDetail.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    //TODOAutoGenerated

                                    dialog.dismiss();
                                }
                            });
                    viewDetail.show();
                }
            });

        }
        catch (JSONException e){
            e.printStackTrace();
        }
    }

    public String getJSONUrl(String url){
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) { // Download OK
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download result..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }
}
