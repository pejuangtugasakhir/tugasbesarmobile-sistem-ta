package id.ac.usu.sistemta.koorTA;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import id.ac.usu.sistemta.R;
import id.ac.usu.sistemta.config.ConfigAssesment;


/**
 * A simple {@link Fragment} subclass.
 */
public class assesmentKoorta extends Fragment {
    Activity context;


    public assesmentKoorta() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        context=getActivity();
        return inflater.inflate(R.layout.koorta_assesment, container, false);
    }

    @Override
    public void onStart(){
        super.onStart();

        if(android.os.Build.VERSION.SDK_INT > 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        ListView listview1 = (ListView)context.findViewById(R.id.listView5);
        listview1.setFastScrollEnabled(true);

        String url = "http://sistemta.esy.es/sistem_ta/mod/koorta/select_assement.php";

        try{
            JSONArray data = new JSONArray(getJSONUrl(url));

            final ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
            HashMap<String,String> map;

            for (int i = 0; i<data.length(); i++){
                JSONObject c= data.getJSONObject(i);
                map = new HashMap<String,String>();
                map.put("id_exsum", c.getString("id_exsum"));
                map.put("tanggal_masuk_exsum", c.getString("tanggal_masuk_exsum"));
                map.put("nim", c.getString("nim"));
                map.put("nama_mhs", c.getString("nama_mhs"));
                map.put("judul_exsum", c.getString("judul_exsum"));
                map.put("rekomendasi_exsum", c.getString("rekomendasi_exsum"));
                map.put("status", c.getString("status"));
                map.put("nama_file", c.getString("nama_file"));
                MyArrList.add(map);
            }

            SimpleAdapter sAdap;

            sAdap = new SimpleAdapter(getActivity(), MyArrList, R.layout.activity_column_assesment,
                    new String[] {"tanggal_masuk_exsum","nim", "nama_mhs", "judul_exsum", "rekomendasi_exsum","status"}, new int[]
                    {R.id.ColTanggal, R.id.ColNim, R.id.ColNama , R.id.ColJudul, R.id.ColRekom, R.id.ColStatus});

            listview1.setAdapter(sAdap);

            final AlertDialog.Builder viewDetail = new AlertDialog.Builder(context);
            //OnClick item

            listview1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(context, koorta_assesment_confirm.class);

//                    HashMap<String, String> map = (HashMap) parent.getItemAtPosition(position);
                    HashMap<String, String> info = (HashMap<String, String>) parent.getItemAtPosition(position);

                    //Then access individual data items within the hash map using the key
//                    String personName = info.get("first_name");

                    String id_exsum = info.get("id_exsum");
                    String nim = info.get("nim");
                    String nama_file = info.get("nama_file");

                    intent.putExtra(ConfigAssesment.ASS_ID, id_exsum);
                    intent.putExtra(ConfigAssesment.ASS_NIM, nim);
                    intent.putExtra(ConfigAssesment.ASS_NAMA_FILE, nama_file);
                    startActivity(intent);
                }
            });

        }
        catch (JSONException e){
            e.printStackTrace();
        }
    }

    public String getJSONUrl(String url){
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) { // Download OK
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download result..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }
}
