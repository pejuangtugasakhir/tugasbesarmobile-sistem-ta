package id.ac.usu.sistemta.config;

public class ConfigDosenBimbingan {
    //address
    public static final String URL_ADD ="http://sistemta.esy.es/sistem_ta/mod/mhs/insert_bimbingan.php";
    public static final String URL_GET_SPINNER = "http://sistemta.esy.es/sistem_ta/mod/mhs/spinner.php?username=";
    public static final String URL_GET_BIMBINGAN_DSN = "http://sistemta.esy.es/sistem_ta/mod/dosen/select_mhs_bimbingan.php?username=";
    public static final String URL_SELECT_BIMBINGAN = "http://sistemta.esy.es/sistem_ta/mod/mhs/select_bimbingan.php?id_bimbingan=";
    public static final String URL_UPDATE_MHS = "http://sistemta.esy.es/sistem_ta/mod/mhs/update_bimbingan.php";
    public static final String URL_DELETE_MHS = "http://sistemta.esy.es/sistem_ta/mod/mhs/delete_bimbingan.php?id_bimbingan=";

    //Keys that will be used to send the request to php scripts
    public static final String KEY_EMP_IDBIM_DSN = "id_bimbingan";
    public static final String KEY_EMP_NAMAMHS_DSN = "nama_mhs";
    public static final String KEY_EMP_NAMADOSEN_DSN = "username";
    public static final String KEY_EMP_TGL_DSN = "tgl_bimbingan";
    public static final String KEY_EMP_TOPIK_DSN = "topik_diskusi";
    public static final String KEY_EMP_HASIL_DSN = "hasil_bimbingan";

    //JSON Tags
    public static final String TAG_JSON_ARRAY_DSN="result";
    public static final String TAG_NAMAMHS_DSN = "username";
    public static final String TAG_NAMADOSEN_DSN = "nama_dosen";
    public static final String TAG_TGL_DSN = "tgl_bimbingan";
    public static final String TAG_TOPIK_DSN = "topik_diskusi";
    public static final String TAG_HASIL_DSN = "hasil_bimbingan";

    public static final String MHS_ID = "id_bimbingan";
    public static final String MHS_NAMA= "nama_mhs";
    public static final String MHS_TGL = "tgl_bimbingan";
    public static final String MHS_TOPIK = "topik_diskusi";
    public static final String MHS_HASIL = "hasil_bimbingan";

}
