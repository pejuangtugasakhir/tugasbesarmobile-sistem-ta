package id.ac.usu.sistemta.tataUsaha;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import id.ac.usu.sistemta.R;
import id.ac.usu.sistemta.config.ConfigMhs;
import id.ac.usu.sistemta.handler.RequestHandler;

public class tu_update_mhs extends AppCompatActivity implements View.OnClickListener{

    private EditText editTextNim;
    private EditText editTextNama;
    private RadioGroup radioSex;
    private RadioButton radioButtonSex;
    private RadioButton radioButtonMale;
    private RadioButton radioButtonFemale;
    private EditText editTextAlamat;
    private EditText editTextEmail;

    private Button btnUpdate;
    private Button btnDelete;

    private String nim;
    private String nama;
    private String jenis;
    private String alamat;
    private String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tu_update_mhs);

        Intent intent = getIntent();

        nim = intent.getStringExtra(ConfigMhs.MHS_NIM);
        nama = intent.getStringExtra(ConfigMhs.MHS_NAMA);
        alamat = intent.getStringExtra(ConfigMhs.MHS_ALAMAT);
        email = intent.getStringExtra(ConfigMhs.MHS_EMAIL);
        jenis = intent.getStringExtra(ConfigMhs.MHS_JENIS);

        editTextNim = (EditText)findViewById(R.id.eNim);
        editTextNama = (EditText)findViewById(R.id.eNama);
        editTextAlamat = (EditText)findViewById(R.id.eAlamat);
        editTextEmail = (EditText)findViewById(R.id.eEmail);

        radioSex = (RadioGroup)findViewById(R.id.radioSex);
        radioButtonMale = (RadioButton)findViewById(R.id.laki);
        radioButtonFemale = (RadioButton)findViewById(R.id.perempuan);

        btnUpdate = (Button) findViewById(R.id.btnUpdate);
        btnDelete = (Button) findViewById(R.id.btnDelete);

        btnUpdate.setOnClickListener(this);
        btnDelete.setOnClickListener(this);

        editTextNim.setText(nim);
        editTextNama.setText(nama);

        if(jenis.equals("Laki-Laki")){
            radioButtonMale.setChecked(true);
            radioButtonFemale.setChecked(false);
        }else{
            radioButtonMale.setChecked(false);
            radioButtonFemale.setChecked(true);
        }

        editTextAlamat.setText(alamat);
        editTextEmail.setText(email);
        editTextNama.requestFocus();
        getMhs();
    }

    public void onClick(View v) {
        if(v == btnUpdate){
            updateMhs();
        }

        if(v == btnDelete){
            confirmDeleteMhs();
        }
    }

    private void getMhs(){
        class GetMhs extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;

            @Override
            protected void onPreExecute(){
                super.onPreExecute();
                loading = ProgressDialog.show(tu_update_mhs.this,"Fetching....","Wait...",false,false);
            }
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                showMhs(s);
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequestParam(ConfigMhs.URL_GET_MHS,nim);
                return s;
            }
        }

        GetMhs ge = new GetMhs();
        ge.execute();
    }

    private void showMhs(String json){
        try{
            JSONObject jsonObject = new JSONObject(json);
            JSONArray result = jsonObject.getJSONArray(ConfigMhs.TAG_JSON_ARRAY);
            JSONObject c = result.getJSONObject(0);
            String nama = c.getString(ConfigMhs.TAG_NAMA);
            String alamat = c.getString(ConfigMhs.TAG_ALAMAT);
            String email = c.getString(ConfigMhs.TAG_EMAIL);
            String jenis = c.getString(ConfigMhs.TAG_JENIS);

            editTextNama.setText(nama);
            editTextAlamat.setText(alamat);
            editTextEmail.setText(email);
            if(jenis.equals("Laki-Laki")){
                radioButtonMale.setChecked(true);
                radioButtonFemale.setChecked(false);
            }else{
                radioButtonMale.setChecked(false);
                radioButtonFemale.setChecked(true);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void updateMhs(){
        final String nama = editTextNama.getText().toString().trim();
        final String alamat = editTextAlamat.getText().toString().trim();
        final String email = editTextEmail.getText().toString().trim();

        int selectId = radioSex.getCheckedRadioButtonId();

        radioButtonSex = (RadioButton)findViewById(selectId);
        final String jenis = radioButtonSex.getText().toString();

        class UpdateMhs extends AsyncTask<Void,Void,String>{
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(tu_update_mhs.this,"Updating...","Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(tu_update_mhs.this, s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(Void... params) {
                HashMap<String,String> hashMap = new HashMap<>();
                hashMap.put(ConfigMhs.KEY_EMP_NIM,nim);
                hashMap.put(ConfigMhs.KEY_EMP_NAMA,nama);
                hashMap.put(ConfigMhs.KEY_EMP_JENIS,jenis);
                hashMap.put(ConfigMhs.KEY_EMP_ALAMAT,alamat);
                hashMap.put(ConfigMhs.KEY_EMP_EMAIL,email);

                RequestHandler rh = new RequestHandler();

                String s = rh.sendPostRequest(ConfigMhs.URL_UPDATE_MHS, hashMap);

                return s;
            }
        }

        UpdateMhs ue = new UpdateMhs();
        ue.execute();
    }

    private void deleteMhs(){
        class DeleteMhs extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(tu_update_mhs.this, "Updating...", "Wait...", false, false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(tu_update_mhs.this, s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequestParam(ConfigMhs.URL_DELETE_MHS, nim);
                return s;
            }
        }

        DeleteMhs de = new DeleteMhs();
        de.execute();
    }

    private void confirmDeleteMhs(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Are you sure you want to delete this mahasiswa?");

        alertDialogBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        deleteMhs();
                        startActivity(new Intent(tu_update_mhs.this,MainActivityTU.class));
                    }
                });

        alertDialogBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

}
