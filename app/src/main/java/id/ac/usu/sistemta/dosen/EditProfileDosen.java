package id.ac.usu.sistemta.dosen;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import id.ac.usu.sistemta.R;
import id.ac.usu.sistemta.config.ConfigProfilDosen;
import id.ac.usu.sistemta.handler.RequestHandler;
import id.ac.usu.sistemta.session.UserSessionManager;

public class EditProfileDosen extends AppCompatActivity implements View.OnClickListener{
    private EditText alamat,email,passbaru,passlama;
    private TextView namamhs,nimmhs;
    private Button edit,cancel;
    UserSessionManager session;
    String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dosen_edit_profile);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        namamhs = (TextView)findViewById(R.id.namamhs);
        nimmhs=(TextView)findViewById(R.id.nim);
        alamat=(EditText) findViewById(R.id.alamat);
        email=(EditText)findViewById(R.id.email);
        passbaru=(EditText)findViewById(R.id.passbaru);
        passlama=(EditText)findViewById(R.id.pass);

        edit=(Button) findViewById(R.id.buttonedit);
        cancel=(Button)findViewById(R.id.buttoncan);
        edit.setOnClickListener(this);
        cancel.setOnClickListener(this);

        getProfil();
    }

    private void getProfil(){
        class GetProf extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute(){
                super.onPreExecute();
                loading = ProgressDialog.show(EditProfileDosen.this,"Fetching....","Wait...",false,false);
            }
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                showProf(s);
            }
            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequestParam(ConfigProfilDosen.URL_SELECT_PROFIL_DOSEN,name);
                return s;
            }
        }
        GetProf gp = new GetProf();
        gp.execute();
    }


    private void showProf(String json){
        try{
            JSONObject jsonObject = new JSONObject(json);
            JSONArray result = jsonObject.getJSONArray(ConfigProfilDosen.TAG_JSON_ARRAY);
            JSONObject c = result.getJSONObject(0);
            String nims = c.getString(ConfigProfilDosen.TAG_NIP);
            String namas = c.getString(ConfigProfilDosen.TAG_NAMA_DOSEN);
            String alamats = c.getString(ConfigProfilDosen.TAG_ALAMAT_DOSEN);
            String emails = c.getString(ConfigProfilDosen.TAG_EMAIL_DOSEN);

            namamhs.setText(namas);
            nimmhs.setText(nims);
            alamat.setText(alamats);
            email.setText(emails);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_profil_dosen, menu);
        return true;
    }

    public void onStart(){

        super.onStart();
        session = new UserSessionManager(getApplicationContext());

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();

        // get name
        name = user.get(UserSessionManager.KEY_NAME);
    }

    @Override
    public void onClick(View v) {
        if(v==edit){
            editProfil();
        }
        else if(v==cancel){
            startActivity(new Intent(this, EditProfileDosen.class));
            finish();
        }
    }

    private void editProfil(){
        final String alamatstring= alamat.getText().toString().trim();
        final String emailstring= email.getText().toString().trim();
        final String passlma= passlama.getText().toString().trim();
        final String passbru= passbaru.getText().toString().trim();

        class addProfil extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(EditProfileDosen.this,"Edit...","Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(EditProfileDosen.this, s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(Void... v) {
                HashMap<String,String> params = new HashMap<>();
                params.put(ConfigProfilDosen.KEY_EMP_UNAMA,name);
                params.put(ConfigProfilDosen.KEY_EMP_ALAMAT_DOSEN,alamatstring);
                params.put(ConfigProfilDosen.KEY_EMP_EMAIL_DOSEN,emailstring);
                params.put(ConfigProfilDosen.KEY_EMP_PASSBARU_DOSEN, passbru);
                params.put(ConfigProfilDosen.KEY_EMP_PASSLAMA_DOSEN, passlma);

                RequestHandler rh = new RequestHandler();
                String res = rh.sendPostRequest(ConfigProfilDosen.URL_UPDATE_DOSEN, params);
                return res;
            }
        }

        addProfil ap = new addProfil();
        ap.execute();
    }
}
