package id.ac.usu.sistemta.menuUtama;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import id.ac.usu.sistemta.R;

public class JadwalTA extends AppCompatActivity {
    EditText search;
    SimpleAdapter sAdap;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_jadwal_ta);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            if(android.os.Build.VERSION.SDK_INT > 9){
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }

            ListView listview = (ListView)findViewById(R.id.listView3);
            listview.setFastScrollEnabled(true);

            String url = "http://sistemta.esy.es/sistem_ta/mod/jadwal/select_jadwal.php";

            try{
                JSONArray data = new JSONArray(getJSONUrl(url));

                final ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
                HashMap<String,String> map;

                for (int i = 0; i<data.length(); i++){
                    JSONObject c= data.getJSONObject(i);

                    map = new HashMap<String,String>();
                    map.put("tanggal_seminar", c.getString("tanggal_seminar"));
                    map.put("waktu_seminar", c.getString("waktu_seminar"));
                    map.put("nama_mhs", c.getString("nama_mhs"));
                    map.put("judul_ta", c.getString("judul_ta"));
                    map.put("status_kegiatan", c.getString("status_kegiatan"));
                    map.put("dosen_pembimbing", c.getString("dosen_pembimbing"));
//                    map.put("nim",c.getString("nim"));
                    MyArrList.add(map);
                }

                sAdap = new SimpleAdapter(JadwalTA.this, MyArrList, R.layout.activity_column_jadwal,
                        new String[] {"tanggal_seminar", "waktu_seminar", "nama_mhs", "judul_ta", "status_kegiatan", "dosen_pembimbing"}, new int[]
                        {R.id.ColTanggal, R.id.ColWaktu, R.id.ColNama , R.id.ColJudul, R.id.ColStatus, R.id.ColDoping });

                listview.setAdapter(sAdap);

                final AlertDialog.Builder viewDetail = new AlertDialog.Builder(this);
                //OnClick item

                listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> myAdapter, View myView, int position, long mylng) {
                        String sTanggal = MyArrList.get(position).get("tanggal_seminar").toString();
                        String sWaktu = MyArrList.get(position).get("waktu_seminar").toString();
                        String sNama = MyArrList.get(position).get("nama_mhs").toString();
                        String sJudul = MyArrList.get(position).get("judul_ta").toString();
                        String sStatus = MyArrList.get(position).get("status_kegiatan").toString();
                        String sDoping = MyArrList.get(position).get("dosen_pembimbing").toString();


                        //viewDetail.setIcon(android.R.drawable.star_on);
                        viewDetail.setTitle("DETAIL");
                        viewDetail.setMessage("Tanggal : " + sTanggal + "\n\n" + "Waktu :" + sWaktu + "\n\n" +
                                "Nama Mahasiswa :" + sNama + "\n\n" +
                                "Judul :" + sJudul + "\n\n" +
                                "Kegiatan :" + sStatus + "\n\n" +
                                "Doping :" + sDoping);

                        viewDetail.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        //TODOAutoGenerated

                                        dialog.dismiss();
                                    }
                                });
                        viewDetail.show();
                    }
                });

                search=(EditText)findViewById(R.id.searchView);
                search.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        JadwalTA.this.sAdap.getFilter().filter(s);
                    }
                    @Override
                    public void afterTextChanged(Editable s) {
                    }
                });
            }
            catch (JSONException e){
                e.printStackTrace();
            }
        }

        public String getJSONUrl(String url){
            StringBuilder str = new StringBuilder();
            HttpClient client = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(url);
            try {
                HttpResponse response = client.execute(httpGet);
                StatusLine statusLine = response.getStatusLine();
                int statusCode = statusLine.getStatusCode();
                if (statusCode == 200) { // Download OK
                    HttpEntity entity = response.getEntity();
                    InputStream content = entity.getContent();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        str.append(line);
                    }
                } else {
                    Log.e("Log", "Failed to download result..");
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return str.toString();
        }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_jadwal_ta, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
