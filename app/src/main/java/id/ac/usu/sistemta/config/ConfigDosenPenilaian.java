package id.ac.usu.sistemta.config;

public class ConfigDosenPenilaian {
    //address
    public static final String URL_ADD ="http://sistemta.esy.es/sistem_ta/mod/dosen/insert_borang_masukan.php";
    public static final String URL_GET_SPINNER = "http://sistemta.esy.es/sistem_ta/mod/dosen/spinner.php?username=";
    public static final String URL_GET_MHS_SEMINAR = "http://sistemta.esy.es/sistem_ta/mod/dosen/select_mhs_seminar.php?username=";
    public static final String URL_GET_MHS_SEMHAS = "http://sistemta.esy.es/sistem_ta/mod/dosen/select_mhs_semhas.php?username=";
    public static final String URL_GET_MHS_SIDANG = "http://sistemta.esy.es/sistem_ta/mod/dosen/select_mhs_sidang.php?username=";
    public static final String URL_ADD_SEMHAS ="http://sistemta.esy.es/sistem_ta/mod/dosen/insert_borang_penilaian_semhas.php";
    public static final String URL_GET_MHS_BIM ="http://sistemta.esy.es/sistem_ta/mod/dosen/select_daftar_mhs_bimbingan.php?username=";

    //Keys that will be used to send the request to php scripts

    public static final String KEY_EMP_NAMAMHS = "nama_mhs";
    public static final String KEY_EMP_NAMADOSEN = "username";
    public static final String KEY_EMP_TGL = "tgl_bimbingan";
    public static final String KEY_EMP_TOPIK = "ltr_belakang";
    public static final String KEY_EMP_IDBIM = "id_jadwal";
    public static final String KEY_EMP_HASIL = "rms_masalah";
    public static final String KEY_EMP_TUPEL = "tujuan_penelitian";
    public static final String KEY_EMP_LANTER = "landasan_teori";
    public static final String KEY_EMP_METOD = "metodologi";
    public static final String KEY_EMP_DAPUT = "dftr_pustaka";

    public static final String KEY_EMP_NAMAMHS_SEMHAS = "nama_mhs";
    public static final String KEY_EMP_NAMADOSEN_SEMHAS = "username";
    public static final String KEY_EMP_TGL_SEMHAS = "tgl_nilai_semhas";
    public static final String KEY_EMP_NILAI_SEMHAS = "n_semhas";



    //JSON Tags
    public static final String TAG_JSON_ARRAY_DSN="result";
    public static final String TAG_NAMA_MHS = "nama_mhs";
    public static final String TAG_WKT_SEMINAR = "waktu_seminar";
    public static final String TAG_NIM = "nim";
    public static final String TAG_JUDUL = "judul_ta";
    public static final String TAG_DSN_PEMBIMBING = "dosen_pembimbing";
    public static final String TAG_NILAI_SEMHAS = "n_semhas";

    public static final String JADWAL_ID = "id_jadwal";
    public static final String NILAI_ID = "id_nilai";
    public static final String MHS_NAMA= "nama_mhs";
    public static final String NILAI_SEMHAS = "n_semhas";
    public static final String MHS_JUDUL = "judul_ta";
    public static final String TGL_SEMHAS = "tgl_nilai_semhas";
    public static final String MHS_HASIL = "hasil_bimbingan";

}
