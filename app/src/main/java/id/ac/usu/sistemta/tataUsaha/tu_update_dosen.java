package id.ac.usu.sistemta.tataUsaha;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import id.ac.usu.sistemta.R;
import id.ac.usu.sistemta.config.ConfigDosen;
import id.ac.usu.sistemta.handler.RequestHandler;

public class tu_update_dosen extends AppCompatActivity implements View.OnClickListener {

    private EditText editTextNip;
    private EditText editTextNama;
    private EditText editTextAlamat;
    private EditText editTextEmail;

    private Button btnUpdate;
    private Button btnDelete;

    private String nip;
    private String nama;
    private String alamat;
    private String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tu_update_dosen);

        Intent intent = getIntent();

        nip = intent.getStringExtra(ConfigDosen.DOSEN_NIP);
        nama = intent.getStringExtra(ConfigDosen.DOSEN_NAMA);
        alamat = intent.getStringExtra(ConfigDosen.DOSEN_ALAMAT);
        email = intent.getStringExtra(ConfigDosen.DOSEN_EMAIL);

        editTextNip = (EditText)findViewById(R.id.eeNip);
        editTextNama = (EditText)findViewById(R.id.eeNama);
        editTextAlamat = (EditText)findViewById(R.id.eeAlamat);
        editTextEmail = (EditText)findViewById(R.id.eeEmail);

        btnUpdate = (Button) findViewById(R.id.btnUpdate);
        btnDelete = (Button) findViewById(R.id.btnDelete);

        btnUpdate.setOnClickListener(this);
        btnDelete.setOnClickListener(this);

        editTextNip.setText(nip);
        editTextNama.setText(nama);
        editTextAlamat.setText(alamat);
        editTextEmail.setText(email);
        editTextNama.requestFocus();
        getDosen();
    }

    public void onClick(View v) {
        if(v == btnUpdate){
            updateDosen();
        }

        if(v == btnDelete){
            confirmDeleteDosen();
        }
    }

    private void getDosen(){
        class GetDosen extends AsyncTask<Void,Void,String>{
            ProgressDialog loading;

            @Override
            protected void onPreExecute(){
                super.onPreExecute();
                loading = ProgressDialog.show(tu_update_dosen.this,"Fetching....","Wait...",false,false);
            }
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                showDosen(s);
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequestParam(ConfigDosen.URL_GET_DSN,nip);
                return s;
            }
        }

        GetDosen ge = new GetDosen();
        ge.execute();
    }

    private void showDosen(String json){
        try{
            JSONObject jsonObject = new JSONObject(json);
            JSONArray result = jsonObject.getJSONArray(ConfigDosen.TAG_JSON_ARRAY);
            JSONObject c = result.getJSONObject(0);
            String nama = c.getString(ConfigDosen.TAG_NAMA);
            String alamat = c.getString(ConfigDosen.TAG_ALAMAT);
            String email = c.getString(ConfigDosen.TAG_EMAIL);

            editTextNama.setText(nama);
            editTextAlamat.setText(alamat);
            editTextEmail.setText(email);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void updateDosen(){
        final String nama = editTextNama.getText().toString().trim();
        final String alamat = editTextAlamat.getText().toString().trim();
        final String email = editTextEmail.getText().toString().trim();

        class UpdateDosen extends AsyncTask<Void,Void,String>{
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(tu_update_dosen.this,"Updating...","Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(tu_update_dosen.this, s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(Void... params) {
                HashMap<String,String> hashMap = new HashMap<>();
                hashMap.put(ConfigDosen.KEY_EMP_NIP,nip);
                hashMap.put(ConfigDosen.KEY_EMP_NAMA,nama);
                hashMap.put(ConfigDosen.KEY_EMP_ALAMAT,alamat);
                hashMap.put(ConfigDosen.KEY_EMP_EMAIL,email);

                RequestHandler rh = new RequestHandler();

                String s = rh.sendPostRequest(ConfigDosen.URL_UPDATE_DSN, hashMap);

                return s;
            }
        }

        UpdateDosen ue = new UpdateDosen();
        ue.execute();
    }

    private void deleteDosen(){
        class DeleteDosen extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(tu_update_dosen.this, "Updating...", "Wait...", false, false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(tu_update_dosen.this, s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequestParam(ConfigDosen.URL_DELETE_DSN, nip);
                return s;
            }
        }

        DeleteDosen de = new DeleteDosen();
        de.execute();
    }

    private void confirmDeleteDosen(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Are you sure you want to delete this dosen?");

        alertDialogBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        deleteDosen();
                        startActivity(new Intent(tu_update_dosen.this,MainActivityTU.class));
                    }
                });

        alertDialogBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

}
