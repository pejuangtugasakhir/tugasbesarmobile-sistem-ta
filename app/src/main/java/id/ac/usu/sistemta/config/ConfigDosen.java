package id.ac.usu.sistemta.config;

/**
 * Created by yeni on 11/25/2015.
 */
public class ConfigDosen {
    //address

    public static final String URL_ADD ="http://sistemta.esy.es/sistem_ta/mod/dosen/insert_dosen.php";
    public static final String URL_GET_ALL = "http://sistemta.esy.es/sistem_ta/mod/dosen/select_dosen.php";
    public static final String URL_GET_DSN = "http://sistemta.esy.es/sistem_ta/mod/dosen/select_dosen_one.php?nip=";
    public static final String URL_UPDATE_DSN = "http://sistemta.esy.es/sistem_ta/mod/dosen/update_dosen.php";
    public static final String URL_DELETE_DSN = "http://sistemta.esy.es/sistem_ta/mod/dosen/delete_dosen.php?nip=";

    //Keys that will be used to send the request to php scripts
    public static final String KEY_EMP_NIP = "nip";
    public static final String KEY_EMP_NAMA = "nama_dosen";
    public static final String KEY_EMP_ALAMAT = "alamat_dosen";
    public static final String KEY_EMP_EMAIL = "email_dosen";
    public static final String KEY_EMP_USERNAME = "username";
    public static final String KEY_EMP_PASSWORD = "password";

    //JSON Tags
    public static final String TAG_JSON_ARRAY="result";
    public static final String TAG_NIP = "nip";
    public static final String TAG_NAMA = "nama_dosen";
    public static final String TAG_ALAMAT = "alamat_dosen";
    public static final String TAG_EMAIL = "email_dosen";

    public static final String DOSEN_NIP = "nip";
    public static final String DOSEN_NAMA= "nama_dosen";
    public static final String DOSEN_ALAMAT = "alamat_dosen";
    public static final String DOSEN_EMAIL = "email_dosen";

}
