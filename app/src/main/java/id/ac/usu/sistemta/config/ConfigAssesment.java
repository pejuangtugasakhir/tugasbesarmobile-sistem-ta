package id.ac.usu.sistemta.config;

/**
 * Created by Endang on 1/2/2016.
 */
public class ConfigAssesment {
    public static final String URL_CONFIRM ="http://sistemta.esy.es/sistem_ta/mod/koorta/confirm_assesment.php";
    public static final String URL_CONFIRM2 ="http://sistemta.esy.es/sistem_ta/mod/koorta/confirm_assesment2.php";
    //Keys that will be used to send the request to php scripts

    public static final String KEY_EMP_NIM = "nim";
    public static final String KEY_EMP_DPG1 = "doping1";
    public static final String KEY_EMP_DPG2 = "doping2";
    public static final String KEY_EMP_ID = "id_exsum";
    public static final String KEY_EMP_KETERANGAN = "keterangan_exsum";
    public static final String KEY_EMP_STATUS = "status";

    //JSON Tags
    public static final String TAG_JSON_ARRAY="result";
    public static final String TAG_ID = "id_exsum";
    public static final String TAG_NIM = "nim";
    public static final String TAG_NAMA_FILE = "nama_file";

    public static final String ASS_ID = "id_exsum";
    public static final String ASS_NIM = "nim";
    public static final String ASS_NAMA_FILE= "nama_file";

}
