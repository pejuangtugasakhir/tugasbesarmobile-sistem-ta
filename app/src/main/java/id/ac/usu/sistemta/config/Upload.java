package id.ac.usu.sistemta.config;

/**
 * Created by acer on 25/12/2015.
 */
public class Upload {
    //Lokasi file upload_file.php diserver
    //public static final String FILEUP = "http://sistemta.esy.es/sistem_ta/mod/mhs/upload_file.php";

    public static final String KEY_IMAGE = "image";
    public static final String KEY_DIRECTORY = "directory";
    public static final String VALUE_DIRECTORY = "Data";
    public static final String KEY_CODE = "kode";
    public static final String KEY_MESSAGE = "pesan";
    public static final String VALUE_CODE_SUCCESS = "2";
    public static final String VALUE_CODE_FAILED = "1";
    public static final String VALUE_CODE_MISSING = "0";
    public static final String KEY_EMP_NAMAMHS = "username";

    public static final int CAMERA_IMAGE_CODE = 0;
    public static final int CAMERA_VIDEO_CODE = 1;
    public static final int FILE_MANAGER_CODE = 2;
    public static final int AUDIO_CODE = 3;

    //Waktu upload hingga sebelum time out, jika koneksi lambat, perbesar nilai dibawah
    public static final int SOCKET_TIMEOUT = 1000 * 100;
    //Berapa kali perulangan, setelah koneksi gagal
    public static final int RETRIES = 0;

}
