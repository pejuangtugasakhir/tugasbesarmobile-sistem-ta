package id.ac.usu.sistemta.config;

/**
 * Created by acer on 06/12/2015.
 */
public class ConfigProfil {
    //address
    public static final String URL_SELECT_PROFIL = "http://sistemta.esy.es/sistem_ta/mod/mhs/select_profil.php?username=";
    public static final String URL_UPDATE = "http://sistemta.esy.es/sistem_ta/mod/mhs/update_profilmhs.php";
    public static final String URL_SELECT_DOPING = "http://sistemta.esy.es/sistem_ta/mod/mhs/select_doping.php?username=";
    public static final String URL_SELECT_NILAI = "http://sistemta.esy.es/sistem_ta/mod/mhs/select_nilai.php?username=";


    //Keys that will be used to send the request to php scripts
    public static final String KEY_EMP_ALAMAT = "alamat_mhs";
    public static final String KEY_EMP_EMAIL = "email";
    public static final String KEY_EMP_PASSBARU = "passwordbaru";
    public static final String KEY_EMP_PASSLAMA = "password";
    public static final String KEY_EMP_NAMA = "nama_mhs";
    public static final String KEY_EMP_UNAMA = "username";
    public static final String KEY_EMP_NIM = "nim";

    //JSON Tags
    public static final String TAG_JSON_ARRAY="result";
    public static final String TAG_JSON_ARRAY2="result2";
    public static final String TAG_JSON_ARRAY3="result3";
    public static final String TAG_ALAMAT = "alamat_mhs";
    public static final String TAG_EMAIL = "email";
    public static final String TAG_PASSBARU = "passwordbaru";
    public static final String TAG_PASSLAMA = "password";
    public static final String TAG_NAMA = "nama_mhs";
    public static final String TAG_NIM = "nim";
    public static final String TAG_NAMADOSEN = "nama_dosen";
    public static final String TAG_NAMADOSENPENG = "doping";
    public static final String TAG_NILAISEMHAS="nilai_semhas";
    public static final String TAG_NILAISIDANG="nilai_sidang";
    public static final String TAG_NILAITOTAL="nilai_total";

    public static final String MHS_ALAMAT = "alamat_mhs";
    public static final String MHS_EMAIL = "email";
    public static final String MHS_PASSBARU = "passwordbaru";
    public static final String MHS_PASSLAMA = "password";
    public static final String MHS_NAMA = "nama_mhs";
    public static final String MHS_NIM = "nim";

}
