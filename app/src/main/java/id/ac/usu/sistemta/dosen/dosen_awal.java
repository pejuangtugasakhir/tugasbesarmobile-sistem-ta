package id.ac.usu.sistemta.dosen;

import android.app.Activity;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.HashMap;

import id.ac.usu.sistemta.R;
import id.ac.usu.sistemta.session.UserSessionManager;

public class dosen_awal extends Fragment {
    Activity context;
    ImageButton bim;
    UserSessionManager session;
    TextView username;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context= getActivity();
        return inflater.inflate(R.layout.dosen_awal, null);
    }

    @Override
    public void onStart() {
        super.onStart();
        session = new UserSessionManager(context.getApplicationContext());

        username = (TextView) context.findViewById(R.id.user);

        if (session.checkLogin())
            context.finish();

        if (session.checkLogin())
            context.finish();

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();

        // get name
        String name = user.get(UserSessionManager.KEY_NAME);

        // Show user data on activity

        username.setText(Html.fromHtml(name));

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
    }
}
