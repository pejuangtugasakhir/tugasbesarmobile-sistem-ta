package id.ac.usu.sistemta.tataUsaha;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import id.ac.usu.sistemta.R;
import id.ac.usu.sistemta.config.ConfigDosen;
import id.ac.usu.sistemta.tataUsaha.tu_tambah_dosen;


/**
 * A simple {@link Fragment} subclass.
 */
public class Tu_dosen extends Fragment {
    ImageButton add;
    Activity context;

    public Tu_dosen() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        context=getActivity();
        return inflater.inflate(R.layout.tu_dosen, container, false);
    }
    @Override
    public void onStart(){
        super.onStart();

        add = (ImageButton)context.findViewById(R.id.add_dosen);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent m = new Intent(context, tu_tambah_dosen.class);
                startActivity(m);
            }
        });

        if(android.os.Build.VERSION.SDK_INT > 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        ListView listview1 = (ListView)context.findViewById(R.id.listView4);
        listview1.setFastScrollEnabled(true);

        String url = "http://sistemta.esy.es/sistem_ta/mod/dosen/select_dosen.php";

        try{
            JSONArray data = new JSONArray(getJSONUrl(url));

            final ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
            HashMap<String,String> map;

            for (int i = 0; i<data.length(); i++){
                JSONObject c= data.getJSONObject(i);

                map = new HashMap<String,String>();
                map.put("nip", c.getString("nip"));
                map.put("nama_dosen", c.getString("nama_dosen"));
                map.put("alamat_dosen", c.getString("alamat_dosen"));
                map.put("email_dosen", c.getString("email_dosen"));
                MyArrList.add(map);
            }

            SimpleAdapter sAdap;

            sAdap = new SimpleAdapter(getActivity(), MyArrList, R.layout.activity_column_dosen,
                    new String[] {"nip", "nama_dosen", "alamat_dosen", "email_dosen"}, new int[]
                    {R.id.ColNip, R.id.ColNama_dsn, R.id.ColAlamat , R.id.ColEmail});

            listview1.setAdapter(sAdap);

            final AlertDialog.Builder viewDetail = new AlertDialog.Builder(context);
            //OnClick item

            listview1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(context, tu_update_dosen.class);

                    HashMap<String,String> map = (HashMap)parent.getItemAtPosition(position);
                    String nipDosen = map.get(ConfigDosen.TAG_NIP).toString();
                    String namaDosen = map.get(ConfigDosen.TAG_NAMA).toString();
                    String alamatDosen = map.get(ConfigDosen.TAG_ALAMAT).toString();
                    String emailDosen = map.get(ConfigDosen.TAG_EMAIL).toString();

                    intent.putExtra(ConfigDosen.DOSEN_NIP,nipDosen);
                    intent.putExtra(ConfigDosen.DOSEN_NAMA,namaDosen);
                    intent.putExtra(ConfigDosen.DOSEN_ALAMAT,alamatDosen);
                    intent.putExtra(ConfigDosen.DOSEN_EMAIL,emailDosen);

                    startActivity(intent);
                }
            });
        }
        catch (JSONException e){
            e.printStackTrace();
        }
    }

    public String getJSONUrl(String url){
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) { // Download OK
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download result..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }

}
