package id.ac.usu.sistemta.tataUsaha;



import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import id.ac.usu.sistemta.R;
/**
 * A simple {@link Fragment} subclass.
 */
public class HomeTU extends Fragment {

    public HomeTU() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.tu_home, null);
    }}
