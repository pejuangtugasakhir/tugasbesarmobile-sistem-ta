package id.ac.usu.sistemta.config;

public class ConfigMhsBimbingan {
    //address
    public static final String URL_ADD ="http://sistemta.esy.es/sistem_ta/mod/mhs/insert_bimbingan.php";
    public static final String URL_GET_SPINNER = "http://sistemta.esy.es/sistem_ta/mod/mhs/spinner.php?username=";
    public static final String URL_GET_BIMBINGAN = "http://sistemta.esy.es/sistem_ta/mod/mhs/select_bimbingan_list.php?username=";
    public static final String URL_SELECT_BIMBINGAN = "http://sistemta.esy.es/sistem_ta/mod/mhs/select_bimbingan.php?id_bimbingan=";
    public static final String URL_UPDATE_MHS = "http://sistemta.esy.es/sistem_ta/mod/mhs/update_bimbingan.php";
    public static final String URL_DELETE_MHS = "http://sistemta.esy.es/sistem_ta/mod/mhs/delete_bimbingan.php?id_bimbingan=";

    //Keys that will be used to send the request to php scripts
    public static final String KEY_EMP_IDBIM = "id_bimbingan";
    public static final String KEY_EMP_NAMAMHS = "username";
    public static final String KEY_EMP_NAMADOSEN = "nama_dosen";
    public static final String KEY_EMP_TGL = "tgl_bimbingan";
    public static final String KEY_EMP_TOPIK = "topik_diskusi";
    public static final String KEY_EMP_HASIL = "hasil_bimbingan";

    //JSON Tags
    public static final String TAG_JSON_ARRAY="result";
    public static final String TAG_NAMAMHS = "username";
    public static final String TAG_NAMADOSEN = "nama_dosen";
    public static final String TAG_TGL = "tgl_bimbingan";
    public static final String TAG_TOPIK = "topik_diskusi";
    public static final String TAG_HASIL = "hasil_bimbingan";

    public static final String MHS_ID = "id_bimbingan";
    public static final String MHS_NAMADOSEN= "nama_dosen";
    public static final String MHS_TGL = "tgl_bimbingan";
    public static final String MHS_TOPIK = "topik_diskusi";
    public static final String MHS_HASIL = "hasil_bimbingan";

}
