package id.ac.usu.sistemta.tataUsaha;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.HashMap;

import id.ac.usu.sistemta.R;
import id.ac.usu.sistemta.config.ConfigMhs;
import id.ac.usu.sistemta.handler.RequestHandler;

public class tu_tambah_mhs extends AppCompatActivity implements View.OnClickListener{
    private EditText editTextNim;
    private EditText editTextNama;
    private RadioGroup radioSexGroup;
    private RadioButton radioSexButton;
    private EditText editTextAlamat;
    private EditText editTextEmail;
    private EditText editTextUsername;
    private EditText editTextPassword;

    private Button addMhs;
    private Button reset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tu_tambah_mhs);

        editTextNim = (EditText) findViewById(R.id.eNim);
        editTextNama = (EditText) findViewById(R.id.eNama);
        radioSexGroup = (RadioGroup) findViewById(R.id.radioSex);
        editTextAlamat = (EditText) findViewById(R.id.eAlamat);
        editTextEmail = (EditText) findViewById(R.id.eEmail);
        editTextUsername = (EditText)findViewById(R.id.eUsername);
        editTextPassword = (EditText)findViewById(R.id.ePassword);

        addMhs = (Button)findViewById(R.id.add_mhs);
        reset = (Button)findViewById(R.id.btnReset);

        addMhs.setOnClickListener(this);
        reset.setOnClickListener(this);
    }
    public void onClick(View v) {
        if(v == addMhs){
            addMhs();
        }
        if(v ==reset){
            reset();
        }
    }

    private void addMhs(){

        final String nim = editTextNim.getText().toString().trim();
        final String nama = editTextNama.getText().toString().trim();
        final String alamat = editTextAlamat.getText().toString().trim();
        final String email = editTextEmail.getText().toString().trim();

        final String username = editTextUsername.getText().toString().trim();
        final String password = editTextPassword.getText().toString().trim();

        int selectId = radioSexGroup.getCheckedRadioButtonId();

        radioSexButton = (RadioButton)findViewById(selectId);
        final String jenis = radioSexButton.getText().toString();

        class AddMhs extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(tu_tambah_mhs.this,"Adding...","Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(tu_tambah_mhs.this, s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(Void... v) {
                HashMap<String,String> params = new HashMap<>();
                params.put(ConfigMhs.KEY_EMP_NIM,nim);
                params.put(ConfigMhs.KEY_EMP_NAMA,nama);
                params.put(ConfigMhs.KEY_EMP_JENIS,jenis);
                params.put(ConfigMhs.KEY_EMP_ALAMAT, alamat);
                params.put(ConfigMhs.KEY_EMP_EMAIL, email);
                params.put(ConfigMhs.KEY_EMP_USERNAME, username);
                params.put(ConfigMhs.KEY_EMP_PASSWORD, password);

                RequestHandler rh = new RequestHandler();
                String res = rh.sendPostRequest(ConfigMhs.URL_ADD, params);
                return res;
            }
        }

        if(nim.isEmpty()){
            Toast.makeText(getBaseContext(),"Masukkan NIM", Toast.LENGTH_LONG).show();
        }else if(nama.isEmpty()){
            Toast.makeText(getBaseContext(),"Masukkan Nama", Toast.LENGTH_LONG).show();
        }else if(alamat.isEmpty()){
            Toast.makeText(getBaseContext(),"Masukkan Alamat", Toast.LENGTH_LONG).show();
        }else if(email.isEmpty()){
            Toast.makeText(getBaseContext(),"Masukkan Email", Toast.LENGTH_LONG).show();
        }else if(username.isEmpty()){
            Toast.makeText(getBaseContext(),"Masukkan Username", Toast.LENGTH_LONG).show();
        }else if(password.isEmpty()){
            Toast.makeText(getBaseContext(),"Masukkan Password", Toast.LENGTH_LONG).show();
        }else if(nim.isEmpty() || nama.isEmpty() || alamat.isEmpty() || email.isEmpty()){
            Toast.makeText(getBaseContext(),"Lengkapi Data", Toast.LENGTH_LONG).show();
        }
        else{
            AddMhs ae = new AddMhs();
            ae.execute();

            editTextNim.setText(" ");
            editTextNama.setText(" ");
            editTextAlamat.setText(" ");
            editTextEmail.setText(" ");
            editTextUsername.setText(" ");
            editTextPassword.setText("");
            editTextNim.requestFocus();
        }
    }
    private void reset() {
        editTextNim.setText(" ");
        editTextNama.setText(" ");
        editTextAlamat.setText(" ");
        editTextEmail.setText(" ");
        editTextUsername.setText(" ");
        editTextPassword.setText("");
        editTextNim.requestFocus();
    }
}
