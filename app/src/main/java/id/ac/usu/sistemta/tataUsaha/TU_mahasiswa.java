package id.ac.usu.sistemta.tataUsaha;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import id.ac.usu.sistemta.R;
import id.ac.usu.sistemta.config.ConfigMhs;


/**
 * A simple {@link Fragment} subclass.
 */
public class TU_mahasiswa extends Fragment {

    ImageButton add_mhs;
    Activity context;
    public TU_mahasiswa() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        context = getActivity();
        return inflater.inflate(R.layout.tu_mahasiswa, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        add_mhs = (ImageButton) context.findViewById(R.id.add_mhs);
        add_mhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent m = new Intent(context, tu_tambah_mhs.class);
                startActivity(m);
            }
        });

        if(android.os.Build.VERSION.SDK_INT > 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        ListView listview1 = (ListView)context.findViewById(R.id.listView5);
        listview1.setFastScrollEnabled(true);

        String url = "http://sistemta.esy.es/sistem_ta/mod/mhs/select_mhs.php";

        try{
            JSONArray data = new JSONArray(getJSONUrl(url));

            final ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
            HashMap<String,String> map;

            for (int i = 0; i<data.length(); i++){
                JSONObject c= data.getJSONObject(i);

                map = new HashMap<String,String>();
                map.put("nim", c.getString("nim"));
                map.put("nama_mhs", c.getString("nama_mhs"));
                map.put("jenis_kelamin", c.getString("jenis_kelamin"));
                map.put("alamat_mhs", c.getString("alamat_mhs"));
                map.put("email", c.getString("email"));
                MyArrList.add(map);
            }

            SimpleAdapter sAdap;

            sAdap = new SimpleAdapter(getActivity(), MyArrList, R.layout.activity_column_mhs,
                    new String[] {"nim", "nama_mhs", "jenis_kelamin", "alamat_mhs", "email"}, new int[]
                    {R.id.ColNim, R.id.ColNama, R.id.ColJenis , R.id.ColAlamat, R.id.ColEmail});

            listview1.setAdapter(sAdap);

            final AlertDialog.Builder viewDetail = new AlertDialog.Builder(context);
            //OnClick item

            listview1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(context, tu_update_mhs.class);

                    HashMap<String, String> map = (HashMap) parent.getItemAtPosition(position);
                    String nipDosen = map.get(ConfigMhs.TAG_NIM).toString();
                    String namaDosen = map.get(ConfigMhs.TAG_NAMA).toString();
                    String jenis = map.get(ConfigMhs.TAG_JENIS).toString();
                    String alamatDosen = map.get(ConfigMhs.TAG_ALAMAT).toString();
                    String emailDosen = map.get(ConfigMhs.TAG_EMAIL).toString();

                    intent.putExtra(ConfigMhs.MHS_NIM, nipDosen);
                    intent.putExtra(ConfigMhs.MHS_NAMA, namaDosen);
                    intent.putExtra(ConfigMhs.MHS_JENIS, namaDosen);
                    intent.putExtra(ConfigMhs.MHS_ALAMAT, alamatDosen);
                    intent.putExtra(ConfigMhs.MHS_EMAIL, namaDosen);

                    startActivity(intent);
                }
            });

        }
        catch (JSONException e){
            e.printStackTrace();
        }
    }

    public String getJSONUrl(String url){
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) { // Download OK
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download result..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }
}