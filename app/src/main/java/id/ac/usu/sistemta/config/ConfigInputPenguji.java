package id.ac.usu.sistemta.config;

/**
 * Created by Citi Fatimah on 1/8/2016.
 */
public class ConfigInputPenguji {
    //address
    public static final String URL_INPUT_PENGUJI = "http://sistemta.esy.es/sistem_ta/mod/koorta/insert_penguji.php";
    public static final String URL_SELECT_PENGUJI_LIST = "http://sistemta.esy.es/sistem_ta/mod/koorta/select_penguji_one.php?nim=";

    //Keys that will be used to send the request to php scripts
    public static final String KEY_EMP_NIM = "nim";
    public static final String KEY_EMP_NAMA = "nama_mhs";
    public static final String KEY_EMP_DOSENPENGUJISATU = "dosenpengujisatu";
    public static final String KEY_EMP_DOSENPENGUJIDUA = "dosenpengujidua";
    public static final String KEY_EMP_DOSENPEMBIMBINGSATU = "doping1";

    //JSON Tags
    public static final String TAG_JSON_ARRAY="result";
    public static final String TAG_NIMMHS = "nim";
    public static final String TAG_NAMAMHS = "nama_mhs";
    public static final String TAG_NAMADOPINGSATU = "doping1";

}
