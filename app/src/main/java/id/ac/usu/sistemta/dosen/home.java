package id.ac.usu.sistemta.dosen;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import id.ac.usu.sistemta.R;
import id.ac.usu.sistemta.session.UserSessionManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class home extends Fragment {
    Activity context;
    EditText search;
    SimpleAdapter sAdap;
    UserSessionManager session;

    public home() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        context = getActivity();

        return inflater.inflate(R.layout.dosen_home, container, false);
    }

    public void onStart(){

        super.onStart();
        session = new UserSessionManager(context.getApplicationContext());


        if(session.checkLogin())
            context.finish();

        if(session.checkLogin())
            context.finish();

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();

        // get name
        String name = user.get(UserSessionManager.KEY_NAME);

        // Show user data on activity

        if(android.os.Build.VERSION.SDK_INT > 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        ListView listview1 = (ListView)context.findViewById(R.id.listView5);
        listview1.setFastScrollEnabled(true);

        String url = "http://sistemta.esy.es/sistem_ta/mod/jadwal/select_jadwal.php";

        try{
            JSONArray data = new JSONArray(getJSONUrl(url));

            final ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
            HashMap<String,String> map;

            for (int i = 0; i<data.length(); i++){
                JSONObject c= data.getJSONObject(i);

                map = new HashMap<String,String>();
                map.put("tanggal_seminar", c.getString("tanggal_seminar"));
                map.put("waktu_seminar", c.getString("waktu_seminar"));
                map.put("nama_mhs", c.getString("nama_mhs"));
                map.put("judul_ta", c.getString("judul_ta"));
                map.put("status_kegiatan", c.getString("status_kegiatan"));
                map.put("dosen_pembimbing", c.getString("dosen_pembimbing"));
//                map.put("nim", c.getString("nim"));
                MyArrList.add(map);
            }

            sAdap = new SimpleAdapter(getActivity(), MyArrList, R.layout.activity_column_jadwal,
                    new String[] {"tanggal_seminar", "waktu_seminar","nama_mhs", "judul_ta", "status_kegiatan", "dosen_pembimbing"}, new int[]
                    {R.id.ColTanggal, R.id.ColWaktu, R.id.ColNama , R.id.ColJudul, R.id.ColStatus, R.id.ColDoping });

            listview1.setAdapter(sAdap);

            final AlertDialog.Builder viewDetail = new AlertDialog.Builder(context);
            //OnClick item

            listview1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> myAdapter, View myView, int position, long mylng) {
                    String sTanggal = MyArrList.get(position).get("tanggal_seminar").toString();
                    String sWaktu = MyArrList.get(position).get("waktu_seminar").toString();
                    String sNama = MyArrList.get(position).get("nama_mhs").toString();
                    String sJudul = MyArrList.get(position).get("judul_ta").toString();
                    String sStatus = MyArrList.get(position).get("status_kegiatan").toString();
                    String sDoping = MyArrList.get(position).get("dosen_pembimbing").toString();

                    viewDetail.setTitle("DETAIL");
                    viewDetail.setMessage("Tanggal : " + sTanggal + "\n\n" + "Waktu :" + sWaktu + "\n\n" +
                            "Nama Mahasiswa :" + sNama + "\n\n" +
                            "Judul :" + sJudul + "\n\n" +
                            "Kegiatan :" + sStatus + "\n\n" +
                            "Doping :" + sDoping);

                    viewDetail.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    //TODOAutoGenerated
                                    dialog.dismiss();
                                }
                            });
                    viewDetail.show();
                }
            });
            search=(EditText) context.findViewById(R.id.searchView);
            search.addTextChangedListener(new TextWatcher() {

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    home.this.sAdap.getFilter().filter(s);
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });
        }
        catch (JSONException e){
            e.printStackTrace();
        }
    }

    public String getJSONUrl(String url){
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) { // Download OK
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download result..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }
}
