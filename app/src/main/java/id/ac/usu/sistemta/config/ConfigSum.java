package id.ac.usu.sistemta.config;

/**
 * Created by acer on 01/12/2015.
 */

public class ConfigSum {
    //address
    public static final String URL_ADD ="http://sistemta.esy.es/sistem_ta/mod/mhs/insert_summary.php";
    public static final String URL_GET_SUM = "http://sistemta.esy.es/sistem_ta/mod/mhs/select_summary.php?username=";
    public static final String URL_GET_HISTORY = "http://sistemta.esy.es/sistem_ta/mod/mhs/select_historyjudul.php?username=";
    public static final String URL_DELETE_SUM = "http://sistemta.esy.es/sistem_ta/mod/mhs/delete_summary.php?id_exsum=";
    public static final String URL_GET_BORANG = "http://sistemta.esy.es/sistem_ta/mod/mhs/select_borang.php?username=";
    public static final String URL_UPDATE_SUM="http://sistemta.esy.es/sistem_ta/mod/mhs/update_exsum.php";
    public static final String URL_GET_EX_ONE="http://sistemta.esy.es/sistem_ta/mod/mhs/select_update_exsum.php?id_exsum=";

    //Keys that will be used to send the request to php scripts

    public static final String KEY_EMP_NAMAMHS = "username";
    public static final String KEY_EMP_JUDUL = "judul";
    public static final String KEY_EMP_REKOM1 = "rekomendasi_exsum";
    public static final String KEY_EMP_BIDANG1= "nama_bidang";
    public static final String KEY_EMP_TGL = "tanggal_masuk_exsum";
    public static final String KEY_EMP_NAMADSN= "nama_dosen";
    public static final String KEY_EMP_IDEX= "id_exsum";
    public static final String KEY_EMP_NIM= "nim";

    public static final String KEY_EMP_TOPIK = "ltr_belakang";
    public static final String KEY_EMP_IDBIM = "id_jadwal";
    public static final String KEY_EMP_HASIL = "rms_masalah";
    public static final String KEY_EMP_TUPEL = "tujuan_penelitian";
    public static final String KEY_EMP_LANTER = "landasan_teori";
    public static final String KEY_EMP_METOD = "metodologi";
    public static final String KEY_EMP_DAPUT = "dftr_pustaka";


    //JSON Tags
    public static final String TAG_JSON_ARRAY="result";
    public static final String TAG_NAMAMHS = "username";
    public static final String TAG_JUDUL = "judul";
    public static final String TAG_REKOM1 = "rekomendasi_exsum";
    public static final String TAG_BIDANG1 = "nama_bidang";
    public static final String TAG_TGL = "tanggal_masuk_exsum";
    public static final String TAG_NAMADSN= "nama_dosen";
    public static final String TAG_NIM = "nim";
    public static final String TAG_IDEX = "id_exsum";

    public static final String MHS_NAMAMHS = "username";
    public static final String MHS_JUDUL = "judul";
    public static final String MHS_REKOM1 = "rekomendasi_exsum";
    public static final String MHS_BIDANG1 = "nama_bidang";
    public static final String MHS_TGL = "tanggal_masuk_exsum";
    public static final String MHS_NIM = "nim";
    public static final String MHS_NAMADSN= "nama_dosen";
    public static final String MHS_IDEX = "id_exsum";
}
