package id.ac.usu.sistemta.mahasiswa;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

import id.ac.usu.sistemta.R;
import id.ac.usu.sistemta.config.ConfigSum;
import id.ac.usu.sistemta.handler.RequestHandler;

public class Mhs_update_exsum extends AppCompatActivity implements View.OnClickListener{
    private String id_exsum,nim,jd,bd;
    EditText judul;
    RadioGroup rekomgroup,bidanggroup;
    private RadioButton rekomta,bidangmul,bidangcom,bidangdat,rekomdos,rekommhs,bidangstatus;
    Button buttonupd,buttondel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mhs_update_exsum);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        id_exsum = intent.getStringExtra(ConfigSum.MHS_IDEX);
        nim = intent.getStringExtra(ConfigSum.MHS_NIM);
        jd = intent.getStringExtra(ConfigSum.MHS_JUDUL);
        bd=intent.getStringExtra(ConfigSum.MHS_BIDANG1);

        judul = (EditText) findViewById(R.id.editText5);
        bidanggroup = (RadioGroup) findViewById(R.id.bidang);
        bidangmul=(RadioButton)findViewById(R.id.radioBidang1);
        bidangdat=(RadioButton)findViewById(R.id.radioBidang2);
        bidangcom=(RadioButton)findViewById(R.id.radioBidang3);

        buttonupd = (Button) findViewById(R.id.button5);
        buttondel = (Button) findViewById(R.id.button6);

        buttonupd.setOnClickListener(this);
        buttondel.setOnClickListener(this);

        judul.setText(jd);
        if(bd.equals("Multimedia")){
            bidangmul.setChecked(true);
            bidangdat.setChecked(false);
            bidangcom.setChecked(false);
        }
        else if(bd.equals("Knowledge Management dan Data Science")){
            bidangmul.setChecked(false);
            bidangdat.setChecked(true);
            bidangcom.setChecked(false);
        }
        else{
            bidangmul.setChecked(false);
            bidangdat.setChecked(false);
            bidangcom.setChecked(true);
        }

        getSum();
    }

    private void getSum(){
        class GetSum extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;

            @Override
            protected void onPreExecute(){
                super.onPreExecute();
                loading = ProgressDialog.show(Mhs_update_exsum.this,"Fetching....","Wait...",false,false);
            }
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                showAs(s);
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequestParam(ConfigSum.URL_GET_EX_ONE,id_exsum);
                return s;
            }
        }

        GetSum ge = new GetSum();
        ge.execute();
    }

    private void showAs(String json){
        try{
            JSONObject jsonObject = new JSONObject(json);
            JSONArray result = jsonObject.getJSONArray(ConfigSum.TAG_JSON_ARRAY);
            JSONObject c = result.getJSONObject(0);

            String judulS = c.getString(ConfigSum.TAG_JUDUL);
            String bdang = c.getString(ConfigSum.TAG_BIDANG1);

            judul.setText(judulS);
            if(bdang.equals("Multimedia")){
                bidangmul.setChecked(true);
                bidangdat.setChecked(false);
                bidangcom.setChecked(false);
            }
            if(bdang.equals("Knowledge Management dan Data Science")){
                bidangmul.setChecked(false);
                bidangdat.setChecked(true);
                bidangcom.setChecked(false);
            }
            if(bdang.equals("Computer System")){
                bidangmul.setChecked(false);
                bidangdat.setChecked(false);
                bidangcom.setChecked(true);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        if(v == buttonupd){
            updateEx();
        }

        if(v == buttondel){
            confirmDeleteEx();
        }
    }

    private void updateEx(){
        final String jdl = judul.getText().toString().trim();
        int selectId = bidanggroup.getCheckedRadioButtonId();
        bidangstatus=(RadioButton)findViewById(selectId);
        final String bidangsum1 = bidangstatus.getText().toString().trim();

        class UpdateMhs extends AsyncTask<Void,Void,String>{
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(Mhs_update_exsum.this,"Updating...","Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(Mhs_update_exsum.this, s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(Void... params) {
                HashMap<String,String> hashMap = new HashMap<>();
                hashMap.put(ConfigSum.KEY_EMP_JUDUL,jdl);
                hashMap.put(ConfigSum.KEY_EMP_NIM,nim);
                hashMap.put(ConfigSum.KEY_EMP_IDEX,id_exsum);
                hashMap.put(ConfigSum.KEY_EMP_BIDANG1,bidangsum1);
                RequestHandler rh = new RequestHandler();
                String s = rh.sendPostRequest(ConfigSum.URL_UPDATE_SUM, hashMap);
                return s;
            }
        }

        UpdateMhs ue = new UpdateMhs();
        ue.execute();
    }

    private void deleteEx(){
        class DeleteEx extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(Mhs_update_exsum.this, "Deleting...", "Wait...", false, false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(Mhs_update_exsum.this, s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequestParam(ConfigSum.URL_DELETE_SUM,id_exsum);
                return s;
            }
        }
        DeleteEx de = new DeleteEx();
        de.execute();
    }

    private void confirmDeleteEx(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Apakah kamu ingin menghapus laporan executive summary ini?");

        alertDialogBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        deleteEx();
                        startActivity(new Intent(Mhs_update_exsum.this,MainActivityMhs.class));
                    }
                });

        alertDialogBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
