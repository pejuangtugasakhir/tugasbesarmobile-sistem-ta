package id.ac.usu.sistemta.config;

/**
 * Created by Endang on 1/18/2016.
 */
public class ConfigJudulTA {

    //    public static final String URL_GET_ALL = "http://sistemta.esy.es/sistem_ta/mod/judul/select_judul.php";
    public static final String URL_GET_JDL_ONE = "http://sistemta.esy.es/sistem_ta/mod/judul/select_judul_one.php?id_judulta=";
    public static final String URL_UPDATE_JDL = "http://sistemta.esy.es/sistem_ta/mod/judul/update_judul.php";

    //Keys that will be used to send the request to php scripts
    public static final String KEY_EMP_ID = "id_judulta";
    public static final String KEY_EMP_NIM = "nim";
    public static final String KEY_EMP_JUDUL = "judul";
    public static final String KEY_EMP_BIDANG = "bidang";

    //JSON tag
    public static final String TAG_JSON_ARRAY="result";
    public static final String TAG_ID = "id_judulta";
    public static final String TAG_NIM = "nim";
    public static final String TAG_JUDUL = "judul_ta";
    public static final String TAG_BIDANG = "nama_bidang";

    public static final String ASS_ID = "id_judulta";
    public static final String ASS_NIM = "nim";
    public static final String ASS_JUDUL= "judul_ta";
    public static final String ASS_BIDANG= "nama_bidang";

}
